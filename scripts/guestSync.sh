#!/usr/bin/env bash

    DBUSERNAME=root
    DBPASSWORD=root
    TODAY=`date +%Y-%m-%d`
    LIST=`mysql -u $DBUSERNAME -p$DBPASSWORD  igroove -e "select concat('guest',id),password from Guest where starting_from>=$TODAY or ending_to<=$TODAY;" -s -N`
    echo $LIST
    ADLIST=`samba-tool user list`


# Prima passata: creo gli utenti che mancano

IFS=$'\n'
for row in $DBLIST
do
IFS=$'\t'
  USERNAME=`echo $row | awk '{print $1}'`
  PASSWORD=`echo $row | awk '{print $2}'`

  EXISTS=`echo $ADLIST | grep $USERNAME -c`
  if [ "$EXISTS" == "0" ]; then
     echo samba-tool user create $USERNAME $PASSWORD
     samba-tool user create $USERNAME $PASSWORD
  fi
  IFS=$'\n'
done


# Seconda passata: elimino gli utenti superflui

IFS=$'\t'
for USERNAME in $ADLIST
do
  EXISTS=`echo $LIST | grep $USERNAME -c`
  if [ "$EXISTS" == "1" ]; then
    echo samba-tool user delete $USERNAME
    samba-tool user delete $USERNAME
  fi
done
  IFS=$'\n'