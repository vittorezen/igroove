#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include"sys/types.h"
#include"sys/socket.h"
#include"string.h"
#include"netinet/in.h"
#include"netdb.h"
#define BUF_SIZE 1024

int get_request(char * url,char * ip);

#define CMD "netstat -rn > gateway.txt"
#define FILENAME "gateway.txt"
#define DEFAULT "default" // this may be "0.0.0.0" or something else try cmd the less gateway.txt
char line[255];
char *p, *q, *x;
char url[255]="http://";
FILE *fp;
int sockfd, bindfd;
struct sockaddr_in addr, cl_addr;
char buffer[BUF_SIZE];
char status_ok[] = "OK";
char buffer[BUF_SIZE];
char http_not_found[] = "HTTP/1.0 404 Not Found";
char http_ok[] = "HTTP/1.0 200 OK";
char location[] = "Location: ";
char contentType[] = "Content-Type: ";
int sPos, ePos;
int sockfd, ret;
char * temp;
char path[1000];

int main()
{
system(CMD);
fp = fopen(FILENAME, "rb");
if (!fp)
return -1;
while (fgets(line, 255, fp)) {
  p = strstr(line, DEFAULT);
  if (p)
    break;
}
int i = 0;
while (!isdigit(p[i]))
i++;
q = p + i;
p = strstr(q, ".");
p++;
p = strstr(p, ".");
p++;
p = strstr(p, ".");
p++;
i = 0;
while (isdigit(p[i]))
i++;
q[strlen(q) - strlen(p) + i] = 0;

fclose(fp);


strcat(url, q);
strcat(url, "/logout");
//printf("Try: %s\n",url);

sockfd = get_request(url,q);

 memset(&buffer, 0, sizeof(buffer));
 ret = recv(sockfd, buffer, BUF_SIZE, 0);
 if (ret < 0) {
  printf("Error receiving HTTP status!\n");
 } else {
  //printf("%s\n", buffer);
  if ((temp = strstr(buffer, http_ok)) != NULL) {
   send(sockfd, status_ok, strlen(status_ok), 0);
  } else {
   close(sockfd);
   return 0;
  }
 }

 memset(&buffer, 0, sizeof(buffer));
 ret = recv(sockfd, buffer, BUF_SIZE, 0);
 if (ret < 0) {
  printf("Error receiving HTTP header!\n");
 } else {
  //printf("%s\n", buffer);



 }
}

int get_request(char * url,char * ip) {

 int sockfd, bindfd;
        char * ptr, * host;
 char getrequest[1024];
        struct sockaddr_in addr;
   sprintf(getrequest, "GET /logout HTTP/1.0\nHOST: %s\n\n", url);


 // creates a socket to the host
 sockfd = socket(AF_INET, SOCK_STREAM, 0);
 if (sockfd < 0) {
  printf("Error creating socket!\n");
  exit(1);
 }
 //printf("Socket created...\n");

 memset(&addr, 0, sizeof(addr));
 addr.sin_family = AF_INET;
 addr.sin_addr.s_addr = inet_addr(ip);
 addr.sin_port = htons(atoi("80"));

 if (connect(sockfd, (struct sockaddr *) &addr, sizeof(addr)) < 0 ) {
  printf("Connection Error!\n");
  exit(1);
 }
 //printf("Connection successful...\n\n\n");
 //ptr = strtok(path, "/");
 //strcpy(path, ptr);
 //printf("path=%s\n", path);
 //fileptr = fopen(path, "w");
 //strcpy(fileName, path);
 //sprintf(fileName, "%s", path);

        //int optval = 1;
        //setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

 // writes the HTTP GET Request to the sockfd
 write(sockfd, getrequest, strlen(getrequest));

 return sockfd;
}

int isValidIP(char * ip) {
 struct sockaddr_in addr;
 int valid = inet_pton(AF_INET, ip, &(addr.sin_addr));
 return valid != 0;
}
