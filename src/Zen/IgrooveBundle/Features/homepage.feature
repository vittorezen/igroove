# homepage.feature
Feature: User homepage
  As a registered user
  I see certain links based on my role


  Scenario: I belong to Domain Admins, Domain Users and Administrators groups
      Given I am logged as user "bob" with password "1qaz-2wsx"
        And I am on the homepage
       When I follow "igroove"
       Then I should be on "/"
