<?php

namespace Zen\IgrooveBundle\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ForcePasswordChange {

    private $tokenStorage;
    protected $authorizationChecker;
    private $router;
    private $session;

    public function __construct(Router $router, TokenStorage $tokenStorage, AuthorizationCheckerInterface $authorizationChecker, Session $session) {
        $this->authorizationChecker          = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->router           = $router;
        $this->session          = $session;
    }

    public function onCheckStatus(GetResponseEvent $event) {

        if (($this->tokenStorage->getToken()) && ($this->authorizationChecker->isGranted('IS_AUTHENTICATED_FULLY'))) {

            $routeName = $event->getRequest()->get('_route');

            if ($routeName == 'changeMyPassword' || $routeName == 'checkPassword' || $routeName == 'setNewPassword') {
                return;
            }

            if ($this->authorizationChecker->isGranted('ROLE_FORCEPASSWORDCHANGE')) {
                $response = new RedirectResponse($this->router->generate('changeMyPassword'));
//                $this->session->setFlash('notice', "Your password has expired. Please change it.");
                $event->setResponse($response);
            }
        }
    }

}