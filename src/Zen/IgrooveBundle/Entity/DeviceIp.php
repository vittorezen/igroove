<?php

namespace Zen\IgrooveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * DeviceIp
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Zen\IgrooveBundle\Repository\DeviceIpRepository")
 * @UniqueEntity("ip")
 */
class DeviceIp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Device
     * @ORM\ManyToOne(targetEntity="Device", inversedBy="ips")
     */
    private $mac;

    /**
     * @var integer
     *
     * @ORM\Column(name="ip", type="integer", unique=true, options={"unsigned":true})
     */
    private $ip;

    /**
     * @var MikrotikPool
     *
     * @ORM\ManyToOne(targetEntity="MikrotikPool", inversedBy="deviceIps")
     */
    private $mikrotikPool;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Device
     */
    public function getMac() {
        return $this->mac;
    }

    /**
     * @param Device $mac
     */
    public function setMac($mac) {
        $this->mac = $mac;
    }

    /**
     * Set ip
     *
     * @param integer $ip
     *
     * @return DeviceIp
     */
    public function setRawIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return integer
     */
    public function getRawIp()
    {
        return $this->ip;
    }

    /**
     * Set ip from string with format xxx.xxx.xxx.xxx
     *
     * @param integer $ip
     *
     * @return DeviceIp
     */
    public function setIp($ip) {
        $this->ip = ip2long($ip);

        return $this;
    }

    /**
     * Get ip as string with format xxx.xxx.xxx.xxx
     *
     * @return string
     */
    public function getIp() {
        return long2ip($this->ip);
    }

    /**
     * @return MikrotikPool
     */
    public function getMikrotikPool() {
        return $this->mikrotikPool;
    }

    /**
     * @param MikrotikPool|null $mikrotikPool
     */
    public function setMikrotikPool($mikrotikPool) {
        $this->mikrotikPool = $mikrotikPool;
    }


    /**
     * @Assert\Callback
     */
    public static function validate(DeviceIp $deviceIp, ExecutionContextInterface $context)
    {
        $mikrotikPool = $deviceIp->getMikrotikPool();
        if(!$mikrotikPool instanceof MikrotikPool) {
            $context->buildViolation('Pool mikrotik non valido')
                ->addViolation();

            return;
        }

        if($deviceIp->getRawIp() < $mikrotikPool->getIpStart() || $deviceIp->getRawIp() > $mikrotikPool->getIpEnd()) {
            $context->buildViolation("L'ip non può essere fuori dal range del pool mikrotik")
//                ->atPath('ips')
                ->addViolation();
        }
    }


    public function __toString() {
        return $this->getIp();
    }
}

