<?php

namespace Zen\IgrooveBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Teacher.
 *
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Zen\IgrooveBundle\Repository\TeacherRepository")
 * @UniqueEntity("fiscalCode")
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class Teacher extends PersonAbstract
{
    /**
     * @var ArrayCollection|TeacherSubjectGroup[]
     *
     * @ORM\OneToMany(targetEntity="TeacherSubjectGroup", mappedBy="teacher")
     **/
    private $teacherSubjectGroups;

    /**
     * @var ArrayCollection|Provider[]
     * @ORM\ManyToMany(targetEntity="Provider", inversedBy="additionalTeachers")
     */
    protected $additionalProviders;

    /**
     * Teacher constructor.
     *
     * @param $id
     */
    public function __construct($fiscalCode)
    {
        $this->teacherSubjectGroups = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($fiscalCode);
    }

    public function __clone() {
        parent::__clone();
        $this->teacherSubjectGroups = clone $this->teacherSubjectGroups;
    }

    /**
     * @return ArrayCollection|TeacherSubjectGroup[]
     */
    public function getTeacherSubjectGroups()
    {
        return $this->teacherSubjectGroups;
    }

    /**
     * @param ArrayCollection $teacherSubjectGroups
     */
    public function setTeacherSubjectGroups(ArrayCollection $teacherSubjectGroups)
    {
        $this->teacherSubjectGroups = $teacherSubjectGroups;
    }

    /**
     * @return ArrayCollection|Subject[]
     */
    public function getSubjects()
    {
        $teacherSubjectGroups = $this->getTeacherSubjectGroups();
        $subjects = [];
        foreach ($teacherSubjectGroups as $teacherSubjectGroup) {
            $subject = $teacherSubjectGroup->getSubject();
            if (!$subject instanceof Subject || $subject->getId() == '' || isset($subjects[$subject->getId()])) {
                continue;
            }

            $subjects[$subject->getId()] = $subject;
        }

        return new \Doctrine\Common\Collections\ArrayCollection($subjects);
    }

    /**
     * @return ArrayCollection|Group[]
     */
    public function getGroups()
    {
        $teacherSubjectGroups = $this->getTeacherSubjectGroups();
        $groups = [];
        foreach ($teacherSubjectGroups as $teacherSubjectGroup) {
            $group = $teacherSubjectGroup->getGroup();
            if (!$group instanceof Group || $group->getId() == '' || isset($groups[$group->getId()])) {
                continue;
            }

            $groups[$group->getId()] = $group;
        }

        return new \Doctrine\Common\Collections\ArrayCollection($groups);
    }

    /**
     * @param \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Teacher $importedTeacher
     * @return bool
     */
    public function isSame(\Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Teacher $importedTeacher)
    {
        if ($this->fiscalCode != $importedTeacher->getFiscalCode()) {
            return false;
        }

        if ($this->firstname != $importedTeacher->getFirstName()) {
            return false;
        }

        if ($this->lastname != $importedTeacher->getLastName()) {
            return false;
        }

        if ($importedTeacher->getEmail() !== null && $this->email != $importedTeacher->getEmail()) {
            return false;
        }
        if ($importedTeacher->getUsername() !== null && strtolower($this->username) != strtolower($importedTeacher->getUsername())) {
            return false;
        }
        if ($importedTeacher->getPassword() !== null && $this->starting_password != $importedTeacher->getPassword()) {
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function getProviderSettings()
    {
        if (!$this->provider instanceof Provider) {
            return Provider::$settingsFieldNulled;
        }

        return $this->provider->getTeacherSettings();
    }
}
