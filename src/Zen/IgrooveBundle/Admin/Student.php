<?php

namespace Zen\IgrooveBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class Student extends Admin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('lastname')
            ->add('firstname')->add('email')->add('username')->add('starting_password')->add('memberOf')->add(
                'provider'
            );


    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id')
            ->add('lastname')
            ->add('firstname')->add('email')->add('username')->add('memberOf')->add(
                'provider'
            );

    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('lastname')
            ->add('firstname')->add('email')->add('username')->add('memberOf')->add(
                'provider'
            );

    }

}


