<?php

namespace Zen\IgrooveBundle\Repository;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Sector;


class SectorRepository extends \Doctrine\ORM\EntityRepository
{
    public function createWithImportData($importedData, Provider $provider, $idOnProvider) {
        if(!$importedData instanceof \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Sector) {
            return null;
        }

        $sector = new Sector();
        $sector->setName($importedData->getName());
        $sector->setProvider($provider);
        $sector->setIdOnProvider($idOnProvider);
        $this->getEntityManager()->persist($sector);
        $this->getEntityManager()->flush();

        return $sector;
    }

    public function updateWithImportData(Sector $sector, $newData, Provider $provider) {
        if(!$newData instanceof \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Sector) {
            return $sector;
        }

        $sector->setName($newData->getName());

        $this->getEntityManager()->flush();

        return $sector;
    }

}
