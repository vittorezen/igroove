<?php

namespace Zen\IgrooveBundle\Repository;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Subject;


class SubjectRepository extends \Doctrine\ORM\EntityRepository
{
    public function createWithImportData($importedData, Provider $provider, $idOnProvider) {
        if(!$importedData instanceof \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Subject) {
            return null;
        }

        $subject = new Subject();
        $subject->setName($importedData->getName());
        $subject->setProvider($provider);
        $subject->setIdOnProvider($idOnProvider);
        $this->getEntityManager()->persist($subject);
        $this->getEntityManager()->flush();

        return $subject;
    }

    public function updateWithImportData(Subject $subject, $newData, Provider $provider) {
        if(!$newData instanceof \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Subject) {
            return $subject;
        }

        $subject->setName($newData->getName());

        $this->getEntityManager()->flush();

        return $subject;
    }

}
