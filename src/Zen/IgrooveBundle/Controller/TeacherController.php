<?php

namespace Zen\IgrooveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sonata\DoctrineORMAdminBundle\Model\ModelManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Teacher;
use Zen\IgrooveBundle\Form\TeacherType;

class TeacherController extends Controller {

    /**
     * Lists all Teacher entities.
     *
     * @Route("/teacher", name="teacher_list")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $providers = $em->getRepository('ZenIgrooveBundle:Provider')->findBy([], ['name' => 'ASC']);

        $query = $em->getRepository('ZenIgrooveBundle:Teacher')->createQueryBuilder('t');
        $query->orderBy('t.lastname', 'ASC');

        if($request->query->has('queryString') && trim($request->query->get('queryString', '')) != "") {
            $query->where("t.firstname LIKE :qs OR t.lastname LIKE :qs OR t.username LIKE :qs OR t.email LIKE :qs")
                ->setParameter('qs', $request->query->get('queryString')."%");
        }

        if($request->query->has('providerId') && (int)$request->query->get('providerId', -1) > 0) {
            $query->andWhere('t.provider = :providerId')->setParameter('providerId', $request->query->get('providerId'));
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),25
        );

        return array('pagination' => $pagination,
                     'queryString' => $request->query->get('queryString', ''),
                     'providerId' => $request->query->get('providerId', ''),
                     'providers' => $providers);

    }

    /**
     * Finds and displays a Teacher entity.
     *
     * @Route("/teacher/{id}/show", name="teacher_show")
     * @Template()
     * @Secure(roles="ROLE_TEACHER")
     *
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Teacher')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Teacher entity.');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * Displays a form to create a new Teacher entity.
     *
     * @Route("/teacher/new", name="teacher_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher = new Teacher(null);

        $providerId = $request->get('providerId', '');
        if($providerId != "") {
            $provider = $em->getRepository('ZenIgrooveBundle:Provider')->find($providerId);
            if($provider instanceof Provider) {
                $teacher->setProvider($provider);
            }
        }

        /** @var ModelManager $modelManager */
        $modelManager = $this->get('sonata.admin.manager.orm');
        $form = $this->createForm(new TeacherType($modelManager), $teacher);
        $form->add('submit', 'submit', array('label' => 'Salva Modifiche'));

        return array(
            'teacher' => $teacher,
            'form' => $form->createView(),
            'providerId' => $providerId,
        );
    }

    /**
     * Creates a new Teacher entity.
     *
     * @Route("/teacher/create", name="teacher_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("ZenIgrooveBundle:Teacher:new.html.twig")
     * @Method("post")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher = new Teacher(null);

        $providerId = $request->get('providerId', '');
        if($providerId != "") {
            $provider = $em->getRepository('ZenIgrooveBundle:Provider')->find($providerId);
            if($provider instanceof Provider) {
                $teacher->setProvider($provider);
            }
        }

        /** @var ModelManager $modelManager */
        $modelManager = $this->get('sonata.admin.manager.orm');
        $form = $this->createForm(new TeacherType($modelManager), $teacher);
        $form->add('submit', 'submit', array('label' => 'Salva Modifiche'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            if($teacher->getProvider() instanceof Provider && $teacher->haveAdditionalProvider($teacher->getProvider())) {
                $teacher->removeAdditionalProvider($teacher->getProvider());
            }

            $em->persist($teacher);
            $em->flush();

            $repositoryAction = $this->get('zen.igroove.repositoryAction.teacher');
            $personAndGroups = $this->get('personsAndGroups');
            try {
                $repositoryAction->executeAfterCreate($teacher);
            } catch (\Exception $e) {
                $personAndGroups->addErrorsToForm($e, $form);
            }

            return $this->redirect($this->generateUrl('teacher_list'));
        }

        return array(
            'entity' => $teacher,
            'form' => $form->createView(),
            'providerId' => $providerId,
        );
    }

    /**
     * Displays a form to edit an existing Teacher entity.
     *
     * @Route("/teacher/{id}/edit", name="teacher_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository('ZenIgrooveBundle:Teacher')->find($id);

        if (!$teacher instanceof Teacher) {
            throw $this->createNotFoundException('Unable to find Teacher entity.');
        }

        /** @var ModelManager $modelManager */
        $modelManager = $this->get('sonata.admin.manager.orm');
        $editForm = $this->createForm(new TeacherType($modelManager), $teacher);
        $editForm->add('submit', 'submit', array('label' => 'Salva Modifiche'));
        $deleteForm = $this->createDeleteForm($id);

        $providerId = "";
        if($teacher->getProvider() instanceof Provider) {
            $providerId = $teacher->getProvider()->getId();
        }

        return array(
            'entity' => $teacher,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'providerId' => $providerId
        );
    }

    /**
     * Edits an existing Teacher entity.
     *
     * @Route("/teacher/{id}/update", name="teacher_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("post")
     * @Template("ZenIgrooveBundle:Teacher:edit.html.twig")
     */
    public function updateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository('ZenIgrooveBundle:Teacher')->find($id);

        if (!$teacher instanceof Teacher) {
            throw $this->createNotFoundException('Unable to find Teacher entity.');
        }

        $previousTeacherEntity = clone $teacher;

        /** @var ModelManager $modelManager */
        $modelManager = $this->get('sonata.admin.manager.orm');
        $editForm = $this->createForm(new TeacherType($modelManager), $teacher);
        $editForm->add('submit', 'submit', array('label' => 'Salva Modifiche'));
        $deleteForm = $this->createDeleteForm($id);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if($teacher->getProvider() instanceof Provider && $teacher->haveAdditionalProvider($teacher->getProvider())) {
                $teacher->removeAdditionalProvider($teacher->getProvider());
            }

            $em->persist($teacher);
            $em->flush();

            $repositoryAction = $this->get('zen.igroove.repositoryAction.teacher');
            $personAndGroups = $this->get('personsAndGroups');
            try {
                $repositoryAction->executeAfterUpdate($teacher, $previousTeacherEntity);
            } catch (\Exception $e) {
                $personAndGroups->addErrorsToForm($e, $editForm);
            }

            if($editForm->getErrors()->count() < 1) {
                return $this->redirect($this->generateUrl('teacher_list'));
            }
        }

        $providerId = "";
        if($teacher->getProvider() instanceof Provider) {
            $providerId = $teacher->getProvider()->getId();
        }

        return array(
            'entity' => $teacher,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'providerId' => $providerId
        );
    }

    /**
     * Present the form to delete a Teacher entity.
     *
     * @Route("/teacher/{id}/remove", name="teacher_delete_form")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("get")
     * @Template("ZenIgrooveBundle:Teacher:delete.html.twig")
     */
    public function deleteFormAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository('ZenIgrooveBundle:Teacher')->find($id);

        if (!$teacher) {
            throw $this->createNotFoundException('Unable to find Teacher entity.');
        }

        $form = $this->createDeleteForm($id);
        return ['form' => $form->createView(), 'entity' => $teacher];
    }

    /**
     * Compose delete form
     *
     * @param $id
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('teacher_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('deleteOnLdap', 'checkbox', ['label' => "Cancellare da Active Directory", 'required' => FALSE])
            ->add('deleteOnGApps', 'choice', ['expanded' => true, 'label' => "Cancellare da Google Apps", 'choices' => [1 => 'Si', 2 => 'Spostare in Ou Indicata'], 'required' => FALSE, 'placeholder' => "No"])
            ->add('gAppsOu', 'text', ['label' => "OU Google Apps di destinazione (se specificato lo spostamento)", 'required' => FALSE])
            ->add('submit', 'submit', array('label' => 'Rimuovi', 'attr' => ['class' => 'btn btn-danger']))
            ->getForm();
    }

    /**
     * Deletes a Teacher entity.
     *
     * @Route("/teacher/{id}/delete", name="teacher_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("delete")
     */
    public function deleteAction($id, Request $request)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $teacher = $em->getRepository('ZenIgrooveBundle:Teacher')->find($id);

            if (!$teacher) {
                throw $this->createNotFoundException('Unable to find Teacher entity.');
            }

            $repositoryAction = $this->get('zen.igroove.repositoryAction.teacher');
            try {
                $repositoryAction->executeBeforeRemove($teacher, $form->get('deleteOnLdap')->getData(), $form->get('deleteOnGApps')->getData(), $form->get('gAppsOu')->getData());
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

            $em->remove($teacher);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('teacher_list'));
    }

    /**
     * Show the badge of a student
     *
     * @Route("/teacher/{id}/badge", name="teacher_badge")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function printStudentBadgeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $teacher = $em->getRepository('ZenIgrooveBundle:Teacher')->find($id);

        throw $this->createNotFoundException('Unavailable.');

//        if (!$teacher) {
//            throw $this->createNotFoundException('Unable to find Teacher entity.');
//        }
//
//        return $this->printStudentBadge($teacher);
    }

    /**
     * Ajax call to reset a teacher password
     *
     * @Route("/teacher/reset_password_ajax", name="teacher_reset_password_ajax")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function resetPasswordAjaxAction() {
        $request = $this->get('Request');
        $teacherId = $request->get('id', '');
        $targetService = $request->get('targetService', 'all');

        return $this->resetPassword($teacherId,$targetService,false);
    }

    /**
     * Ajax call to renew a teacher password
     *
     * @Route("/teacher/renew_password_ajax", name="teacher_renew_password_ajax")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function renewPasswordAjaxAction() {
        $request = $this->get('Request');
        $teacherId = $request->get('id', '');
        $targetService = $request->get('targetService', 'all');

        return $this->resetPassword($teacherId,$targetService,true);
    }

    /**
     * Reset or renew a teacher password, anserwing in json
     *
     * @param $teacherId
     * @param $targetService
     * @param bool $renew
     * @return Response
     */
    protected function resetPassword($teacherId, $targetService, $renew=false) {
        $configurationManager = $this->container->get('zen.igroove.configuration');
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $em = $this->getDoctrine()->getManager();

        $teacher = $em->getRepository('ZenIgrooveBundle:Teacher')->find($teacherId);
        if(!$teacher instanceof Teacher) {
            $response->setContent(json_encode(["success" => false, 'error' => "Utente specificato non valido"]));
            return $response;
        }

        if(strlen(trim($teacher->getStartingPassword())) == 0 && !$renew) {
            $response->setContent(json_encode(["success" => false, 'error' => "Password di default non generata"]));
            return $response;
        }

        $personsAndGroups = $this->get('personsAndGroups');
        $newPassword = "";
        if($renew) {
            $newPassword = $personsAndGroups->getRandomPassword($configurationManager->getActiveDirectoryPasswordComplexity(), $configurationManager->getActiveDirectoryPasswordMinChar());
            $teacher->setStartingPassword($newPassword);
        }

        if($targetService == "ad" || $targetService == "all") {
            try {
                $personsAndGroups->resetPersonLdapUserPassword($teacher, true);
            } catch (\Exception $e) {
                $response->setContent(json_encode(["success" => false, 'error' => "Errore durante il tentativo di cambio password di rete"]));
                return $response;
            }
        }

        if($targetService == "email" || $targetService == "all") {
            try {
                $personsAndGroups->resetPersonGoogleAppsUserPassword($teacher, true);
            } catch (\Exception $e) {
                $response->setContent(json_encode(["success" => false, 'error' => "Errore durante il tentativo di cambio password dell'account Google"]));
                return $response;
            }
        }

        $response->setContent(json_encode(["success" => true, 'newPassword' => $newPassword]));

        return $response;
    }

    /**
     * Send the teacher credential to the specified email
     *
     * @Route("/teacher/{id}/send_credential_to_email", name="teacher_send_credential_to_email")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function sendCredentialToEmailAction(Request $request, $id) {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $em = $this->getDoctrine()->getManager();

        $teacher = $em->getRepository('ZenIgrooveBundle:Teacher')->find($id);
        if(!$teacher instanceof Teacher) {
            $response->setContent(json_encode(["success" => false, 'error' => "Docente non valido"]));
            return $response;
        }

        $email = trim($request->get('emailTo', ''));
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response->setContent(json_encode(["success" => false, 'error' => "Email specificata non valida"]));
            return $response;
        }

        $body = <<<MAIL
Riepilogo credenziali per il docente {$teacher->getLastname()} {$teacher->getFirstname()}:

Username: {$teacher->getUsername()}
E-Mail: {$teacher->getEmail()}
Password: {$teacher->getStartingPassword()}
MAIL;

        $message = new \Swift_Message("Credenziali per il docente {$teacher->getLastname()} {$teacher->getFirstname()}", $body);
        $message->setTo($email)->setFrom($this->getParameter('mailer_user'), $this->getParameter($this->container->hasParameter('mailer_name') ? "mailer_name" : "mailer_user"));

        if($this->get('mailer')->send($message)) {
            $response->setContent(json_encode(["success" => true]));
        } else {
            $response->setContent(json_encode(["success" => false, 'error' => "Errore durante l'invio dell'email"]));
        }

        return $response;
    }


}
