<?php

namespace Zen\IgrooveBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Zen\IgrooveBundle\Entity\Mikrotik;
use Zen\IgrooveBundle\Form\MikrotikType;
use Zen\IgrooveBundle\Manager\ConfigurationManager;
use Zen\IgrooveBundle\Manager\MikrotikManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Mikrotik controller.
 *
 * @Route("/config/mikrotik")
 */
class MikrotikController extends Controller
{


    /**
     * Refresh all Mikrotik setups.
     *
     * @Route("/refresh", name="mikrotik_refresh")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function refreshAction()
    {
        $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
        $msg = array('command' => 'addMacToHospot', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addIpReservation', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addIpList', 'parameters' => array());
        $client->publish(serialize($msg));
        $msg = array('command' => 'addPools', 'parameters' => array());
        $client->publish(serialize($msg));
        $response = new Response(json_encode([]));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Lists all MikrotikList entities.
     *
     * @Route("/", name="mikrotik")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $request = $this->get('Request');
        $queryString = $request->get('queryString', false);
        $q = '%' . $queryString . '%';
        $em = $this->getDoctrine()->getManager();
        if ($queryString) {
            $query = $em->createQuery(
                "SELECT l FROM ZenIgrooveBundle:Mikrotik l WHERE l.ip  LIKE :q ORDER BY l.ip"
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery("SELECT l FROM ZenIgrooveBundle:Mikrotik l  ORDER BY l.ip");
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),25
        );


        return array(
            'pagination' => $pagination
        );
    }

    /**
     * Creates a new Mikrotik entity.
     *
     * @Route("/", name="config_mikrotik_create")
     * @Method("POST")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("ZenIgrooveBundle:Mikrotik:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Mikrotik();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Mikrotik entity.
     *
     * @param Mikrotik $entity The entity
     * @Secure(roles="ROLE_ADMIN")
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Mikrotik $entity)
    {
        $form = $this->createForm(
            new MikrotikType(),
            $entity,
            array(
                'action' => $this->generateUrl('config_mikrotik_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Aggiungi'));

        return $form;
    }

    /**
     * Displays a form to create a new Mikrotik entity.
     *
     * @Route("/new", name="config_mikrotik_new")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Mikrotik();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }


    /**
     * Displays a form to edit an existing Mikrotik entity.
     *
     * @Route("/{id}/edit", name="config_mikrotik_edit")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Mikrotik')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mikrotik entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Mikrotik entity.
     *
     * @param Mikrotik $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Mikrotik $entity)
    {
        $form = $this->createForm(
            new MikrotikType(),
            $entity,
            array(
                'action' => $this->generateUrl('config_mikrotik_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Aggiorna'));

        return $form;
    }

    /**
     * Edits an existing Mikrotik entity.
     *
     * @Route("/{id}", name="config_mikrotik_update")
     * @Method("PUT")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("ZenIgrooveBundle:Mikrotik:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Mikrotik')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mikrotik entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('mikrotik'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Mikrotik entity.
     *
     * @Route("/{id}", name="config_mikrotik_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ZenIgrooveBundle:Mikrotik')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Mikrotik entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('mikrotik'));
    }

    /**
     * Creates a form to delete a Mikrotik entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('config_mikrotik_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete', 'attr' => ['class' => "btn btn-danger"]))
            ->getForm();
    }

    /**
     * Get all dhcp servers name from the Mikrotik
     *
     * @Route("/{id}/getDhcpServersName", name="config_mikrotik_get_dhcp_servers_name")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function getDhcpServerName($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ZenIgrooveBundle:Mikrotik')->find($id);

        if (!$entity || $entity->getIp() == "") {
            return new Response(json_encode(['error' => 'Unable to find the mikrotik you specified']), 404);
        }

        $mikrotikManager = $this->get('zen.igroove.mikrotik');

        return new Response(json_encode(['names' => $mikrotikManager->getDhcpServersName($entity->getIp())]));
    }
}
