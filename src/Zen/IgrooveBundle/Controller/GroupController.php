<?php

namespace Zen\IgrooveBundle\Controller;

use Dompdf\Dompdf;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Zen\IgrooveBundle\Entity\Group;
use Zen\IgrooveBundle\Entity\InternetOpen;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Sector;
use Zen\IgrooveBundle\Entity\Student;
use Zen\IgrooveBundle\Form\GroupType;
use Ps\PdfBundle\Annotation\Pdf;
use JMS\SecurityExtraBundle\Annotation\Secure;

/**
 * Group controller.
 *
 */
class GroupController extends Controller
{
    /**
     * List all providers
     *
     * @Route("/student/provider", name="group_providers_list")
     * @Secure(roles="ROLE_TEACHER")
     * @Method("GET")
     * @Template()
     */
    public function listProvidersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $filterCriteria = ['active' => true];
        if(!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            $filterCriteria['internetTeachersControl'] = true;
        }

        $providers = $em->getRepository('ZenIgrooveBundle:Provider')->findBy($filterCriteria);
        
        return array('providers' => $providers);
    }

    /**
     * Lists all Group entities in a provider.
     *
     * @Route("/student/provider/{providerId}", name="list_groups_in_provider")
     * @Secure(roles="ROLE_TEACHER")
     * @Method("GET")
     * @Template()
     */
    public function listGroupsInProviderAction($providerId)
    {
        $em = $this->getDoctrine()->getManager();
        $ldapGroupRepository = $em->getRepository('ZenIgrooveBundle:LdapGroup');
        $configurationManager = $this->get('zen.igroove.configuration');
        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $provider = $em->getRepository('ZenIgrooveBundle:Provider')->find($providerId);
        if(!$provider instanceof Provider) {
            return false;
        }

        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $currentUserName = "";
        $currentUserIsAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
        if($currentUser instanceof \IMAG\LdapBundle\User\LdapUser){
            $currentUserName = $currentUser->getUsername();
        }

        $currentSectorName = "";
        $sectorsList = [];
        if($currentSectorId = $this->get('Request')->get('sectorId',false)) {
            $currentSector = $em->getRepository("ZenIgrooveBundle:Sector")->find($currentSectorId);
            if(!$currentSector instanceof Sector) {
                return false;
            }

            $groups = $currentSector->getGroups();
            $currentSectorName = $currentSector->getName();
        } else {
            $groups = $provider->getGroups();
            $sectorsList = $provider->getSectors();
        }

        $activatingGroup = false;
        $groupsList=array();
        foreach ($groups as $groupId => $group) {
            $groupName = (string)($group->getName());
            $ldapGroupName = $active_directory_generated_group_prefix . $ldapGroupRepository::ldapEscape($groupName);
            $state = $ldapGroupRepository->getInternetStatus($ldapGroupName,$active_directory_generated_group_prefix);
            $endingTime = "";
            $activedByOtherUser = $permitPersonalDevices = false;
            $activedBy = "";
            if($state=="group-active") {
                $internetOpen = $em->getRepository('ZenIgrooveBundle:InternetOpen')->findOneBy(['account' => $ldapGroupName, 'type' => 'group']);
                if($internetOpen instanceof InternetOpen) {
                    $permitPersonalDevices = $internetOpen->getPermitPersonalDevices();
                    if(!$internetOpen->getActivationCompleated()) {
                        $state = "activating";
                        $activatingGroup = true;
                    } else {
                        $endingTime = $internetOpen->getCloseAt()->format("H:i");

                        $activedBy = $currentUserIsAdmin && $internetOpen->getActivedBy() != "" ? $internetOpen->getActivedBy() : "";
                        if($currentUserName != "" && $internetOpen->getActivedBy() != $currentUserName) {
                            $activedByOtherUser = true;
                        }
                    }
                }
            }

            $groupsList[$groupName]['id']=$group->getId();
            $groupsList[$groupName]['name']=$group->getName();
            $groupsList[$groupName]['state'] = $state;
            $groupsList[$groupName]['permitPersonalDevices'] = $permitPersonalDevices;
            $groupsList[$groupName]['endingTime'] = $endingTime;
            $groupsList[$groupName]['activedByOtherUser'] = $activedByOtherUser;
            $groupsList[$groupName]['activedBy'] = $activedBy;
        }
        ksort($groupsList);
        $timeToSet = new \DateTime("+1 hour");

        $activatedGroup = "";
        if($activatingGroup && $this->get('Request')->get('activatedGroup',"") != "") {
            $activatedGroup = $this->get('Request')->get('activatedGroup',"");
        }

        return ['provider' => $provider, 'groups' => $groupsList, 'timeToSet' => $timeToSet, 'currentSectorId' => $currentSectorId,
                'currentSectorName' => $currentSectorName, 'sectorsList' => $sectorsList, 'currentUserIsAdmin' => $currentUserIsAdmin,
                'userCanEnablePersonalDevice' => ($currentUserIsAdmin || $configurationManager->getTeacherCanEnablePersonalDevice()),
                'wifiSsid' => $configurationManager->getWirelessSsid(), 'activatedGroup' => $activatedGroup];
    }

    /**
     * Redirect to group show
     *
     * @Route("/student/group/{id}", name="group_show")
     * @Secure(roles="ROLE_TEACHER")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('ZenIgrooveBundle:Group')->find($id);

        if(!$group instanceof Group || !$group->getProvider() instanceof Provider) {
            throw $this->createNotFoundException('Specified group not found');
        }

        return $this->redirectToRoute('list_students_in_group', ['providerId' => $group->getProvider()->getId(), 'groupId' => $group->getId()]);
    }

    /**
     * Displays a form to create a new Group entity.
     *
     * @Route("/student/provider/{providerId}/group/new", name="group_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template()
     */
    public function newAction($providerId)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Group();
        $entity->setManageManually(true);

        if($providerId != "") {
            $provider = $em->getRepository('ZenIgrooveBundle:Provider')->find($providerId);
            if($provider instanceof Provider) {
                $entity->setProvider($provider);
            }
        }

        $form   = $this->createCreateForm($providerId, $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'providerId' => $providerId
        );
    }

    /**
     * Creates a new Group entity.
     *
     * @Route("/student/provider/{providerId}/group/create", name="group_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("ZenIgrooveBundle:Group:new.html.twig")
     */
    public function createAction(Request $request, $providerId)
    {
        $em = $this->getDoctrine()->getManager();
        $group = new Group();
        $group->setManageManually(true);

        if($providerId != "") {
            $provider = $em->getRepository('ZenIgrooveBundle:Provider')->find($providerId);
            if($provider instanceof Provider) {
                $group->setProvider($provider);
            }
        }

        $form = $this->createCreateForm($providerId, $group);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($group);
            $em->flush();

            $repositoryAction = $this->get('zen.igroove.repositoryAction.group');
            try {
                $repositoryAction->executeAfterCreate($group);
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

            return $this->redirect($this->generateUrl('list_students_in_group', array('groupId' => $group->getId(), 'providerId' => $providerId)));
        }

        return array(
            'entity' => $group,
            'form'   => $form->createView(),
            'providerId' => $providerId
        );
    }

    /**
     * Creates a form to create a Group entity.
     *
     * @param Group $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm($providerId, Group $entity)
    {
        $form = $this->createForm(new GroupType(), $entity, array(
            'action' => $this->generateUrl('group_create', ['providerId' => $providerId]),
            'method' => 'POST',
        ));

        return $form;
    }

    /**
     * Displays a form to edit an existing Group entity.
     *
     * @Route("/student/provider/{providerId}/group/{groupId}/edit", name="group_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template()
     */
    public function editAction($providerId, $groupId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Group')->find($groupId);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Group entity.');
        }

        $editForm = $this->createEditForm($providerId, $entity);
//        $deleteForm = $this->createDeleteForm($providerId, $groupId);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
            'providerId' => $providerId
        );
    }

    /**
     * Edits an existing Group entity.
     *
     * @Route("/student/provider/{providerId}/group/{groupId}/update", name="group_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("ZenIgrooveBundle:Group:edit.html.twig")
     */
    public function updateAction(Request $request, $providerId, $groupId)
    {
        $em = $this->getDoctrine()->getManager();

        $group = $em->getRepository('ZenIgrooveBundle:Group')->find($groupId);

        if (!$group instanceof Group) {
            throw $this->createNotFoundException('Unable to find Group entity.');
        }

        $oldName = $group->getName();

//        $deleteForm = $this->createDeleteForm($providerId, $groupId);
        $editForm = $this->createEditForm($providerId, $group);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->persist($group);
            $em->flush();

            $repositoryAction = $this->get('zen.igroove.repositoryAction.group');
            try {
                $repositoryAction->executeAfterUpdate($group, $oldName);
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $editForm->addError(new FormError($message));
                }
            }

            return $this->redirect($this->generateUrl('list_students_in_group', array('groupId' => $groupId, 'providerId' => $providerId)));
        }

        return array(
            'entity'      => $group,
            'edit_form'   => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
            'providerId' => $providerId
        );
    }

    /**
     * Creates a form to edit a Group entity.
     *
     * @param Group $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm($providerId, Group $entity)
    {
        $form = $this->createForm(new GroupType(), $entity, array(
            'action' => $this->generateUrl('group_update', array('groupId' => $entity->getId(), 'providerId' => $providerId)),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Salva Modifiche'));

        return $form;
    }

    /**
     * Present the form to delete a Student entity.
     *
     * @Route("/student/provider/{providerId}/group/{groupId}/remove", name="group_delete_form")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("get")
     * @Template("ZenIgrooveBundle:Group:delete.html.twig")
     */
    public function deleteFormAction(Request $request, $providerId, $groupId)
    {
        $em = $this->getDoctrine()->getManager();
        $group = $em->getRepository('ZenIgrooveBundle:Group')->find($groupId);

        if (!$group) {
            throw $this->createNotFoundException('Unable to find Group entity.');
        }

        if(!$group->getManageManually()) {
            throw $this->createAccessDeniedException("Imported group cannot be deleted manually");
        }

        $form = $this->createDeleteForm($providerId, $groupId);
        return ['form' => $form->createView(), 'entity' => $group, 'providerId' => $providerId];
    }

    /**
     * Deletes a Group entity.
     *
     * @Route("/student/provider/{providerId}/group/{groupId}", name="group_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $providerId, $groupId)
    {
        $form = $this->createDeleteForm($providerId, $groupId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $group = $em->getRepository('ZenIgrooveBundle:Group')->find($groupId);

            if (!$group) {
                throw $this->createNotFoundException('Unable to find Group group.');
            }

            $repositoryAction = $this->get('zen.igroove.repositoryAction.group');
            try {
                $repositoryAction->executeBeforeRemove($group, $form->get('deleteOnLdap')->getData(), $form->get('deleteOnGApps')->getData());
            } catch (\Exception $e) {
                foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
                    $form->addError(new FormError($message));
                }
            }

            $em->remove($group);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('list_groups_in_provider', ['providerId' => $providerId]));
    }

    /**
     * Creates a form to delete a Group entity by id.
     *
     * @param mixed $groupId The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($providerId, $groupId)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('group_delete', array('groupId' => $groupId, 'providerId' => $providerId)))
            ->setMethod('DELETE')
            ->add('deleteOnLdap', 'checkbox', ['label' => "Cancellare da Active Directory", 'required' => FALSE])
            ->add('deleteOnGApps', 'checkbox', ['label' => "Cancellare da Google Apps", 'required' => FALSE])
            ->add('submit', 'submit', array('label' => 'Elimina', 'attr' => ['class' => "btn-danger"]))
            ->getForm()
            ;
    }

    /**
     * Show all the students in a group
     *
     * @Route("/student/provider/{providerId}/group/{groupId}", name="list_students_in_group")
     * @Method("GET")
     * @Secure(roles="ROLE_TEACHER")
     * @Template()
     */
    public function listStudentsInGroupAction($providerId, $groupId)
    {
        $em = $this->getDoctrine()->getManager();
        $configurationManager = $this->get('zen.igroove.configuration');

        $active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
//        $users = $em->getRepository('ZenIgrooveBundle:LdapGroup')->getAllChildrenUsers($active_directory_generated_group_prefix.$group);

        $provider = $em->getRepository('ZenIgrooveBundle:Provider')->find($providerId);
        $group = $em->getRepository('ZenIgrooveBundle:Group')->find($groupId);
        if(!$group instanceof Group) {
            throw $this->createNotFoundException('Non è stato possibile trovare il gruppo indicato.');
        }

        $students = $group->getStudents();

        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $currentUserName = "";
        $currentUserIsAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
        if($currentUser instanceof \IMAG\LdapBundle\User\LdapUser){
            $currentUserName = $currentUser->getUsername();
        }

        $activatingUser = false;
        $usersList=array();
        foreach ($students as $student) {
            $username = $student->getUsername();
            $state = $em->getRepository('ZenIgrooveBundle:LdapUser')->getInternetStatus($username,$active_directory_generated_group_prefix);
            $endingTime = "";
            $activedByOtherUser = $permitPersonalDevices = false;
            $activedBy = "";
            if($state=="user-active") { //@todo stato utente ldap non creato?
                $internetOpen = $em->getRepository('ZenIgrooveBundle:InternetOpen')->findOneBy(['account' => $username, 'type' => 'user']);
                if($internetOpen instanceof InternetOpen) {
                    $permitPersonalDevices = $internetOpen->getPermitPersonalDevices();
                    if(!$internetOpen->getActivationCompleated()) {
                        $state = "activating";
                        $activatingUser = true;
                    } else {
                        $endingTime = $internetOpen->getCloseAt()->format("H:i");

                        $activedBy = $currentUserIsAdmin && $internetOpen->getActivedBy() != "" ? $internetOpen->getActivedBy() : "";
                        if($currentUserName != "" && $internetOpen->getActivedBy() != $currentUserName) {
                            $activedByOtherUser = true;
                        }
                    }
                }
            }
            $usersList[$username] = array(
                'student' => $student,
                'state' =>  $state,
                'ending_time' => $endingTime,
                'permitPersonalDevices' => $permitPersonalDevices,
                'activedByOtherUser' => $activedByOtherUser,
                'activedBy' => $activedBy
            );
        }
        ksort($usersList);

        $timeToSet = new \DateTime("+1 hour");
        $activatedUser = "";
        if($activatingUser && $this->get('Request')->get('activatedUser',"") != "") {
            $activatedUser = $this->get('Request')->get('activatedUser',"");
        }

        return array('group' => $group, 'students' => $usersList, 'timeToSet' => $timeToSet, 'provider' => $provider,
                     'currentUserIsAdmin' => $currentUserIsAdmin, 'teacherCanSeeList' => $configurationManager->getTeacherCanSeeList(),
                     'teacherCanResetPassword' => $configurationManager->getTeacherCanResetPassword(),
                     'userCanEnablePersonalDevice' => ($currentUserIsAdmin || $configurationManager->getTeacherCanEnablePersonalDevice()),
                     'wifiSsid' => $configurationManager->getWirelessSsid(), 'activatedUser' => $activatedUser
        );
    }


    /**
     * Print the list of the students data, in pdf format
     *
     * @Route("/student/provider/{providerId}/group/{groupId}/print", name="print_students_in_group")
     * @Secure(roles="ROLE_TEACHER")
     */
    public function printStudentsInGroupAction($providerId, $groupId)
    {
        $format = $this->get('request')->get('_format', 'html');
        $em = $this->getDoctrine()->getManager();

        if(!$this->get('zen.igroove.configuration')->getTeacherCanSeeList() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException();
        }

        $provider = $em->getRepository('ZenIgrooveBundle:Provider')->find($providerId);
        $group = $em->getRepository('ZenIgrooveBundle:Group')->find($groupId);
        $students = $group->getStudents();
        $showPassword=($provider->getStudentForceImportedPassword())?false:true;
        $list=array();
        foreach ($students as $student) {
            $list[]=array(
                'student'=>$student,
                'ldapUser'=> $em->getRepository('ZenIgrooveBundle:LdapUser')->findOneByDistinguishedId($student->getId())
            );
        }

        $pdfHeader = $pdfFooter = "";
        if($provider->getPdfHeader() != "") {
            $pdfHeader = $provider->getPdfHeader();
        }

        if($provider->getPdfFooter() != "") {
            $pdfFooter = $provider->getPdfFooter();
        }

        $out = $this->render(
            sprintf('ZenIgrooveBundle:Group:showStudentsGroup.%s.twig', $format), array(
                'provider' => $provider,
                'group' => $group,
                'list' => $list,
                'showPassword' => $showPassword,
                'pdfHeader' => $pdfHeader,
                'pdfFooter' => $pdfFooter,
                'headerHeight' => $provider->getPdfHeaderHeight() > 0 ? $provider->getPdfHeaderHeight() : ($pdfHeader != "" ? 20 : 0),
                'footerHeight' => $provider->getPdfFooterHeight() > 0 ? $provider->getPdfFooterHeight() : ($pdfFooter != "" ? 20 : 0),
            )
        );

        if($format == "pdf") {
            $dompdf = new Dompdf();
            $dompdf->loadHtml($out->getContent());
            $dompdf->getOptions()->set('isRemoteEnabled', true);

            $dompdf->render();
            $out->setContent($dompdf->output());
            $out->headers->set('Content-Type', 'application/pdf');
        }

        return $out;
    }
}
