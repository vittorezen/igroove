<?php

namespace Zen\IgrooveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Zen\IgrooveBundle\Entity\Guest;
use Zen\IgrooveBundle\Form\GuestType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Response;
use Zen\IgrooveBundle\Manager\ConfigurationManager;

/**
 * Guest controller.
 *
 */
class GuestController extends Controller
{
    /**
     * Lists all Guest entities.
     *
     * @Route("/guest", name="guest")
     * @Secure(roles="ROLE_RECEPTION")
     * @Template()
     */
    public function indexAction()
    {
        $request = $this->get('Request');
        $queryString = $request->get('queryString', false);

        $showInactive = $request->get('showInactive', 'off') == 'on' ? true : false;
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime();
        $timeWhere = "(g.starting_from<= :today AND g.ending_to >= :today)";


        if ($queryString) {
            $q = '%' . $queryString . '%';

            if ($showInactive) {
                $query = $em->createQuery(
                    "SELECT g
            FROM ZenIgrooveBundle:Guest g
            WHERE g.firstname LIKE :q
            OR g.lastname  LIKE :q
            ORDER BY g.id DESC, g.lastname ASC , g.firstname ASC "
                )
                    ->setParameter('q', $q);
            } else {
                $query = $em->createQuery(
                    "SELECT g
            FROM ZenIgrooveBundle:Guest g
            WHERE g.firstname LIKE :q
            OR g.lastname  LIKE :q AND $timeWhere
            ORDER BY g.id DESC, g.lastname ASC , g.firstname ASC "
                )
                    ->setParameter('q', $q)
                    ->setParameter('today', $today);
            }
        } else {
            if ($showInactive) {
                $query = $em->createQuery(
                    "SELECT g
            FROM ZenIgrooveBundle:Guest g
            ORDER BY g.id DESC, g.lastname ASC , g.firstname ASC "
                );
            } else {
                $query = $em->createQuery(
                    "SELECT g
            FROM ZenIgrooveBundle:Guest g
            WHERE $timeWhere
            ORDER BY g.id DESC, g.lastname ASC , g.firstname ASC "
                )->setParameter('today', $today);
            }
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1),25
        );

        return array(
            'pagination' => $pagination,
            'showInactive' => $showInactive
        );
    }

    /**
     * Finds and displays a Guest entity.
     *
     * @Route("/guest/{id}/show", name="guest_show")
     * @Secure(roles="ROLE_RECEPTION")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Guest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Guest entity.
     *
     * @Route("/guest/new", name="guest_new")
     * @Secure(roles="ROLE_RECEPTION")
     * @Template()
     */
    public function newAction()
    {
        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $now = new \DateTime();

        $entity = new Guest();
        $entity->setIdentifiedBy((string)$currentUser);
        $entity->setStartingFrom($now);
        $entity->setEndingTo($now);
        $form = $this->createForm(new GuestType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Guest entity.
     *
     * @Route("/guest/create", name="guest_create")
     * @Method("post")
     * @Template("ZenIgrooveBundle:Guest:new.html.twig")
     * @Secure(roles="ROLE_RECEPTION")
     */
    public function createAction()
    {
        $entity = new Guest();
        $request = $this->getRequest();
        $form = $this->createForm(new GuestType(), $entity);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setPassword();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('guest_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Guest entity.
     *
     * @Route("/guest/{id}/edit", name="guest_edit")
     * @Secure(roles="ROLE_RECEPTION")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Guest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }

        $editForm = $this->createForm(new GuestType(), $entity);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        );
    }

    /**
     * Edits an existing Guest entity.
     *
     * @Route("/guest/{id}/update", name="guest_update")
     * @Method("post")
     * @Template("ZenIgrooveBundle:Guest:edit.html.twig")
     * @Secure(roles="ROLE_RECEPTION")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Guest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }

        $editForm = $this->createForm(new GuestType(), $entity);

        $request = $this->getRequest();

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if (!$entity->getPassword()) {
                $entity->setPassword();
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('guest', array()));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        );
    }

    /**
     * Deletes a Guest entity.
     *
     * @Route("/guest/{id}/delete", name="guest_delete")
     * @Secure(roles="ROLE_RECEPTION")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ZenIgrooveBundle:Guest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('guest'));
    }

    /**
     * Check a Guest entity.
     *
     * @Route("/checkGuest/guest{id}/{password}", name="checkGuest")
     */
    public function checkGuestAction($id, $password)
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->getRepository('ZenIgrooveBundle:Guest')
            ->createQueryBuilder('g')
            ->select('g.id')
            ->where(
                'g.starting_from <= :date and g.ending_to >= :date and
                    g.id = :id and g.password = :password'
            )
            ->setParameter('date', new \DateTime('today'))
            ->setParameter('password', $password)
            ->setParameter('id', $id)
            ->getQuery();
        $results = $query->getResult();

        if (!$results) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }

        return new Response('ok');
    }

    /**
     * Print a badge of an existing Guest entity.
     *
     * @Route("/guest/{id}/print", name="guest_print")
     * @Secure(roles="ROLE_RECEPTION")
     * @Template()
     */
    public function printAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:Guest')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Guest entity.');
        }
        $configurationManager = $this->get('zen.igroove.configuration');
        $ssid = $configurationManager->getWirelessSsid();
        $network_type = $configurationManager->getWirelessGuestNetworkType();
        $rules = $configurationManager->getWirelessGuestNetworkRules();
        $realm = $configurationManager->getGuestRealm();

        return array(
            'entity' => $entity,
            'ssid' => $ssid,
            'rules' => $rules,
            'network_type' => $network_type,
            'realm' => $realm != "" ? "@".$realm : ""
        );
    }


    /**
     * Send the student credential to the specified email
     *
     * @Secure(roles="ROLE_RECEPTION")
     * @Route("/guest/{id}/send_credential_to_email", name="guest_send_credential_to_email")
     */
    public function sendCredentialToEmailAction(Request $request, $id) {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $em = $this->getDoctrine()->getManager();

        $guest = $em->getRepository('ZenIgrooveBundle:Guest')->find($id);
        if(!$guest instanceof Guest) {
            $response->setContent(json_encode(["success" => false, 'error' => "Guest non valido"]));
            return $response;
        }

        $email = trim($request->get('emailTo', ''));
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response->setContent(json_encode(["success" => false, 'error' => "Email specificata non valida"]));
            return $response;
        }

        $configurationManager = $this->get('zen.igroove.configuration');
        $ssid = $configurationManager->getWirelessSsid();
        $network_type = $configurationManager->getWirelessGuestNetworkType();
        $realm = $configurationManager->getGuestRealm() != "" ? "@".$configurationManager->getGuestRealm() : "";

        $body = <<<MAIL
Riepilogo credenziali per l'utenza guest di {$guest->getLastname()} {$guest->getFirstname()}:

Username: guest{$guest->getId()}{$realm}
Password: {$guest->getPassword()}
Rete wifi (SSID): {$ssid}
Cifratura: {$network_type}
Valido dal: {$guest->getStartingFrom()->format('d/m/Y')}
Fino al: {$guest->getEndingTo()->format('d/m/Y')}
MAIL;

        $message = new \Swift_Message("Credenziali per l'utenza guest di {$guest->getLastname()} {$guest->getFirstname()}", $body);
        $message->setTo($email)->setFrom($this->getParameter('mailer_user'), $this->getParameter($this->container->hasParameter('mailer_name') ? "mailer_name" : "mailer_user"));

        if($this->get('mailer')->send($message)) {
            $response->setContent(json_encode(["success" => true]));
        } else {
            $response->setContent(json_encode(["success" => false, 'error' => "Errore durante l'invio dell'email"]));
        }

        return $response;
    }

}
