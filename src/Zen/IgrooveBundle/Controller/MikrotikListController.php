<?php

namespace Zen\IgrooveBundle\Controller;

use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Zen\IgrooveBundle\Entity\InternetOpen;
use Zen\IgrooveBundle\Entity\MikrotikList;
use Zen\IgrooveBundle\Form\MikrotikListType;

/**
 * MikrotikList controller.
 *
 * @Route("/mikrotik/list")
 */
class MikrotikListController extends Controller
{
    /**
     * Lists all MikrotikList entities.
     *
     * @Route("/", name="mikrotik_list")
     * @Method("GET")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function indexAction()
    {
        $request = $this->get('Request');
        $queryString = $request->get('queryString', false);
        $q = '%' . $queryString . '%';
        $em = $this->getDoctrine()->getManager();
        if ($queryString) {
            $query = $em->createQuery(
                'SELECT l FROM ZenIgrooveBundle:MikrotikList l WHERE l.nome  LIKE :q ORDER BY l.nome'
            )
                ->setParameter('q', $q);
        } else {
            $query = $em->createQuery('SELECT l FROM ZenIgrooveBundle:MikrotikList l  ORDER BY l.nome');
        }

        $currentUser = $this->get('security.token_storage')->getToken()->getUser();
        $currentUserName = "";
        $currentUserIsAdmin = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');
        if($currentUser instanceof \IMAG\LdapBundle\User\LdapUser){
            $currentUserName = $currentUser->getUsername();
        }

        $mikrotikLists = $query->getResult();
        foreach ($mikrotikLists as $k => $mikrotikList) {
            $mikrotikLists[$k]->state = "not-active";
            $mikrotikLists[$k]->endingTime = "";
            $mikrotikLists[$k]->activedByOtherUser = false;
            $mikrotikLists[$k]->activedBy = "";

            $internetOpen = $em->getRepository('ZenIgrooveBundle:InternetOpen')->findOneBy(['account' => $mikrotikList->getId(), 'type' => 'ipList']);
            if($internetOpen instanceof InternetOpen) {
                if(!$internetOpen->getActivationCompleated()) {
                    $mikrotikLists[$k]->state = "activating";
                } else {
                    $mikrotikLists[$k]->state = "active";
                    $mikrotikLists[$k]->endingTime = $internetOpen->getCloseAt()->format("H:i");

                    $mikrotikLists[$k]->activedBy = $currentUserIsAdmin && $internetOpen->getActivedBy() != "" ? $internetOpen->getActivedBy() : "";
                    if($currentUserName != "" && $internetOpen->getActivedBy() != $currentUserName) {
                        $mikrotikLists[$k]->activedByOtherUser = true;
                    }
                }
            }
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $mikrotikLists,
            $this->get('request')->query->get('page', 1),
            25
        );

        return array(
            'pagination' => $pagination,
            'timeToSet' => new \DateTime("+1 hour")
        );
    }

    /**
     * Creates a new MikrotikList entity.
     *
     * @Route("/", name="mikrotik_list_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("ZenIgrooveBundle:MikrotikList:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new MikrotikList();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $msg = array('command' => 'addIpList', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addMacToHospot', 'parameters' => array());
            $client->publish(serialize($msg));

            return $this->redirect($this->generateUrl('mikrotik_list'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a MikrotikList entity.
     *
     * @param MikrotikList $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(MikrotikList $entity)
    {
        $form = $this->createForm(
            new MikrotikListType(),
            $entity,
            array(
                'action' => $this->generateUrl('mikrotik_list_create'),
                'method' => 'POST',
            )
        );

        return $form;
    }

    /**
     * Displays a form to create a new MikrotikList entity.
     *
     * @Route("/new", name="mikrotik_list_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new MikrotikList();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing MikrotikList entity.
     *
     * @Route("/{id}/edit", name="mikrotik_list_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:MikrotikList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MikrotikList entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a MikrotikList entity.
     *
     * @param MikrotikList $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(MikrotikList $entity)
    {
        $form = $this->createForm(
            new MikrotikListType(),
            $entity,
            array(
                'action' => $this->generateUrl('mikrotik_list_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        return $form;
    }

    /**
     * Edits an existing MikrotikList entity.
     *
     * @Route("/{id}", name="mikrotik_list_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("PUT")
     * @Template("ZenIgrooveBundle:MikrotikList:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ZenIgrooveBundle:MikrotikList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find MikrotikList entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $client = $this->container->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $msg = array('command' => 'addIpList', 'parameters' => array());
            $client->publish(serialize($msg));
            $msg = array('command' => 'addMacToHospot', 'parameters' => array());
            $client->publish(serialize($msg));

            return $this->redirect($this->generateUrl('mikrotik_list'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a MikrotikList entity.
     *
     * @Route("/{id}", name="mikrotik_list_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ZenIgrooveBundle:MikrotikList')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find MikrotikList entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('mikrotik_list'));
    }

    /**
     * Creates a form to delete a MikrotikList entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mikrotik_list_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }
}
