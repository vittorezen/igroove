<?php

namespace Zen\IgrooveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;

class StatsController extends Controller
{
    /**
     * @Route("/stats/utenti/{server}", name="stats_utenti")
     * @Template()
     */
    public function indexAction($server)
    {
        $em = $this->getDoctrine()->getManager();
        $configurationManager = $this->get('zen.igroove.configuration');
        $mikrotikManager = $this->get('zen.igroove.mikrotik');

        $realm = $configurationManager->getFreeradiusRealm();
        $all = $mikrotikManager->getAllUsersInHotspots();

        $countAllRealm = array();
        $countAllRealm[$realm] = 0;
        $countAllRealm['Ospiti'] = 0;
        $countAllRealm['Studenti'] = 0;
        $countAllRealm[$realm] = 0;
        foreach ($all as $user => $serverHotpot) {
            if (($server != 'all') AND ($server != $serverHotpot)) {
                continue;
            }
            if (substr($user, -strlen(trim('@' . $realm))) == '@' . $realm) {
                $username = strtolower(substr($user, 0, -strlen(trim('@' . $realm))));
                $student = $em->getRepository('ZenIgrooveBundle:LdapUser')->find($username);
                if ($student) {
                    $countAllRealm['Studenti']++;
                } else {
                    $countAllRealm[$realm]++;
                }
            } else {
                if (strpos($user, '@')) {
                    $realmX = substr(strrchr($user, '@'), 1);
                    if (!array_key_exists($realmX, $countAllRealm)) {
                        $countAllRealm[$realmX] = 0;
                    }
                    $countAllRealm[$realmX]++;
                } else {
                    $countAllRealm['Ospiti']++;
                }
            }
        }


        $ready = array();
        foreach ($countAllRealm as $k => $v) {
            if ($v == 0) {
                continue;
            }
            $ready[] = array($k . " ($v)", $v);
        }

        $response = new Response(
            json_encode(
                $ready
            )
        );
        $response->headers->set('Content-Type', 'application/json');

        return $response;


    }


}

