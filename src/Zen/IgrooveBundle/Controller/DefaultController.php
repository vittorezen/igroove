<?php

namespace Zen\IgrooveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

use Zen\IgrooveBundle\Manager\ConfigurationManager;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction()
    {
        $msg = array('command' => 'checkNewVersion', 'parameters' => array());
        $client = $this->container->get('old_sound_rabbit_mq.version_service_producer');
        $client->publish(serialize($msg));

        $versions = $this->get('zen.igroove.version')->getVersions();
        $configurationManager = $this->get('zen.igroove.configuration');
        $wirelessHotspotServer = $configurationManager->getWirelessHotspotServer();
        $showStats = $configurationManager->getShowStats();

        return array(
            'version' => $versions,
            'menu_render' => $configurationManager->getMenuConfiguration($this->currentUserIsStudent()),
            'wirelessHotspotServer' => $wirelessHotspotServer,
            'showStats' => $showStats,
        );
    }

    /**
     * @Route("/menu", name="menu")
     * @Template()
     */
    public function menuAction()
    {
        $configurationManager = $this->get('zen.igroove.configuration');

        return array(
            'menu_render' => $configurationManager->getMenuConfiguration($this->currentUserIsStudent())
        );
    }

    /**
     * @Route("/credits", name="credits")
     * @Template()
     */
    public function creditsAction()
    {
        return array();
    }

    /**
     * @Route("/help", name="help")
     * @Template()
     */
    public function helpAction()
    {
        $configurationManager = $this->get('zen.igroove.configuration');

        return array(

            'menu_render' => $configurationManager->getMenuConfiguration($this->currentUserIsStudent())
        );
    }

    /**
     * @Route("/login", name="login")
     * @Template()
     */
    public function loginAction()
    {
        $error = $this->getAuthenticationError();

        $versions = $this->get('zen.igroove.version')->getVersions();

        return $this->render(
            'ZenIgrooveBundle:Default:login.html.twig',
            array(
                'last_username' => $this->get('request')->getSession()->get(Security::LAST_USERNAME),
                'error' => $error,
                'token' => $this->generateToken(),
                'version' => $versions
            )
        );
    }

    protected function getAuthenticationError()
    {
        if ($this->get('request')->attributes->has(Security::AUTHENTICATION_ERROR)) {
            return $this->get('request')->attributes->get(Security::AUTHENTICATION_ERROR);
        }

        return $this->get('request')->getSession()->get(Security::AUTHENTICATION_ERROR);
    }

    protected function generateToken()
    {
        $token = $this->get('form.csrf_provider')
            ->generateCsrfToken('authenticate');

        return $token;
    }

    protected function currentUserIsStudent() {
        static $isStudent;

        if(!isset($isStudent)) {
            $em = $this->getDoctrine()->getManager();
            $isStudent = false;
            if($this->get('security.token_storage')->getToken() instanceof TokenInterface) {
                $username = $this->get('security.token_storage')->getToken()->getUsername();
                $isStudent = $em->getRepository('ZenIgrooveBundle:Student')->userIsStudent($username);
            }
        }

        return $isStudent;
    }
}
