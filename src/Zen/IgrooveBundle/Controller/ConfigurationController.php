<?php

namespace Zen\IgrooveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Zen\IgrooveBundle\Form\ConfigurationType;
use Symfony\Component\HttpFoundation\Request;
use Zen\IgrooveBundle\Manager\ConfigurationManager;

/**
 * Configuration controller.
 *
 */
class ConfigurationController extends Controller
{
    /**
     * List Configuration.
     *
     * @Route("/config/edit", name="config_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $mikrotiks = $em->getRepository('ZenIgrooveBundle:Mikrotik')->findAll();
        $providers = $em->getRepository('ZenIgrooveBundle:Provider')->findAll();


        $configurationManager = $this->get('zen.igroove.configuration');

        $form = $this->createForm(new ConfigurationType(), $configurationManager, array(
            'action' => $this->generateUrl('config_update'),
            'method' => 'POST',
        ));


        return array(
            'form' => $form->createView(),
            'saved' => $request->get('saved', false),
            'mikrotiks' => $mikrotiks,
            'providers' => $providers
        );
    }

    /**
     * Edit a Configuration.
     *
     * @Route("/config/update", name="config_update")
     * @Method("post")
     * @Template("ZenIgrooveBundle:Configuration:edit.html.twig")
     * @Secure(roles="ROLE_ADMIN")
     */
    public function updateAction(Request $request)
    {

        $configurationManager = $this->get('zen.igroove.configuration');
        $form = $this->createForm(new ConfigurationType(), $configurationManager);


        $form->handleRequest($request);

        if ($form->isValid()) {
            $configurationManager->save();

            return $this->redirect($this->generateUrl('config_edit', array('saved' => true)));
        }

        return array(
            'form' => $form->createView(),
            'saved' => false
        );
    }


    /**
     * Edit a Configuration.
     *
     * @Route("/config/theme", name="config_theme")
     * @Template()
     * @Secure(roles="ROLE_ADMIN")
     */
    public function themeAction(Request $request)
    {
        $cssPath=$this->get('kernel')->getRootDir() . '/../web/css';
        if ($request->get('theme')){
            copy($cssPath.'/themes/'.$request->get('theme'),$cssPath.'/bootstrap.css');
        }
        $list = array();
        $d = dir($cssPath.'/themes');
        while (false !== ($entry = $d->read())) {
            if (substr($entry,-4)=='.css') {
                $list[] = $entry;
            }
        }
        $d->close();

        return array(
            'list' => $list
        );
    }

}
