<?php

namespace Zen\IgrooveBundle\Form;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DeviceIpType extends AbstractType
{
    private $entityManager;

    public function __construct(ObjectManager $entityManager) // Create the constructor if not exist and add the entity manager as first parameter (we will add it later)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ip', 'text')
            ->add('mikrotikPool', 'hidden');

        $builder->get('mikrotikPool')->addModelTransformer(new CallbackTransformer(function ($data) {
            if (null === $data) {
                return '';
            }

            return $data->getId();
        }, function ($data){
            if (null === $data) {
                throw new TransformationFailedException(sprintf(
                    'Pool cannot be null!',
                    $data
                ));
            }

            $pool = $this->entityManager
                ->getRepository('ZenIgrooveBundle:MikrotikPool')
                ->find($data);

            if (null === $pool) {
                throw new TransformationFailedException(sprintf(
                    'An issue with number "%s" does not exist!',
                    $pool
                ));
            }

            return $pool;
        }));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zen\IgrooveBundle\Entity\DeviceIp'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_deviceip';
    }
}
