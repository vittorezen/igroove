<?php

namespace Zen\IgrooveBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Zen\IgrooveBundle\Manager\ConfigurationManager;

class ConfigurationType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $letters = array();
        for ($i = 65; $i < 91; $i++) {
            $letters[chr($i)] = chr($i);
        }

        $builder
            ->add('onlyReception', 'checkbox', array(
                'label' => 'igroove è solo per la gestione degli ospiti e dei MAC address.',
                'required' => false))
            ->add('userCanChangeOwnPassword', 'checkbox', array('label' => 'Gli utenti (studenti e docenti) possono cambiare la loro password.', 'required' => false))
            ->add('showBadge', 'checkbox', array('label' => 'Gli utenti (studenti e docenti) possono stampare il loro badge con username e password. [ver. beta]', 'required' => false))
            ->add('teacherCanResetPassword', 'checkbox', array('label' => 'I docenti possono cambiare la password degli studenti.', 'required' => false))
            ->add('teacherCanSeeList', 'checkbox', array('label' => 'I docenti possono vedere i dati di accesso degli studenti.', 'required' => false))
            ->add('teacherCanEnablePersonalDevice', 'checkbox', array('label' => 'I docenti possono abilitare i dispositivi personali dei ragazzi.', 'required' => false))
            ->add('active_directory_account_suffix', 'text', array('label' => 'Nome corto del dominio:', 'required' => false))
            ->add('active_directory_base_dn', 'text', array('label' => 'LDAP Base DN:', 'required' => false))
            ->add('active_directory_domain_controller', 'text', array('label' => 'Indirizzo IP del domain controller: (È possibile inserirne più di uno, separato da virgola)', 'required' => false))
            ->add('active_directory_admin_username', 'text', array('label' => 'Username amministratore:', 'required' => false))
            ->add('active_directory_admin_password', 'password', array('label' => 'Password: (vuoto per non cambiare)', 'required' => false, 'always_empty' => false, 'attr' => array("autocomplete" => "off")))
            ->add('active_directory_use_ssl', 'checkbox', array('label' => 'Il server è abilitato alla scrittura (SSL).', 'required' => false))
            ->add('active_directory_sync_suffix', 'text', array('label' => 'Carattere finale di sincronizzazione (aggiungere per utilizzare più di un iGroove sullo stesso AD):', 'required' => false, 'trim' => false))
            ->add('active_directory_generated_group_prefix', 'text', array('label' => 'Carattere iniziale dei gruppi creati:', 'required' => false, 'trim' => false))
            ->add('active_directory_generated_teacher_group_prefix', 'text', array('label' => 'Carattere iniziale dei gruppi docenti creati: (vuoto per non creare)', 'required' => false, 'trim' => false))
            ->add('active_directory_generated_provider_group_with_users', 'checkbox', array('label' => 'Inserisci gli utenti al posto dei gruppi nel gruppo provider studenti', 'required' => false))
            ->add('active_directory_generated_teacher_provider_group_with_users', 'checkbox', array('label' => 'Inserisci gli utenti al posto dei gruppi nel gruppo provider docenti', 'required' => false))
            ->add('active_directory_home_folder', 'text', array('label' => 'Home directory degli utenti:', 'required' => false))
            ->add('active_directory_home_drive', 'choice', array('label' => 'Lettera del disco da mappare con la home directory:', 'required' => false, 'choices' => $letters))
            ->add('active_directory_password_min_char', 'integer', array('label' => 'Numero minimo dei caratteri:', 'required' => false))
            ->add('active_directory_password_complexity', 'checkbox', array('label' => 'Necessaria password complessa.', 'required' => false))
            ->add('active_directory_username_style', 'choice', array('label' => 'Sintassi dei nomi utenti creati:', 'required' => false,
                'choices' => ConfigurationManager::$usernameStyles
            ))
            ->add('active_directory_email_for_error', 'text', array('label' => 'Indirizzo email per la notifica degli errori:', 'required' => false))

            ->add('dizda_cloud_backup_username', 'text', array('label' => 'Username Dropbox:', 'required' => false))
            ->add('dizda_cloud_backup_password', 'password', array('label' => 'Password:', 'required' => false, 'always_empty' => false, 'attr' => array("autocomplete" => "off")))

            ->add('wireless_ssid', 'text', array('label' => 'SSID rete guest:', 'required' => false))
            ->add('wireless_guest_network_type', 'text', array('label' => 'Tipo di rete guest:', 'required' => false))
            ->add('wireless_guest_network_rules', 'ckeditor', array('label' => 'Regolamento rete guest:', 'required' => false))
            ->add('wireless_unifi_controller', 'text', array('label' => 'Indirizzo IP:', 'required' => false))
            ->add('wireless_unifi_username', 'text', array('label' => 'Username:', 'required' => false))
            ->add('wireless_unifi_password', 'password', array('label' => 'Password:', 'required' => false, 'always_empty' => false, 'attr' => array("autocomplete" => "off")))
            ->add('wireless_unifi_kick_expired', 'checkbox', array('label' => 'Scollega dal wifi gli studenti ai quali è scaduto il tempo. (Beta)', 'required' => false))
            ->add('wireless_unifi_kick_expired_networks', 'text', array('label' => 'Nome delle reti dalle quali scollegare i ragazzi (separate da virgola, oppure lasciare vuoto per tutte)', 'required' => false))

            ->add('freeradius_realm', 'text', array('label' => 'Realm per gli studenti', 'required' => false))
            ->add('guest_realm', 'text', array('label' => 'Realm per gli ospiti', 'required' => false))

            ->add('showStats', 'checkbox', array('label' => 'Visualizza statistiche', 'required' => false))
            ->add('wirelessHotspotServer', 'text', array('label' => 'Nome del server hotspot Mikrotik per gli utenti wireless', 'required' => false))

            ->add('save', 'submit', array('label' => 'Cambia la configurazione di igroove'));;
    }

    public function getName()
    {
        return 'configuration';
    }


}

