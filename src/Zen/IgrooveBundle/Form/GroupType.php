<?php

namespace Zen\IgrooveBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GroupType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', ['label' => "Nome", 'disabled' => !$builder->getData()->getManageManually()])
            ->add('radiusVlan', 'integer', ['label' => "vLan Dinamica (Opzionale)", 'required' => false, 'disabled' => $builder->getData()->getManageManually()])
            ->add('sector', 'entity', [ 'label' => "Settore (Opzionale)", 'class' => "Zen\IgrooveBundle\Entity\Sector",
                                                    'choice_label' => 'name', 'required' => false, 'disabled' => !$builder->getData()->getManageManually()]) //@todo filtrare per provider
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zen\IgrooveBundle\Entity\Group'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'zen_igroovebundle_group';
    }
}
