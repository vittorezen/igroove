<?php

namespace Zen\IgrooveBundle\Handler;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Http\HttpUtils;
use Zen\IgrooveBundle\Exception\NeedPasswordChangeAuthenticationException;


class ForcePasswordChangeAuthenticationFailureHandler extends DefaultAuthenticationFailureHandler {

    protected $router;

    public function __construct(HttpKernelInterface $httpKernel, HttpUtils $httpUtils, Router $router, LoggerInterface $logger = NULL, array $options = array()) {
        parent::__construct($httpKernel, $httpUtils, $options, $logger);
        $this->router = $router;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
        if($exception instanceof NeedPasswordChangeAuthenticationException) {
            $request->getSession()->set('forcePasswordChangeUser', $exception->getLdapUser());
            return $this->httpUtils->createRedirectResponse($request, $this->router->generate('forceChangeMyPassword'));
        } elseif($exception->getPrevious() instanceof NeedPasswordChangeAuthenticationException) {
            $request->getSession()->set('forcePasswordChangeUser', $exception->getPrevious()->getLdapUser());
            return $this->httpUtils->createRedirectResponse($request, $this->router->generate('forceChangeMyPassword'));
        }

        return parent::onAuthenticationFailure($request, $exception);
    }
}