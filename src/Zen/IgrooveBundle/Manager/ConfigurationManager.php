<?php

namespace Zen\IgrooveBundle\Manager;

use Symfony\Component\Yaml\Yaml;
use Zen\IgrooveBundle\Entity\ConfigurationStorage;

class ConfigurationManager
{
    protected $em;
    protected $cacheFile;
    protected $onlyReception;
    protected $userCanChangeOwnPassword;
    protected $teacherCanResetPassword;
    protected $teacherCanSeeList;
    protected $teacherCanEnablePersonalDevice;
    protected $showBadge;
    protected $showStats;
    protected $wirelessHotspotServer;

    protected $dizda_cloud_backup_username;
    protected $dizda_cloud_backup_password;

    protected $active_directory_account_suffix;
    protected $active_directory_base_dn;
    protected $active_directory_domain_controller;
    protected $active_directory_admin_username;
    protected $active_directory_admin_password;
    protected $active_directory_use_ssl;
    protected $active_directory_sync_suffix;
    protected $active_directory_generated_group_prefix;
    protected $active_directory_generated_teacher_group_prefix;
    protected $active_directory_generated_provider_group_with_users;
    protected $active_directory_generated_teacher_provider_group_with_users;
    protected $active_directory_home_folder;
    protected $active_directory_home_drive;
    protected $active_directory_password_min_char;
    protected $active_directory_password_complexity;
    protected $active_directory_username_style;
    protected $active_directory_email_for_error;

    protected $wireless_ssid;
    protected $wireless_guest_network_type;
    protected $wireless_guest_network_rules;
    protected $wireless_unifi_controller;
    protected $wireless_unifi_username;
    protected $wireless_unifi_password;
    protected $wireless_unifi_kick_expired;
    protected $wireless_unifi_kick_expired_networks;
    protected $freeradius_realm;
    protected $guest_realm;

    public static $usernameStyles = [
        'COGNOME.NOME' => 'cognome.nome',
        'NOME.COGNOME' => 'nome.cognome',
        'INIZIALENOME.COGNOME' => 'inizialeNome.cognome',
        'INIZIALENOMECOGNOME' => 'inizialeNomeCognome',
        'COGNOME.PRIMONOME' => 'cognome.primoNome',
        'PRIMONOME.COGNOME' => 'primoNome.cognome',
        'NOMEUTENTE-DA-EMAIL' => 'nomeUtente da email'
    ];

    public function __construct($em, $kernelRootDir)
    {
        $this->em = $em;
        $this->cacheFile = $kernelRootDir . '/../src/Zen/IgrooveBundle/Resources/config/configCache.yml';
        $called = get_called_class();
        $classVars = array_keys(get_class_vars($called));

        $cachedData = [];
        if(file_exists($this->cacheFile)) {
            $cachedData = Yaml::parse(file_get_contents($this->cacheFile));
        }

        $repositoryConfiguration = $em->getRepository('ZenIgrooveBundle:ConfigurationStorage');

        foreach ($classVars as $key) {
            if ($key == 'em' || $key == "usernameStyles" || $key == "cacheFile") {
                continue;
            }

            if(key_exists($key, $cachedData)) {
                $this->$key = $cachedData[$key];
                continue;
            }

            $entity = $repositoryConfiguration->find($key);
            if ($entity) {
                $this->$key = $entity->getValue();
            }
        }
    }

    public function setSingleKeyConfiguration($key, $value)
    {
        $repositoryConfiguration = $this->em->getRepository('ZenIgrooveBundle:ConfigurationStorage');

        $entity = $repositoryConfiguration->find( $key);
        if (!$entity) {
            $entity->setKey($key);
            $this->em->persist($entity);
        }
        $entity->setValue($value);
        $this->em->flush();

        $this->saveInCache();
    }


    public function save()
    {
        $called = get_called_class();
        $classVars = array_keys(get_class_vars($called));

        $dataToCache = [];
        $repositoryConfiguration = $this->em->getRepository('ZenIgrooveBundle:ConfigurationStorage');

        foreach ($classVars as $key) {
            if ($key == 'em' || $key == "usernameStyles"|| $key == "cacheFile") {
                continue;
            }

            $dataToCache[$key] = $this->$key;

            $entity = $repositoryConfiguration->find( $key);
            if (!$entity) {
                $entity = new ConfigurationStorage();
                $entity->setKey($key);
                $this->em->persist($entity);
            }
            $entity->setValue($this->$key);

        }
        $this->em->flush();

        $this->saveInCache($dataToCache);
    }

    protected function saveInCache($dataToCache=null)
    {
        if($dataToCache === null || !is_array($dataToCache)) {
            $called = get_called_class();
            $classVars = array_keys(get_class_vars($called));

            foreach ($classVars as $key) {
                if ($key == 'em' || $key == "usernameStyles" || $key == "cacheFile") {
                    continue;
                }

                $dataToCache[$key] = $this->$key;
            }
        }

        file_put_contents($this->cacheFile, Yaml::dump($dataToCache));
    }

    /**
     * Set onlyReception
     *
     * @param  string $onlyReception
     * @return ConfigurationManager
     */
    public function setOnlyReception($onlyReception)
    {
        $this->onlyReception = $onlyReception;

        return $this;
    }

    /**
     * Get onlyReception
     *
     * @return string
     */
    public function getOnlyReception()
    {
        return (bool)$this->onlyReception;
    }


    /**
     * Set onlyReception
     *
     * @param  string $onlyReception
     * @return ConfigurationManager
     */
    public function setShowBadge($showBadge)
    {
        $this->showBadge = $showBadge;

        return $this;
    }

    /**
     * Get onlyReception
     *
     * @return string
     */
    public function getShowBadge()
    {
        return (bool)$this->showBadge;
    }

    /**
     * Set userCanChangeOwnPassword
     *
     * @param  string $userCanChangeOwnPassword
     * @return ConfigurationManager
     */
    public function setUserCanChangeOwnPassword($userCanChangeOwnPassword)
    {
        $this->userCanChangeOwnPassword = $userCanChangeOwnPassword;

        return $this;
    }

    /**
     * Get userCanChangeOwnPassword
     *
     * @return bool
     */
    public function getUserCanChangeOwnPassword()
    {
        return (bool)$this->userCanChangeOwnPassword;
    }

    /**
     * Set teacherCanResetPassword
     *
     * @param  string $teacherCanResetPassword
     * @return ConfigurationManager
     */
    public function setTeacherCanResetPassword($teacherCanResetPassword)
    {
        $this->teacherCanResetPassword = $teacherCanResetPassword;

        return $this;
    }

    /**
     * Get teacherCanResetPassword
     *
     * @return bool
     */
    public function getTeacherCanResetPassword()
    {
        return (bool)$this->teacherCanResetPassword;
    }

    /**
     * Set teacherCanSeeList
     *
     * @param  string $teacherCanSeeList
     * @return ConfigurationManager
     */
    public function setTeacherCanSeeList($teacherCanSeeList)
    {
        $this->teacherCanSeeList = $teacherCanSeeList;

        return $this;
    }

    /**
     * Get teacherCanSeeList
     *
     * @return bool
     */
    public function getTeacherCanSeeList()
    {
        return (bool)$this->teacherCanSeeList;
    }

    /**
     * @return bool
     */
    public function getTeacherCanEnablePersonalDevice() {
        return (bool)$this->teacherCanEnablePersonalDevice;
    }

    /**
     * @param bool $teacherCanEnablePersonalDevice
     */
    public function setTeacherCanEnablePersonalDevice($teacherCanEnablePersonalDevice) {
        $this->teacherCanEnablePersonalDevice = (bool)$teacherCanEnablePersonalDevice;
    }



    /**
     * Set dizda_cloud_backup_username
     *
     * @param  string $dizdaCloudBackupUsername
     * @return ConfigurationManager
     */
    public function setDizdaCloudBackupUsername($dizdaCloudBackupUsername)
    {
        $this->dizda_cloud_backup_username = $dizdaCloudBackupUsername;

        return $this;
    }

    /**
     * Get dizda_cloud_backup_username
     *
     * @return string
     */
    public function getDizdaCloudBackupUsername()
    {
        return $this->dizda_cloud_backup_username;
    }

    /**
     * Set dizda_cloud_backup_password
     *
     * @param  string $dizdaCloudBackupPassword
     * @return ConfigurationManager
     */
    public function setDizdaCloudBackupPassword($dizdaCloudBackupPassword)
    {
        if(is_null($dizdaCloudBackupPassword))
            return $this;

        $this->dizda_cloud_backup_password = $dizdaCloudBackupPassword;

        return $this;
    }

    /**
     * Get dizda_cloud_backup_password
     *
     * @return string
     */
    public function getDizdaCloudBackupPassword()
    {
        return $this->dizda_cloud_backup_password;
    }

    /**
     * setActive_directory_account_suffix
     *
     * @param  string $activeDirectoryAccountSuffix
     * @return ConfigurationManager
     */
    public function setActiveDirectoryAccountSuffix($activeDirectoryAccountSuffix)
    {
        $this->active_directory_account_suffix = $activeDirectoryAccountSuffix;

        return $this;
    }

    /**
     * getActive_directory_account_suffix
     *
     * @return string
     */
    public function getActiveDirectoryAccountSuffix()
    {
        if ((strlen($this->active_directory_account_suffix) > 0) and (substr(
                    $this->active_directory_account_suffix,
                    0,
                    1
                ) != "@")
        ) {
            $this->active_directory_account_suffix = '@' . $this->active_directory_account_suffix;
        }

        return $this->active_directory_account_suffix;

    }

    /**
     * setActive_directory_base_dn
     *
     * @param  string $activeDirectoryBaseDn
     * @return ConfigurationManager
     */
    public function setActiveDirectoryBaseDn($activeDirectoryBaseDn)
    {
        $this->active_directory_base_dn = $activeDirectoryBaseDn;

        return $this;
    }

    /**
     * getActive_directory_base_dn
     *
     * @return string
     */
    public function getActiveDirectoryBaseDn()
    {
        return $this->active_directory_base_dn;
    }

    /**
     * setActive_directory_domain_controller
     *
     * @param  string $activeDirectoryDomainController
     * @return ConfigurationManager
     */
    public function setActiveDirectoryDomainController($activeDirectoryDomainController)
    {
        $this->active_directory_domain_controller = $activeDirectoryDomainController;

        return $this;
    }

    /**
     * getActive_directory_domain_controller
     *
     * @return string
     */
    public function getActiveDirectoryDomainController()
    {
        return $this->active_directory_domain_controller;
    }

    /**
     * setActive_directory_admin_username
     *
     * @param  string $activeDirectoryAdminUsername
     * @return ConfigurationManager
     */
    public function setActiveDirectoryAdminUsername($activeDirectoryAdminUsername)
    {
        $this->active_directory_admin_username = $activeDirectoryAdminUsername;

        return $this;
    }

    /**
     * getActive_directory_admin_username
     *
     * @return string
     */
    public function getActiveDirectoryAdminUsername()
    {
        return $this->active_directory_admin_username;
    }

    /**
     * setActive_directory_admin_password
     *
     * @param  string $activeDirectoryAdminPassword
     * @return ConfigurationManager
     */
    public function setActiveDirectoryAdminPassword($activeDirectoryAdminPassword)
    {
        if(is_null($activeDirectoryAdminPassword))
            return $this;

        $this->active_directory_admin_password = $activeDirectoryAdminPassword;

        return $this;
    }

    /**
     * getActive_directory_admin_password
     *
     * @return string
     */
    public function getActiveDirectoryAdminPassword()
    {
        return $this->active_directory_admin_password;
    }

    /**
     * setActive_directory_use_ssl
     *
     * @param  string $activeDirectoryUseSsl
     * @return ConfigurationManager
     */
    public function setActiveDirectoryUseSsl($activeDirectoryUseSsl)
    {
        $this->active_directory_use_ssl = $activeDirectoryUseSsl;

        return $this;
    }

    /**
     * getActive_directory_use_ssl
     *
     * @return string
     */
    public function getActiveDirectoryUseSsl()
    {
        return (bool)$this->active_directory_use_ssl;
    }

    /**
     * @return string
     */
    public function getActiveDirectorySyncSuffix() {
        return $this->active_directory_sync_suffix;
    }

    /**
     * @param string $active_directory_sync_suffix
     */
    public function setActiveDirectorySyncSuffix($active_directory_sync_suffix) {
        $this->active_directory_sync_suffix = $active_directory_sync_suffix;
    }

    /**
     * setActive_directory_generated_group_prefix
     *
     * @param  string $activeDirectoryGeneratedGroupPrefix
     * @return ConfigurationManager
     */
    public function setActiveDirectoryGeneratedGroupPrefix($activeDirectoryGeneratedGroupPrefix)
    {
        $this->active_directory_generated_group_prefix = $activeDirectoryGeneratedGroupPrefix;

        return $this;
    }

    /**
     * getActive_directory_generated_group_prefix
     *
     * @return string
     */
    public function getActiveDirectoryGeneratedGroupPrefix()
    {
        return $this->active_directory_generated_group_prefix;
    }

    /**
     * @return string
     */
    public function getActiveDirectoryGeneratedTeacherGroupPrefix() {
        return $this->active_directory_generated_teacher_group_prefix;
    }

    /**
     * @param string $active_directory_generated_teacher_group_prefix
     * @return ConfigurationManager
     */
    public function setActiveDirectoryGeneratedTeacherGroupPrefix($active_directory_generated_teacher_group_prefix) {
        $this->active_directory_generated_teacher_group_prefix = $active_directory_generated_teacher_group_prefix;
        return $this;
    }

    /**
     * @return bool
     */
    public function getActiveDirectoryGeneratedProviderGroupWithUsers() {
        return (bool)$this->active_directory_generated_provider_group_with_users;
    }

    /**
     * @param bool $active_directory_generated_provider_group_with_users
     */
    public function setActiveDirectoryGeneratedProviderGroupWithUsers($active_directory_generated_provider_group_with_users) {
        $this->active_directory_generated_provider_group_with_users = (bool)$active_directory_generated_provider_group_with_users;
    }

    /**
     * @return bool
     */
    public function getActiveDirectoryGeneratedTeacherProviderGroupWithUsers() {
        return (bool)$this->active_directory_generated_teacher_provider_group_with_users;
    }

    /**
     * @param bool $active_directory_generated_teacher_provider_group_with_users
     */
    public function setActiveDirectoryGeneratedTeacherProviderGroupWithUsers($active_directory_generated_teacher_provider_group_with_users) {
        $this->active_directory_generated_teacher_provider_group_with_users = (bool)$active_directory_generated_teacher_provider_group_with_users;
    }

    /**
     * setActive_directory_home_folder
     *
     * @param  string $activeDirectoryHomeFolder
     * @return ConfigurationManager
     */
    public function setActiveDirectoryHomeFolder($activeDirectoryHomeFolder)
    {
        $this->active_directory_home_folder = $activeDirectoryHomeFolder;

        return $this;
    }

    /**
     * getActive_directory_home_folder
     *
     * @return string
     */
    public function getActiveDirectoryHomeFolder()
    {
        return $this->active_directory_home_folder;
    }

    /**
     * setActive_directory_home_drive
     *
     * @param  string $activeDirectoryHomeDrive
     * @return ConfigurationManager
     */
    public function setActiveDirectoryHomeDrive($activeDirectoryHomeDrive)
    {
        $this->active_directory_home_drive = $activeDirectoryHomeDrive;

        return $this;
    }

    /**
     * getActive_directory_home_drive
     *
     * @return string
     */
    public function getActiveDirectoryHomeDrive()
    {
        return substr($this->active_directory_home_drive, 0, 1);
    }

    /**
     * setActive_directory_password_min_char
     *
     * @param  string $activeDirectoryPasswordMinChar
     * @return ConfigurationManager
     */
    public function setActiveDirectoryPasswordMinChar($activeDirectoryPasswordMinChar)
    {
        $this->active_directory_password_min_char = $activeDirectoryPasswordMinChar;

        return $this;
    }

    /**
     * getActive_directory_password_min_char
     *
     * @return string
     */
    public function getActiveDirectoryPasswordMinChar()
    {
        return (int)$this->active_directory_password_min_char;
    }

    /**
     * setActive_directory_password_complexity
     *
     * @param  string $activeDirectoryPasswordComplexity
     * @return ConfigurationManager
     */
    public function setActiveDirectoryPasswordComplexity($activeDirectoryPasswordComplexity)
    {
        $this->active_directory_password_complexity = $activeDirectoryPasswordComplexity;

        return $this;
    }

    /**
     * getActive_directory_password_complexity
     *
     * @return string
     */
    public function getActiveDirectoryPasswordComplexity()
    {
        return (bool)$this->active_directory_password_complexity;
    }

    /**
     * setActive_directory_username_style
     *
     * @param  string $activeDirectoryUsernameStyle
     * @return ConfigurationManager
     */
    public function setActiveDirectoryUsernameStyle($activeDirectoryUsernameStyle)
    {
        $this->active_directory_username_style = $activeDirectoryUsernameStyle;

        return $this;
    }

    /**
     * getActive_directory_username_style
     *
     * @return string
     */
    public function getActiveDirectoryUsernameStyle()
    {
        return $this->active_directory_username_style;
    }


    /**
     * setActiveDirectoryEmailForError
     *
     * @param  string $active_directory_email_for_error
     * @return ConfigurationManager
     */
    public function setActiveDirectoryEmailForError($active_directory_email_for_error)
    {
        $this->active_directory_email_for_error = $active_directory_email_for_error;

        return $this;
    }

    /**
     * getActiveDirectoryEmailForError
     *
     * @return string
     */
    public function getActiveDirectoryEmailForError()
    {
        return $this->active_directory_email_for_error;
    }


    /**
     * @return array
     * @throws \Exception
     */
    public function getActiveDirectoryConfiguration()
    {
        if (strlen($this->active_directory_account_suffix) < 1) {
            throw new \Exception("Active Directory Suffix must be specified!");
        }

        return array(
            'account_suffix' => $this->getActiveDirectoryAccountSuffix(),
            'base_dn' => $this->getActiveDirectoryBaseDn(),
            'domain_controllers' => explode(",",(string)$this->getActiveDirectoryDomainController()),
            'admin_username' => $this->getActiveDirectoryAdminUsername(),
            'admin_password' => $this->getActiveDirectoryAdminPassword(),
            'use_ssl' => $this->getActiveDirectoryUseSsl(),
            'sync_suffix' => $this->getActiveDirectorySyncSuffix(),
            'real_primarygroup' => true,
            'generated_group_prefix' => $this->getActiveDirectoryGeneratedGroupPrefix(),
            'generated_group_teacher_prefix' => $this->getActiveDirectoryGeneratedTeacherGroupPrefix(),
            'home_folder' => $this->getActiveDirectoryHomeFolder(),
            'home_drive' => $this->getActiveDirectoryHomeDrive(),
            'password' => array(
                'min_char' => $this->getActiveDirectoryPasswordMinChar(),
                'complexity' => $this->getActiveDirectoryPasswordComplexity()
            ),
            'username_style' => $this->getActiveDirectoryUsernameStyle(),
            'email_for_error' => $this->getActiveDirectoryEmailForError()
        );

    }

    /**
     *
     * @return array
     */
    public function getMenuConfiguration($isStudent)
    {
        return array(

            'onlyReception' => $this->getOnlyReception(),
            'userCanChangeOwnPassword' => $this->getUserCanChangeOwnPassword(),
            'teacherCanResetPassword' => $this->getTeacherCanResetPassword(),
            'teacherCanSeeList' => $this->getTeacherCanSeeList(),
            'showBadge' => $this->getShowBadge() && $isStudent

        );
    }


    public function setWirelessSsid($wirelessSsid)
    {
        $this->wireless_ssid = $wirelessSsid;
    }

    /**
     * getWirelessSsid
     *
     * @return string
     */
    public function getWirelessSsid()
    {
        return $this->wireless_ssid;
    }

    /**
     * @return string
     */
    public function getWirelessGuestNetworkType() {
        return $this->wireless_guest_network_type;
    }

    /**
     * @param string $wireless_guest_network_type
     */
    public function setWirelessGuestNetworkType($wireless_guest_network_type) {
        $this->wireless_guest_network_type = $wireless_guest_network_type;
    }

    /**
     * @return string
     */
    public function getWirelessGuestNetworkRules() {
        return $this->wireless_guest_network_rules;
    }

    /**
     * @param string $wireless_guest_network_rules
     */
    public function setWirelessGuestNetworkRules($wireless_guest_network_rules) {
        $this->wireless_guest_network_rules = $wireless_guest_network_rules;
    }



    public function setWirelessUnifiController($wirelessUnifiController)
    {
        $this->wireless_unifi_controller = $wirelessUnifiController;
    }

    /**
     * getWirelessUnifiController
     *
     * @return string
     */
    public function getWirelessUnifiController()
    {
        return $this->wireless_unifi_controller;
    }

    public function setwirelessUnifiUsername($wirelessUnifiUsername)
    {
        $this->wireless_unifi_username = $wirelessUnifiUsername;
    }

    /**
     * getWirelessUnifiUsername
     *
     * @return string
     */
    public function getWirelessUnifiUsername()
    {
        return $this->wireless_unifi_username;
    }

    public function setWirelessUnifiPassword($wirelessUnifiPassword)
    {
        if(is_null($wirelessUnifiPassword))
            return;

        $this->wireless_unifi_password = $wirelessUnifiPassword;
    }

    /**
     * getWirelessUnifiPassword
     *
     * @return string
     */
    public function getWirelessUnifiPassword()
    {
        return $this->wireless_unifi_password;
    }

    /**
     * getWirelessUnifiKickExpired
     *
     * @return bool
     */
    public function getWirelessUnifiKickExpired()
    {
        return (bool)$this->wireless_unifi_kick_expired;
    }

    public function setWirelessUnifiKickExpired($wirelessUnifiKickExpired)
    {
        $this->wireless_unifi_kick_expired = (bool)$wirelessUnifiKickExpired;
    }

    /**
     * getWirelessUnifiKickExpiredNetworks
     *
     * @return string
     */
    public function getWirelessUnifiKickExpiredNetworks()
    {
        return $this->wireless_unifi_kick_expired_networks;
    }

    public function setWirelessUnifiKickExpiredNetworks($wirelessUnifiKickExpiredNetworks)
    {
        $this->wireless_unifi_kick_expired_networks = $wirelessUnifiKickExpiredNetworks;
    }

    /**
     * getFreeradiusRealm
     *
     * @return string
     */
    public function getFreeradiusRealm()
    {
        return $this->freeradius_realm;
    }

    public function setFreeradiusRealm($freeradiusRealm)
    {
        $this->freeradius_realm = $freeradiusRealm;
    }

    public function getGuestRealm()
    {
        return $this->guest_realm;
    }

    public function setGuestRealm($guestRealm)
    {
        $this->guest_realm = $guestRealm;
    }



    /**
     * getShowStats
     *
     * @return string
     */
    public function getShowStats()
    {
        return (bool)$this->showStats;
    }

    public function setShowStats($showStats)
    {
        $this->showStats = $showStats;
    }


    /**
     * getShowStats
     *
     * @return string
     */
    public function getWirelessHotspotServer()
    {
        return $this->wirelessHotspotServer;
    }

    public function setWirelessHotspotServer($wirelessHotspotServer)
    {
        $this->wirelessHotspotServer = $wirelessHotspotServer;
    }

}
