<?php

namespace Zen\IgrooveBundle\Manager;

use Psr\Log\LoggerInterface;
use Zen\IgrooveBundle\Exception\ConnectionException;
use Zen\IgrooveBundle\Exception\LoggedException;


class GoogleApps
{
    /**
     * @var \Google_Service_Directory
     */
    protected $service;
    protected $domain;
    protected $clientId;
    protected $clientSecret;
    protected $kernelRootDir;
    protected $ouBasePath;
    protected $logger;
    protected $usersListCache = [];
    protected $groupsListCache = [];
    protected $OUsListCache = [];

    public function __construct($domain, $clientId, $clientSecret, $ouBasePath = "/", $kernelRootDir, LoggerInterface $logger)
    {
        $this->domain = $domain;
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->kernelRootDir = $kernelRootDir;
        $this->logger = $logger;
        $this->ouBasePath = substr($ouBasePath, -1, 1) != "/" ? $ouBasePath . "/" : $ouBasePath;
        $this->authorize();
    }

    /**
     * Check if user exist on GApps
     *
     * @param string $email
     * @param bool $cache store the users list in cache, to check multiple user faster
     * @return bool
     * @throws \Exception
     */
    public function userExists($email, $cache = false)
    {
        if ($email == "") {
            $this->logger->error("GOOGLEAPPS: Empty email to check");
            throw new LoggedException("Empty email to check for user in GApps");
        }

        if (strpos($email, '@') === false) {
            $email = $email . '@' . $this->domain;
        }

        if ($cache) {
            $usersList = $this->getUsersListCached();
            if (isset($usersList[$email]))
                return true;
            else
                return false;
        }

        try {
            $user = $this->service->users->get($email);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->info("GOOGLEAPPS: User {$email} not found: " . $gse->getMessage());
            return false;
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }

        return $user instanceof \Google_Service_Directory_User;
    }

    /**
     * Get the list of all the Users in Gapps Domain
     *
     * @return array|\Google_Service_Directory_User[]
     */
    public function getUsersList()
    {
        $users = [];

        $nextPageToken = "";
        do {
            try {
                $pagedUsersAnswer = $this->service->users->listUsers(['maxResults' => 500, 'domain' => $this->domain, 'pageToken' => $nextPageToken]);
                if (count($pagedUsersAnswer->getUsers()) > 0) {
                    foreach ($pagedUsersAnswer->getUsers() as $user) {
                        $users[$user->getPrimaryEmail()] = $user;
                    }
                }

                $nextPageToken = $pagedUsersAnswer->getNextPageToken();
            } catch (\Google_Service_Exception $gse) {
                $this->logger->error("GOOGLEAPPS: Error getting the users list: " . $gse->getMessage());
                return [];
            } catch (\Google_IO_Exception $gioe) {
                $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
                return [];
            }
        } while ($nextPageToken != "");

        return $users;
    }

    /**
     * Cache the list of all the User in Gapps, and return it
     *
     * @param bool $reset Reset the current cache
     * @return array|\Google_Service_Directory_User[]
     */
    public function getUsersListCached($reset = false)
    {
        if (empty($this->usersListCache) || $reset) {
            $this->usersListCache = $this->getUsersList();
        }

        return $this->usersListCache;
    }

    /**
     * Check if an email is in the current domain
     *
     * @param string $email
     * @return bool
     */
    public function emailInDomain($email)
    {
        return substr($email, -strlen("@" . $this->domain)) == "@" . $this->domain;
    }

    /**
     * Create a new User on Gapps domain
     *
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @param string $password
     * @throws \Exception
     */
    public function createUser($email, $firstname, $lastname, $password = "no-password")
    {
        if ($firstname == "" || $lastname == "" || $email == "" || $password == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty user {$email} ({$lastname} {$firstname}) data to create");
            throw new LoggedException("Invalid or empty GApps user {$email} ({$lastname} {$firstname}) data to create");
        }

        if (strpos($email, '@') === false) {
            $email = $email . '@' . $this->domain;
        }

        if ($this->userExists($email)) {
            return;
        }

        $userInstance = new \Google_Service_Directory_User();
        $nameInstance = new \Google_Service_Directory_UserName();
        $nameInstance->setGivenName($firstname);
        $nameInstance->setFamilyName($lastname);
        $userInstance->setName($nameInstance);
        $userInstance->setHashFunction("MD5");
        $userInstance->setPrimaryEmail($email);
        $userInstance->setPassword(hash("md5", $password));
        $userInstance->setChangePasswordAtNextLogin(false);
        try {
            $this->service->users->insert($userInstance);
            $this->logger->info("GOOGLEAPPS: User {$email} created successfully");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error creating user {$email}: " . $gse->getMessage());
            throw new LoggedException("Error creating GApps user {$email}: " . $gse->getMessage(), 0, $gse);
        }
    }

    /**
     * Rename the Gapps user with $previousEmail to $email, changing the first/last name if required
     *
     * @param string $email
     * @param string $previousEmail
     * @param string $firstname
     * @param string $lastname
     * @throws \Exception
     */
    public function renameUser($email, $previousEmail, $firstname = "", $lastname = "")
    {
        if ($email == "" || $previousEmail == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty user to rename {$previousEmail} => {$email}");
            throw new LoggedException("Invalid or empty GApps user to rename {$previousEmail} => {$email}");
        }

        if (strpos($email, '@') === false) {
            $email = $email . '@' . $this->domain;
        }

        if (strpos($previousEmail, '@') === false) {
            $previousEmail = $previousEmail . '@' . $this->domain;
        }

        try {
            $user = $this->service->users->get($previousEmail);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->info("GOOGLEAPPS: User {$email} not found: " . $gse->getMessage());
            throw new LoggedException("Error finding GApps user {$email} to rename: " . $gse->getMessage(), 0, $gse);
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }

        if (!$user instanceof \Google_Service_Directory_User) {
            $this->logger->error("GOOGLEAPPS: Invalid user {$previousEmail} to rename");
            throw new LoggedException("Invalid GApps user {$previousEmail} to rename");
        }

        $user->setPrimaryEmail($email);

        if ($firstname != "" || $lastname != "") {
            $nameInstance = new \Google_Service_Directory_UserName();
            $nameInstance->setGivenName($firstname);
            $nameInstance->setFamilyName($lastname);
            $user->setName($nameInstance);
        }

        try {
            $this->service->users->update($previousEmail, $user);
            $this->logger->info("GOOGLEAPPS: User {$email} renamed successfully from {$previousEmail}");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error renaming user {$previousEmail} => {$email}: " . $gse->getMessage());
            throw new LoggedException("Error renanaming GApps user {$previousEmail} => {$email}: " . $gse->getMessage(), 0, $gse);
        }
    }

    /**
     * Update the user $email data
     *
     * @param string $email
     * @param string $firstname
     * @param string $lastname
     * @throws \Exception
     */
    public function updateUser($email, $firstname = "", $lastname = "")
    {
        if ($email == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty user {$email} data to update");
            throw new LoggedException("Invalid or empty GApps user {$email} to update");
        }

        if (strpos($email, '@') === false) {
            $email = $email . '@' . $this->domain;
        }

        try {
            $user = $this->service->users->get($email);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->info("GOOGLEAPPS: User {$email} not found: " . $gse->getMessage());
            throw new LoggedException("Error finding GApps user {$email} to update: " . $gse->getMessage(), 0, $gse);
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }

        if (!$user instanceof \Google_Service_Directory_User) {
            $this->logger->error("GOOGLEAPPS: Invalid user {$email}");
            throw new LoggedException("Invalid GApps user {$email} to update");
        }

        if ($firstname != "" || $lastname != "") {
            $nameInstance = new \Google_Service_Directory_UserName();
            $nameInstance->setGivenName($firstname);
            $nameInstance->setFamilyName($lastname);
            $user->setName($nameInstance);
        }

        try {
            $this->service->users->update($email, $user);
            $this->logger->info("GOOGLEAPPS: User {$email} updated successfully");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error updating user {$email}: " . $gse->getMessage());
            throw new LoggedException("Error updating GApps user {$email}: " . $gse->getMessage(), 0, $gse);
        }
    }

    /**
     * Remove a user from Gapps
     *
     * @param string $email
     * @throws \Exception
     */
    public function removeUser($email)
    {
        if ($email == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty user {$email} data to remove");
            throw new LoggedException("Invalid or empty GApps user {$email} to remove");
        }

        if (strpos($email, '@') === false) {
            $email = $email . '@' . $this->domain;
        }

        try {
            $this->service->users->delete($email);
            $this->logger->info("GOOGLEAPPS: User {$email} deleted successfully");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error deleting user {$email}: " . $gse->getMessage());
            throw new LoggedException("Error removing GApps user {$email}: " . $gse->getMessage(), 0, $gse);
        }
    }

    /**
     * Reset the password of a Gapps user
     *
     * @param string $email
     * @param string $password
     * @throws \Exception
     */
    public function resetUserPassword($email, $password = "no-password")
    {
        if ($email == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty user {$email} data to reset password");
            throw new LoggedException("Invalid or empty GApps user {$email} to reset password");
        }

        if (strpos($email, '@') === false) {
            $email = $email . '@' . $this->domain;
        }

        try {
            $user = $this->service->users->get($email);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->info("GOOGLEAPPS: User {$email} not found: " . $gse->getMessage());
            throw new LoggedException("Error finding GApps user {$email} to reset password: " . $gse->getMessage(), 0, $gse);
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }

        if (!$user instanceof \Google_Service_Directory_User) {
            $this->logger->error("GOOGLEAPPS: Invalid GApps user {$email} to reset password");
            throw new LoggedException("Invalid GApps user {$email} to reset password");
        }

        $user->setHashFunction("MD5");
        $user->setPassword(hash("md5", $password));
        $user->setChangePasswordAtNextLogin(false);

        try {
            $this->service->users->update($email, $user);
            $this->logger->info("GOOGLEAPPS: Password for user {$email} resetted succesfully");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->info("GOOGLEAPPS: Error resetting user {$email} password: " . $gse->getMessage());
            throw new LoggedException("Error resetting GApps user {$email}: " . $gse->getMessage(), 0, $gse);
        }
    }


    /**
     * RE-enable a Gapps user
     *
     * @param string $email
     * @throws \Exception
     */
    public function setUnsuspendedUser($email)
    {
        if ($email == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty user {$email} data to reset password");
            throw new LoggedException("Invalid or empty GApps user {$email} to reset password");
        }

        if (strpos($email, '@') === false) {
            $email = $email . '@' . $this->domain;
        }

        try {
            $user = $this->service->users->get($email);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->info("GOOGLEAPPS: User {$email} not found: " . $gse->getMessage());
            throw new LoggedException("Error finding GApps user {$email} to reset password: " . $gse->getMessage(), 0, $gse);
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }

        if (!$user instanceof \Google_Service_Directory_User) {
            $this->logger->error("GOOGLEAPPS: Invalid GApps user {$email} to reset password");
            throw new LoggedException("Invalid GApps user {$email} to reset password");
        }

        $user->setSuspended(false);

        try {
            $this->service->users->update($email, $user);
            $this->logger->info("GOOGLEAPPS: setSuspended(false) for user {$email} setted succesfully");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->info("GOOGLEAPPS: Error setSuspended(false) user {$email} password: " . $gse->getMessage());
            throw new LoggedException("Error resetting GApps user {$email}: " . $gse->getMessage(), 0, $gse);
        }
    }

    /**
     * CHeck if a group name exists in Gapps domain
     *
     * @param string $name
     * @param bool $cache store the groups list in cache, to check multiple group faster
     * @throws \Exception
     * @return bool
     */
    public function groupExists($name, $cache = false)
    {
        $email = $this->getCleanedGroupName($name, true);

        if ($cache) {
            $groupsList = $this->getGroupsListCached();
            if (isset($groupsList[$email]))
                return true;
            else
                return false;
        }

        try {
            $user = $this->service->groups->get($email);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->info("GOOGLEAPPS: Group {$email} not found: " . $gse->getMessage());
            return false;
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }

        return $user instanceof \Google_Service_Directory_Group;
    }

    /**
     * Get the list of all the groups in Gapps domain
     *
     * @return array|\Google_Service_Directory_Group[]
     */
    public function getGroupsList()
    {
        try {
            $groups = $this->service->groups->listGroups(['domain' => $this->domain]);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error getting the groups list: " . $gse->getMessage());
            return [];
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            return [];
        }

        $groupsList = array();
        foreach ($groups->getGroups() as $group) {
            $groupsList[$group->getEmail()] = $group;
        }

        return $groupsList;
    }

    /**
     * Cache the list of all the Groups in Gapps, and return it
     *
     * @param bool $reset Reset the current cache
     * @return array|\Google_Service_Directory_Group[]
     */
    public function getGroupsListCached($reset = false)
    {
        if (empty($this->groupsListCache) || $reset) {
            $this->groupsListCache = $this->getGroupsList();
        }

        return $this->groupsListCache;
    }

    /**
     * Create a group on the Gapps domain
     *
     * @param string $groupName
     * @throws \Exception
     */
    public function createGroup($groupName)
    {
        if ($groupName == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty group name {$groupName} to create");
            throw new LoggedException("Invalid or empty GApps group {$groupName} to create");
        }

        if ($this->groupExists($groupName)) {
            return;
        }

        $groupInstance = new \Google_Service_Directory_Group();
        $groupInstance->setName($groupName);
        $groupInstance->setEmail($this->getCleanedGroupName($groupName, true));
        $groupInstance->setKind('Anyone'); //@todo controllare se migliore o altre possibilità per restringere permessi (solo ragazzi classe + docenti, o solo docenti)
        try {
            $this->service->groups->insert($groupInstance);
            $this->logger->info("GOOGLEAPPS: Group {$groupName} created successfully");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error creating group {$groupName}: " . $gse->getMessage());
            throw new LoggedException("Error creating GApps group {$groupName}: " . $gse->getMessage(), 0, $gse);
        }
    }

    /**
     * Rename a group on the GApps domain
     *
     * @param $groupName
     * @param $previousGroupName
     * @throws \Exception
     */
    public function renameGroup($groupName, $previousGroupName)
    {
        if ($groupName == "" || $previousGroupName == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty group name {$groupName} to rename to {$previousGroupName}");
            throw new LoggedException("Invalid or empty GApps group {$groupName} to rename to {$previousGroupName}");
        }

        $previousEmail = $this->getCleanedGroupName($previousGroupName, true);
        $groupEmail = $this->getCleanedGroupName($groupName, true);

        try {
            $group = $this->service->groups->get($previousEmail);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->info("GOOGLEAPPS: Group {$previousEmail} not found: " . $gse->getMessage());
            throw new LoggedException("Error finding GApps group {$previousEmail} to rename: " . $gse->getMessage(), 0, $gse);
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }

        if (!$group instanceof \Google_Service_Directory_Group) {
            $this->logger->error("GOOGLEAPPS: Invalid group {$previousEmail} to rename");
            throw new LoggedException("Invalid GApps group {$previousEmail} to rename");
        }

        $group->setName($groupName);
        $group->setEmail($groupEmail);

        try {
            $this->service->groups->update($previousEmail, $group);
            $this->logger->info("GOOGLEAPPS: Group {$groupEmail} renamed successfully from {$previousEmail}");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error renaming group {$previousEmail} => {$groupEmail}: " . $gse->getMessage());
            throw new LoggedException("Error renanaming GApps group {$previousEmail} => {$groupEmail}: " . $gse->getMessage(), 0, $gse);
        }
    }

    /**
     * Remove a group from the Gapps domain
     *
     * @param string $groupName
     * @throws \Exception
     */
    public function removeGroup($groupName)
    {
        if ($groupName == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty group name {$groupName} to remove");
            throw new LoggedException("Invalid or empty GApps group {$groupName} to remove");
        }

        $email = $this->getCleanedGroupName($groupName, true);
        try {
            $this->service->groups->delete($email);
            $this->logger->info("GOOGLEAPPS: Group {$groupName} deleted successfully");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error deleting group {$groupName}: " . $gse->getMessage());
            throw new LoggedException("Error removing GApps group {$groupName}: " . $gse->getMessage(), 0, $gse);
        }
    }

    /**
     * Update all the users in a Gapps group
     *
     * @param array $usersInGroup
     * @param string $groupName
     */
    public function updateGroupMembers($usersInGroup, $groupName)
    {
        if ($groupName == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty group name {$groupName} to update member");
            return;
        }

        $this->logger->info("GOOGLEAPPS: Updating {$groupName} group members (" . count($usersInGroup) . ")");
        foreach ($usersInGroup as $k => $v) {
            $usersInGroup[$k] = strtolower(strpos($v, '@') === false ? $v . '@' . $this->domain : $v);
        }

        $name = $this->getCleanedGroupName($groupName);
        $list = array();

        if (!$this->groupExists($name)) {
            $this->createGroup($name);
        }

        $members = $this->service->members->listMembers($name . '@' . $this->domain);
        foreach ($members->getMembers() as $member) {
            $list[] = strtolower($member->email);
        }

        $removeThese = array_unique(array_diff($list, $usersInGroup));
        $addThese = array_unique(array_diff($usersInGroup, $list));
        foreach ($addThese as $email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                continue;
            }

            try {
                $member = new \Google_Service_Directory_Member();
                $member->setEmail($email);
                $this->service->members->insert($name . '@' . $this->domain, $member);
                $this->logger->info("GOOGLEAPPS: Added {$email} to google group {$name}");
            } catch (\Google_Service_Exception $gse) {
                $this->logger->error("GOOGLEAPPS: Error adding {$email} to google group {$name}: " . $gse->getMessage());
            } catch (\Google_IO_Exception $gioe) {
                $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            }
        }

        foreach ($removeThese as $email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                continue;
            }

            try {
                $this->service->members->delete($name . '@' . $this->domain, $email);
                $this->logger->info("GOOGLEAPPS: Removed {$email} from  group {$name}");
            } catch (\Google_Service_Exception $gse) {
                $this->logger->error("GOOGLEAPPS: Error removing {$email} from  group {$name}: " . $gse->getMessage());
            } catch (\Google_IO_Exception $gioe) {
                $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            }
        }
    }

    /**
     * Add user to Gapps group
     *
     * @param string $userEmail
     * @param string $groupName
     * @throws \Exception
     */
    public function addUserToGroup($userEmail, $groupName)
    {
        if ($groupName == "" || $userEmail == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty group name {$groupName} to add user {$userEmail}");
            throw new LoggedException("Invalid or empty GApps group {$groupName} to add user {$userEmail}");
        }

        if (strpos($userEmail, '@') === false) {
            $userEmail = $userEmail . '@' . $this->domain;
        }

        if (!$this->groupExists($groupName)) {
            $this->createGroup($groupName);
        }

        try {
            $member = new \Google_Service_Directory_Member();
            $member->setEmail($userEmail);
            $this->service->members->insert($this->getCleanedGroupName($groupName, true), $member);
            $this->logger->info("GOOGLEAPPS: Added {$userEmail} to group {$groupName}");
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error adding {$userEmail} to group {$groupName}: " . $gse->getMessage());
            throw new LoggedException("Error adding Gapps user {$userEmail} to group {$groupName}: " . $gse->getMessage());
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }
    }

    /**
     * Get the cleaned group name
     *
     * @param string $groupName
     * @param bool $asEmail return the group with the domain
     * @return string
     */
    public function getCleanedGroupName($groupName, $asEmail = false)
    {
        $groupName = str_replace(' ', '', (strtolower($groupName)));

        if ($asEmail) {
            $groupName .= '@' . $this->domain;
        }

        return $groupName;
    }

    /**
     * Check if OU of group exists
     *
     * @param string $ouName
     * @param bool $cache
     * @return bool
     */
    public function ouExists($ouName, $cache = false)
    {
        if ($cache) {
            $ouList = $this->getOUListCached();
            if (isset($ouList[$ouName]))
                return true;
            else
                return false;
        }

        try {
            $ou = $this->service->orgunits->get("my_customer", substr($this->ouBasePath, 1) . $this->getCleanedOUName($ouName));
        } catch (\Google_Exception $e) {
            return false;
        }

        return $ou instanceof \Google_Service_Directory_OrgUnit;
    }

    /**
     * Get the list of all OU in Gapps
     *
     * @return array|\Google_Service_Directory_OrgUnit[]
     */
    public function getOUsList()
    {
        try {
            $ous = $this->service->orgunits->listOrgunits("my_customer");
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error getting the ou list: " . $gse->getMessage());
            return [];
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            return [];
        }

        $ousList = array();
        foreach ($ous->getOrganizationUnits() as $ou) {
            $ousList[$ou->getName()] = $ou;
        }

        return $ousList;
    }

    /**
     * Cache the list of all the OU in Gapps, and return it
     *
     * @param bool $reset Reset the current cache
     * @return array|\Google_Service_Directory_OrgUnit[]
     */
    public function getOUListCached($reset = false)
    {
        if (empty($this->OUsListCache) || $reset) {
            $this->OUsListCache = $this->getOUsList();
        }

        return $this->OUsListCache;
    }

    /**
     * Create OU on Gapps Domain
     *
     * @param string $ouName
     * @throws \Exception
     */
    public function createOU($ouName)
    {
        if ($ouName == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty ou name {$ouName} to create");
            throw new LoggedException("Invalid or empty GApps ou {$ouName} to create");
        }

        if ($this->ouExists($ouName)) {
            return;
        }

        $ouName = $this->getCleanedOUName($ouName);
        $ouInstance = new \Google_Service_Directory_OrgUnit();
        $ouInstance->setName($ouName);

        if (strlen($this->ouBasePath) > 1 && substr($this->ouBasePath, -1, 1) == "/")
            $ouInstance->setParentOrgUnitPath(substr($this->ouBasePath, 0, -1));
        else
            $ouInstance->setParentOrgUnitPath($this->ouBasePath);

        try {
            $this->service->orgunits->insert('my_customer', $ouInstance);
            $this->logger->info("GOOGLEAPPS: OU {$ouName} created successfully");
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error creating OU {$ouName}: " . $gse->getMessage());
            throw new LoggedException("Error creating OU {$ouName}: " . $gse->getMessage());
        }
    }


    /**
     * Move all the users in the Gapps OU
     *
     * @param array $usersInOU
     * @param string $ouName
     * @throws \Exception
     */
    public function updateOUMembers($usersInOU, $ouName)
    {
        if ($ouName == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty ou name {$ouName} to update");
            throw new LoggedException("Invalid or empty GApps ou {$ouName} to update");
        }

        $this->logger->info("GOOGLEAPPS: Updating {$ouName} OU members");
        $name = $this->getCleanedOUName($ouName);
        if (!$this->ouExists($name)) {
            $this->createOU($ouName);
        }

//        foreach ($usersInOU as $k => $v) {
//            $this->addUserToOU($v,$name);
//        }

        $ouPath = (strlen($this->ouBasePath) > 1 && substr($this->ouBasePath, -1, 1) == "/") ? substr($this->ouBasePath, 0, -1) . "/" : $this->ouBasePath;
        $members = $this->service->users->listUsers(['domain' => $this->domain, 'query' => "orgUnitPath=" . $ouPath . $name . ""]);
        $currentUsersInOu = $membersByEmail = [];
        foreach ($members->getUsers() as $member) {
            $currentUsersInOu[] = strtolower($member->primaryEmail);
            $membersByEmail[strtolower($member->primaryEmail)] = $member;
        }

        $usersInOU = array_map(function ($item) {
            return strtolower($item);
        }, $usersInOU);

        $removeThese = array_unique(array_diff($currentUsersInOu, $usersInOU));
        $addThese = array_unique(array_diff($usersInOU, $currentUsersInOu));
        foreach ($addThese as $email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                continue;
            }

            try {
                $member = $this->service->users->get($email);
                $member->setOrgUnitPath($this->ouBasePath . $name);
                $this->service->users->update($email, $member);
                $this->logger->info("GOOGLEAPPS: Added {$email} to google ou {$name}");
            } catch (\Google_Service_Exception $gse) {
                $this->logger->error("GOOGLEAPPS: Error adding {$email} to google ou {$name}: " . $gse->getMessage());
            } catch (\Google_IO_Exception $gioe) {
                $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            }
        }

        foreach ($removeThese as $email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false || !isset($membersByEmail[$email])) {
                continue;
            }

            try {
                if (strlen($this->ouBasePath) > 1 && substr($this->ouBasePath, -1, 1) == "/") {
                    $membersByEmail[$email]->setOrgUnitPath(substr($this->ouBasePath, 0, -1));
                } else {
                    $membersByEmail[$email]->setOrgUnitPath($this->ouBasePath);
                }

                $this->service->users->update($email, $membersByEmail[$email]);
                $this->logger->info("GOOGLEAPPS: Removed {$email} from google ou {$name}");
            } catch (\Google_Service_Exception $gse) {
                $this->logger->error("GOOGLEAPPS: Error removing {$email} from ou {$name}: " . $gse->getMessage());
            } catch (\Google_IO_Exception $gioe) {
                $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            }
        }
    }

    /**
     * Move the user to the Gapps OU
     *
     * @param string $userEmail
     * @param string $ouName
     * @throws \Exception
     */
    public function addUserToOU($userEmail, $ouName)
    {
        if ($ouName == "" || $userEmail == "") {
            $this->logger->error("GOOGLEAPPS: Invalid or empty ou name {$ouName} or user email {$userEmail} to move into ou");
            throw new LoggedException("Invalid or empty GApps ou {$ouName} or user email {$userEmail} to move into ou");
        }

        if (strpos($userEmail, '@') === false) {
            $userEmail = $userEmail . '@' . $this->domain;
        }

        try {
            $user = $this->service->users->get($userEmail);
            $this->logger->info("GOOGLEAPPS: {$userEmail} moved sucessfully to OU {$ouName}");
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: User {$userEmail} not found: " . $gse->getMessage());
            throw new LoggedException("Gapps user {$userEmail} not found: " . $gse->getMessage(), 0, $gse);
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }

        $ouName = $this->getCleanedOUName($ouName);

        if (strtolower($user->getOrgUnitPath()) == strtolower($this->ouBasePath . $ouName)) {
            return;
        }

        $user->setOrgUnitPath($this->ouBasePath . $ouName);
        try {
            $this->service->users->update($userEmail, $user);
            $this->logger->info("GOOGLEAPPS: User {$userEmail} added successfully to OU {$ouName}");
        } catch (\Google_Service_Exception $gse) {
            $this->logger->error("GOOGLEAPPS: Error adding User {$userEmail} to OU {$ouName}: " . $gse->getMessage());
            throw new LoggedException("Error adding Gapps User {$userEmail} to OU {$ouName}: " . $gse->getMessage(), 0, $gse);
        } catch (\Google_IO_Exception $gioe) {
            $this->logger->error("GOOGLEAPPS: Error in connection: " . $gioe->getMessage());
            throw new ConnectionException("Error connecting to GApps: " . $gioe->getMessage(), 0, $gioe);
        }
    }

    /**
     * Get the cleaned ou name
     *
     * @param string $groupName
     * @param bool $asEmail return the group with the domain
     * @return string
     */
    protected function getCleanedOUName($groupName)
    {
        return str_replace([' ', "/"], ['', "-"], (strtolower($groupName)));
    }

    private function authorize()
    {
        $client = new \Google_Client();
        $client->setApplicationName('igroove');
        $client->setClientId($this->clientId);
        $client->setClientSecret($this->clientSecret);
        $client->setRedirectUri('urn:ietf:wg:oauth:2.0:oob');
        $client->setScopes(
            array(
                \Google_Service_Directory::ADMIN_DIRECTORY_USER,
                \Google_Service_Directory::ADMIN_DIRECTORY_GROUP,
                \Google_Service_Directory::ADMIN_DIRECTORY_ORGUNIT
            )
        );
        $client->setAccessType('offline');

        if ($this->clientId == "" || $this->clientSecret == "")
            return;

        $tokenFilename = $this->kernelRootDir . '/../googleAppsToken_' . $this->domain . '.json';
        if (!file_exists($tokenFilename)) {
            if (php_sapi_name() != "cli") {
                $this->logger->error("GOOGLEAPPS: Autenticazione API GOOGLE non effettuata. Chiamare il cron da terminale");
                throw new LoggedException('Autenticazione API GOOGLE non effettuata. Chiamare il cron da terminale');
            }

            $authUrl = $client->createAuthUrl();
            //Request authorization
            print "\n* * * *  Prima autenticazione";
            print "Inserisci nel browser:\n$authUrl\n\n";
            print "Inserisci il codice di autenticazione:\n";
            $authCode = trim(fgets(STDIN));
            // Exchange authorization code for access token
            $accessToken = $client->authenticate($authCode);

            $client->setAccessToken($accessToken);

            file_put_contents($tokenFilename, json_encode($accessToken));
        }

        $client->setAccessToken(file_get_contents($tokenFilename));
        if ($client->isAccessTokenExpired()) {
            if (php_sapi_name() == "cli")
                echo "\n* * * *  Riautenticazione API GOOGLE in corso...\n";
            $accessToken = $client->getAccessToken();
            $client->refreshToken($accessToken['refresh_token']);
            file_put_contents($tokenFilename, json_encode($client->getAccessToken()));
        }
        $this->service = new \Google_Service_Directory($client);
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }
}
