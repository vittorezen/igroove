<?php

namespace Zen\IgrooveBundle\Manager;

use Zen\IgrooveBundle\LdapTool;
use Zen\IgrooveBundle\MikrotikPool;

class InternetAccessManager
{
    protected $mikrotikPool;
    protected $groupNameInInternet;
    protected $userNameInInternet;
    protected $em;
    protected $configurationAD;
    protected $adldapManager;

    public function __construct($configurationManager, $adldapManager, $mikrotikManager, $em)
    {

        $this->configurationAD = $configurationManager->getActiveDirectoryConfiguration();
        $this->adldapManager = $adldapManager;

        $this->mikrotikManager = $mikrotikManager;
        $this->getUsers();
        $this->em = $em;
    }

    private function getUsers()
    {


        $adldap=$this->adldapManager->getAdLdap();
        $groupsInInternet = $adldap->group()->inGroup('_InternetAccess');
        $groupNameInInternet = array();
        if (!is_array($groupsInInternet)) {
        sleep(10);
            $groupsInInternet = $adldap->group()->inGroup('_InternetAccess');
        }
        if (is_array($groupsInInternet)) {
            foreach ($groupsInInternet as $key => $groupAD) {
                $groupNameInInternet[] = trim(substr($groupAD, 3, strpos($groupAD, ',', 3) - 3));
            }
        }



        $this->userNameInInternet = $adldap->group()->members('_InternetAccess', false);

        $this->groupNameInInternet = $groupNameInInternet;

    }

    public function addGroup($group)
    {
        $adldap=$this->adldapManager->getAdLdap();

        if (!in_array($group, $this->groupNameInInternet)) {
            $result = $adldap->group()->addGroup('_InternetAccess', $group);
            if ($result) {
                $this->getUsers();
                $this->adldapManager->explodeGroupInternetAccess();
            }
        }
    }

    public function removeGroup($group)
    {
        $adldap=$this->adldapManager->getAdLdap();


        if (in_array($group, $this->groupNameInInternet)) {
            $result = $adldap->group()->removeGroup('_InternetAccess', $group);
            if ($result) {
                $entities = $this->em->getRepository('ZenIgrooveBundle:InternetOpen')->findBy(
                    array('type' => 'group', 'account' => strtolower($group))
                );
                foreach ($entities as $entity) {
                    $this->em->remove($entity);
                }
                $this->em->flush();

                $this->getUsers();

                $this->adldapManager->explodeGroupInternetAccess();
                $this->mikrotikManager->KickOffUsers($this->adldapManager);
            }
        }

    }

    public function addUser($user)
    {
        $adldap=$this->adldapManager->getAdLdap();
        if (!in_array($user, $this->userNameInInternet)) {
            $result = $adldap->group()->addUser('_InternetAccess', $user);
            if ($result) {
                $this->getUsers();
                $this->adldapManager->explodeGroupInternetAccess();
            }
        }
    }

    public function removeUser($user)
    {
        $adldap=$this->adldapManager->getAdLdap();
        if (in_array($user, $this->userNameInInternet)) {
            $result = $adldap->group()->removeUser('_InternetAccess', $user);
            if ($result) {
                $entities = $this->em->getRepository('ZenIgrooveBundle:InternetOpen')->findBy(
                    array('type' => 'user', 'account' => strtolower($user))
                );
                foreach ($entities as $entity) {
                    $this->em->remove($entity);
                }
                $this->em->flush();

                $this->getUsers();

                $this->adldapManager->explodeGroupInternetAccess();
                $this->mikrotikManager->KickOffUsers($this->adldapManager);
            }
        }
    }

}
