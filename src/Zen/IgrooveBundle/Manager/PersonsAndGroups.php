<?php

namespace Zen\IgrooveBundle\Manager;

use Doctrine\ORM\EntityManager;
use OldSound\RabbitMqBundle\RabbitMq\Producer;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Zen\IgrooveBundle\Entity\Group;
use Zen\IgrooveBundle\Entity\LdapGroup;
use Zen\IgrooveBundle\Entity\LdapUser;
use Zen\IgrooveBundle\Entity\PersonAbstract;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Student;
use Zen\IgrooveBundle\Entity\Subject;
use Zen\IgrooveBundle\Entity\Teacher;
use Zen\IgrooveBundle\Entity\TeacherSubjectGroup;
use Zen\IgrooveBundle\Exception\LoggedException;
use Zen\IgrooveBundle\ImporterFilter\AbstractFilter;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\AbstractEntity;
use Zen\IgrooveBundle\Repository\GroupRepository;
use Zen\IgrooveBundle\Repository\StudentRepository;
use Zen\IgrooveBundle\Repository\LdapGroupRepository;
use Zen\IgrooveBundle\Repository\LdapUserRepository;
use Zen\IgrooveBundle\Repository\TeacherRepository;

class PersonsAndGroups
{
    /**
     * @var ConfigurationManager
     */
    protected $configurationManager;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var GroupRepository
     */
    protected $groupRepository;

    /**
     * @var StudentRepository
     */
    protected $studentRepository;

    /**
     * @var TeacherRepository
     */
    protected $teacherRepository;

    /**
     * @var EntityManager
     */
    protected $providerRepository;

    /**
     * @var LdapGroupRepository
     */
    protected $ldapGroupRepository;

    /**
     * @var LdapUserRepository
     */
    protected $ldapUserRepository;

    /**
     * @var LdapProxy
     */
    protected $ldapProxy;

    /**
     * @var string
     */
    protected $kernelRootDir;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    /**
     * @var string
     */
    protected $inDebug;

    /**
     * @var string
     */
    protected $active_directory_generated_group_prefix;

    /**
     * @var string
     */
    protected $active_directory_generated_teacher_group_prefix;

    /**
     * @var \Symfony\Component\HttpKernel\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var array
     */
    protected $importedEntities;

    /**
     * @var GoogleApps[]
     */
    protected $googleAppManagers = [];

    /**
     * @var null|array
     */
    protected $ldapAccountsUsername = null;

    /**
     * @var array
     */
    protected $gappsAccountsUsername = [];

    /**
     * @var Producer
     */
    protected $rabbitMQclient;

    /**
     * @param \Doctrine\ORM\EntityManager                       $em
     * @param ConfigurationManager                              $configurationManager
     * @param Producer                                          $rabbitMQclient
     * @param \Symfony\Component\HttpKernel\Log\LoggerInterface $logger
     * @param LdapProxy                                         $ldapProxy
     * @param string                                            $kernelRootDir
     * @param AuthorizationChecker                              $authorizationChecker
     * @param string                                            $inDebug
     */
    public function __construct($em, $configurationManager, $rabbitMQclient, $logger, $ldapProxy, $kernelRootDir, $authorizationChecker, $inDebug)
    {
        $this->configurationManager = $configurationManager;
        $this->em = $em;
        $this->rabbitMQclient = $rabbitMQclient;
        $this->logger = $logger;
        $this->groupRepository = $this->em->getRepository('ZenIgrooveBundle:Group');
        $this->studentRepository = $this->em->getRepository('ZenIgrooveBundle:Student');
        $this->teacherRepository = $this->em->getRepository('ZenIgrooveBundle:Teacher');
        $this->ldapGroupRepository = $this->em->getRepository('ZenIgrooveBundle:LdapGroup');
        $this->ldapUserRepository = $this->em->getRepository('ZenIgrooveBundle:LdapUser');
        $this->providerRepository = $this->em->getRepository('ZenIgrooveBundle:Provider');
        $this->active_directory_generated_group_prefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $this->active_directory_generated_teacher_group_prefix = $configurationManager->getActiveDirectoryGeneratedTeacherGroupPrefix();
        $this->ldapProxy = $ldapProxy;
        $this->kernelRootDir = $kernelRootDir;
        $this->authorizationChecker = $authorizationChecker;
        $this->inDebug = $inDebug;
    }

    /**
     * Import People, Groups, Subject and Association from the filter loaded with the provider data.
     *
     * @param AbstractFilter $filter
     * @param Provider       $provider
     * @throws \Exception
     */
    public function importElements(AbstractFilter $filter, Provider $provider)
    {
        $this->logger->info("Inizio importazione dal provider {$provider->getName()}");

        $this->importEntity($filter->getSectors(), $provider, 'Sector', 'name');

        $newGroups = $filter->getGroups();
        foreach ($newGroups as $newGroup) {
            if ($newGroup->getSectorId() !== null && isset($this->importedEntities['Sector'][$newGroup->getSectorId()])) {
                $newGroup->setSector($this->importedEntities['Sector'][$newGroup->getSectorId()]);
            }
        }
        $this->importEntity($newGroups, $provider, 'Group', 'name');
        $this->importEntity($filter->getTeachers(), $provider, 'Teacher', 'fiscalCode');
        $this->importEntity($filter->getSubjects(), $provider, 'Subject', 'name');
        $newStudents = $filter->getStudents();
        foreach ($newStudents as $k => $newStudent) {
            if ($newStudent->getGroupId() !== null && isset($this->importedEntities['Group'][$newStudent->getGroupId()])) {
                $newStudent->setGroup($this->importedEntities['Group'][$newStudent->getGroupId()]);
            }
        }
        $this->importEntity($newStudents, $provider, 'Student', 'fiscalCode');

        $this->logger->info('Cancellazione associazioni teacher-subject-group del provider');

        $query = $this->em->getRepository('ZenIgrooveBundle:TeacherSubjectGroup')->createQueryBuilder('tsg')
            ->innerJoin('tsg.group', 'g')
            ->where('tsg.provider = :pid AND g.manageManually = 0')
            ->setParameter('pid', $provider->getId())
            ->getQuery();
        $tsgsToDelete = $query->execute();
        foreach ($tsgsToDelete as $tsgToDelete) {
            $this->em->remove($tsgToDelete);
        }
        $this->em->flush();

        $this->logger->info('Importazione associazioni teacher-subject-group del provider');
        foreach ($filter->getTeacherSubjectGroupRelation() as $teacherSubjectGroupRelation) {
            if (!isset($this->importedEntities['Group'][$teacherSubjectGroupRelation['group_id']]) ||
                !isset($this->importedEntities['Subject'][$teacherSubjectGroupRelation['subject_id']])
            ) {
                continue;
            }

            if (!$this->importedEntities['Group'][$teacherSubjectGroupRelation['group_id']] instanceof Group ||
                !$this->importedEntities['Subject'][$teacherSubjectGroupRelation['subject_id']] instanceof Subject
            ) {
                continue;
            }

            if(isset($this->importedEntities['Teacher'][$teacherSubjectGroupRelation['teacher_id']]) &&
                $this->importedEntities['Teacher'][$teacherSubjectGroupRelation['teacher_id']] instanceof Teacher) {
                $teacher = $this->importedEntities['Teacher'][$teacherSubjectGroupRelation['teacher_id']];
            } else {
                continue;
            }

            $teacherSubjectGroup = new TeacherSubjectGroup();
            $teacherSubjectGroup->setProvider($provider);
            $teacherSubjectGroup->setTeacher($teacher);
            $teacherSubjectGroup->setGroup($this->importedEntities['Group'][$teacherSubjectGroupRelation['group_id']]);
            $teacherSubjectGroup->setSubject($this->importedEntities['Subject'][$teacherSubjectGroupRelation['subject_id']]);
            $this->em->persist($teacherSubjectGroup);
            $this->logger->info("* Inserita associazione con id {$teacherSubjectGroup->getId()}");
        }
        $this->em->flush();

        $this->logger->info("Terminata importazione provider {$provider->getName()}\n");
    }

    /**
     * Import a specific entity type into the database.
     *
     * @param AbstractEntity[] $newEntities
     * @param Provider         $provider
     * @param $entityType
     * @param string $keyToCheck
     * @throws \Exception
     */
    protected function importEntity($newEntities, Provider $provider, $entityType, $keyToCheck = 'id')
    {
        $this->logger->info("Importazione entità {$entityType}. Oggetti ricevuti ".count($newEntities));

        $keyToCheckGet = 'get'.str_replace('_', '', ucwords($keyToCheck, '_'));
        $entityTypeFull = '\\Zen\\IgrooveBundle\\Entity\\'.$entityType;
        $repository = $this->em->getRepository('ZenIgrooveBundle:'.$entityType);
        $this->importedEntities[$entityType] = [];
        $currentEntities = $repository->findBy(['provider' => $provider->getId()]);
        $residualCurrentEntities = [];
        $checkKey = [];

        //controllo tra le entità correnti se trovo già un entità, usando l'id provider, altrimenti aggiungo alle entità locali residue
        foreach ($currentEntities as $currentEntity) {
            //aggiungo a entità residue, se non ha un idOnProvider o se non esiste l'entità con quell'id fra le nuove
            if ($currentEntity->getIdOnProvider() == '' || !isset($newEntities[$currentEntity->getIdOnProvider()])) {
                $residualCurrentEntities[$currentEntity->getId()] = $currentEntity;
                if ($keyToCheck != '') {
                    $checkKey[strtolower($currentEntity->$keyToCheckGet())] = $currentEntity->getId();
                }

                continue;
            }

            $newEntity = $newEntities[$currentEntity->getIdOnProvider()];
            if (!$newEntity->isValid()) {
                $this->logger->error("* Dati dell'entità dal provider {$currentEntity->getIdOnProvider()} da aggiornare su id locale {$currentEntity->getId()}, non validi");
                continue;
            }
            unset($newEntities[$currentEntity->getIdOnProvider()]);
            $this->importedEntities[$entityType][$currentEntity->getIdOnProvider()] = $currentEntity;
            if (!$currentEntity->isSame($newEntity)) {
                $repository->updateWithImportData($currentEntity, $newEntity, $provider);
                $this->logger->info("* Aggiornata entità con id locale {$currentEntity->getId()} e id provider {$currentEntity->getIdOnProvider()}");
            }
        }

        $residualCurrentAdditionalEntities = [];
        //scarico l'elenco delle persone già esistenti che hanno questo provider come addizionale
        if($entityType == "Student" || $entityType == "Teacher") {
            $additionalPersons = $entityType == "Teacher" ? $provider->getAdditionalTeachers() : $provider->getAdditionalStudents();
            if ($keyToCheck != '') {
                foreach ($additionalPersons as $k => $additionalPerson) {
                    $residualCurrentAdditionalEntities[strtolower($additionalPerson->$keyToCheckGet())] = $additionalPerson;
                }
            }
        }

        foreach ($newEntities as $idOnProvider => $newEntity) {
            //cerco se esiste un entità con la chiave da cercare (nome o id) combaciante, e nel caso la aggiorno, altrimenti inserico
            if ($keyToCheck != '' && isset($checkKey[strtolower($newEntity->$keyToCheckGet())]) &&
                isset($residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]])
            ) {
                $entity = $residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]];
                unset($residualCurrentEntities[$checkKey[strtolower($newEntity->$keyToCheckGet())]]);

                if (!$entity instanceof $entityTypeFull) {
                    continue;
                }

                if (!$newEntity->isValid()) {
                    $this->logger->error("* Dati dell'entità dal provider {$keyToCheck} = {$newEntity->$keyToCheckGet()} da aggiornare su id locale {$entity->getId()}, non validi");
                    continue;
                }

                $this->logger->info("* Aggiorno entità con id locale {$entity->getId()} in base alla chiave {$keyToCheck} di valore {$newEntity->$keyToCheckGet()}");
                $entity->setIdOnProvider($idOnProvider);

                if (!$entity->isSame($newEntity)) {
                    $repository->updateWithImportData($entity, $newEntity, $provider);
                    $this->logger->info('** Dati variati');
                } else {
                    $this->em->flush();
                }
            } elseif(($entityType == "Student" || $entityType == "Teacher") && $keyToCheck != '' &&
                isset($residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())])) { //se è una persona, cerco se esiste fra le persone che hanno questo provider come addizionale e nel caso sia studente gli aggiorno i gruppi

                $entity = $residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())];
                unset($residualCurrentAdditionalEntities[strtolower($newEntity->$keyToCheckGet())]);

                if (!$entity instanceof $entityTypeFull) {
                    continue;
                }

                if (!$newEntity->isValid()) {
                    $this->logger->error("* Dati della persona dal provider {$keyToCheck} = {$newEntity->$keyToCheckGet()} da aggiornare su id locale {$entity->getId()}, non validi");
                    continue;
                }

                if($entityType == "Student" && !$entity->haveSameGroup($newEntity, $provider)) {
                    $repository->updateMembershipWithImportData($entity, $newEntity, $provider);
                    $this->logger->info("* Aggiornate le membership per lo studente con id locale {$entity->getId()}");
                }
            } elseif($keyToCheck != '' && ($entityType == "Student" || $entityType == "Teacher") && $newEntity->$keyToCheckGet() != "" &&
                ($entity = $repository->findOneBy([$keyToCheck => strtolower($newEntity->$keyToCheckGet())])) instanceof $entityTypeFull
            ) { //se è una persona, cerco con la chiave di controllo se non esiste già su un'altro provider, e nel caso la associo a questo provider e se studente aggiorno i suoi gruppi
                $entity->addAdditionalProvider($provider);
                $this->em->flush();
                $this->logger->info("* Associata la persona con id locale {$entity->getId()} al provider {$provider->getId()}");

                if($entityType == "Student") {
                    $repository->updateMembershipWithImportData($entity, $newEntity, $provider);
                    $this->logger->info("* Aggiornate le membership per la persona con id locale {$entity->getId()}");
                }
            } else { //non esiste già, lo creo
                if (!$newEntity->isValid()) {
                    $this->logger->error("* Dati dell'entità dal provider {$newEntity->$keyToCheckGet()} da inserire, non validi");
                    continue;
                }

                try {
                    $entity = $repository->createWithImportData($newEntity, $provider, $idOnProvider);
                } catch (\Throwable $e) {
                    $this->logger->error("* Errore durante l'importazione di {$newEntity->$keyToCheckGet()}");
                    continue;
                }

                if ($entity === null) {
                    $this->logger->error("* Entità dal provider {$newEntity->$keyToCheckGet()} da inserire, non valida");
                    continue;
                }

                $this->logger->info("* Inserita nuova entità con nuovo id {$entity->getId()}");
            }

            $this->importedEntities[$entityType][$idOnProvider] = $entity;
            unset($newEntities[$idOnProvider]);
        }

        $this->logger->info("Terminata importazione entità {$entityType}.");
        $this->logger->info('Id entità rimanenti ('.count($residualCurrentEntities).'): '.implode(', ', array_map(function ($el) {
            return $el->getId();
        }, $residualCurrentEntities)));
        $this->logger->info('Id entità non importate ('.count($newEntities).'): '.implode(', ', array_map(function ($el) use ($keyToCheckGet) {
            return $el->$keyToCheckGet();
        }, $newEntities)));
        $this->logger->info('Id associazioni addizionali entità rimanenti ('.count($residualCurrentAdditionalEntities).'): '.implode(', ', array_map(function ($el) {
                return $el->getId();
        }, $residualCurrentAdditionalEntities)));
    }

    /**
     * Check that all the Groups in database are present in Ldap/AD.
     */
    public function checkLdapGroups()
    {
        $this->logger->info('check groups not present in ldap');

        $groups = $this->groupRepository->findAll();
        foreach ($groups as $group) {
            try {
                $this->syncGroupWithLdapGroup($group);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapGroup: ".$e->getMessage());
                }
            }

            try {
                $this->syncGroupWithLdapGroup($group, null, true);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapGroup-teacher: ".$e->getMessage());
                }
            }
        }

        $this->checkInternetAccessLdapGroups();
        $this->checkPersonalDeviceAccessLdapGroups();

        $this->em->flush();
    }

    /**
     * Synchronize or create one Group in Ldap/AD based on a Group Datas.
     *
     * @param Group $group
     * @param bool  $teacher
     * @param bool  $immediate
     *
     * @throws \Exception
     */
    public function syncGroupWithLdapGroup(Group $group, $currentName = null, $teacher = false, $immediate = false)
    {
        if ($teacher && $this->active_directory_generated_teacher_group_prefix == '') {
            return;
        }

        $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
        $ldapName = $groupPrefix.$this->ldapEscape($group->getName());

        if ($currentName !== null) {
            $currentLdapName = $groupPrefix.$this->ldapEscape($currentName);
            if ($currentLdapName == $ldapName || $ldapName == $groupPrefix || $currentName == $groupPrefix) {
                return;
            }

            $ldapGroup = $this->ldapGroupRepository->find($currentLdapName);
            if (!$ldapGroup instanceof LdapGroup) {
                $this->logger->error('PERS&GRP: Impossible to find the Ldap Group to rename');
                throw new LoggedException('Impossible to find the Ldap Group to rename');
            }

            $ldapGroup->setOperation('RENAME:'.$ldapName);
        } else {
            $ldapGroup = $this->ldapGroupRepository->find($ldapName);

            if (!$ldapGroup) {
                $ldapGroup = new LdapGroup();
                $ldapGroup->setName($ldapName);
                $ldapGroup->setOperation('CREATE');
                $this->em->persist($ldapGroup);
                $this->logger->info('Group '.$group->getName().($teacher ? ' teacher' : '').' to be created in ldap');
            }
        }

//        $providerLdapGroup = $this->checkProviderLdapGroup($group->getProvider(), $teacher);
//        if ($providerLdapGroup instanceof LdapGroup) {
//            $providerLdapGroup->addMember('group', $ldapGroup->getName());
//        }

        if ($immediate) {
            $this->em->flush();
            $this->ldapProxy->syncLdapGroupWithDBGroup($ldapGroup);
//            if ($providerLdapGroup instanceof LdapGroup) {
//                $this->ldapProxy->syncLdapGroupWithDBGroup($providerLdapGroup);
//            }
            $this->em->flush();
        }
    }

    /**
     * Check if exist or create the Ldap/AD group for the provider.
     *
     * @param Provider $provider
     * @param bool     $teacher
     *
     * @return LdapGroup
     */
    protected function checkProviderLdapGroup(Provider $provider, $teacher = false)
    {
        static $ldapGroups = [];
        $providerId = (string) $provider->getId().($teacher ? 't' : '');

        if (!isset($ldapGroups[$providerId])) {
            $this->logger->info('Check if group '.$provider->getName().($teacher ? ' teacher' : '').' exists in ldap');

            $name = $this->ldapEscape($provider->getName());
            $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
            $ldapGroups[$providerId] = $this->ldapGroupRepository->find(
                $groupPrefix.$name
            );

            if (!$ldapGroups[$providerId]) {
                $ldapGroups[$providerId] = new LdapGroup();
                $ldapGroups[$providerId]->setName($groupPrefix.$name);
                $ldapGroups[$providerId]->setOperation('CREATE');
                $this->em->persist($ldapGroups[$providerId]);
                $this->em->flush();
                $this->logger->info('group '.$name.($teacher ? ' teacher' : '').' to be created in ldap');
            }
        }

        return $ldapGroups[$providerId];
    }

    /**
     * Check if exist or create the default internet access Ldap/Ad group for activated users/groups.
     */
    public function checkInternetAccessLdapGroups()
    {
        $this->logger->info('checking default internetaccess group in ldap');
        $ldapGroup = $this->ldapGroupRepository->find(
            $this->active_directory_generated_group_prefix.'InternetAccess'
        );
        if (!$ldapGroup) {
            $ldapGroup = new LdapGroup();
            $ldapGroup->setName($this->active_directory_generated_group_prefix.'InternetAccess');
            $ldapGroup->setOperation('CREATE');
            $this->em->persist($ldapGroup);
            $this->em->flush();
            $this->logger->info('default internetaccess group to be created in ldap');
        }

        return $ldapGroup;
    }

    /**
     * Check if exist or create the default internet access Ldap/Ad group for activated users/groups.
     */
    public function checkPersonalDeviceAccessLdapGroups()
    {
        $this->logger->info('checking default personal device access group in ldap');
        $ldapGroup = $this->ldapGroupRepository->find(
            $this->active_directory_generated_group_prefix.'PersonalDeviceAccess'
        );
        if (!$ldapGroup) {
            $ldapGroup = new LdapGroup();
            $ldapGroup->setName($this->active_directory_generated_group_prefix.'PersonalDeviceAccess');
            $ldapGroup->setOperation('CREATE');
            $this->em->persist($ldapGroup);
            $this->em->flush();
            $this->logger->info('default personal device access group to be created in ldap');
        }

        return $ldapGroup;
    }

    /**
     * Remove the group from Ldap/Ad.
     *
     * @param Group $group
     * @param bool  $teacher
     *
     * @throws \Exception
     */
    public function removeGroupLdapGroup(Group $group, $teacher = false)
    {
        $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
        $ldapName = $groupPrefix.$this->ldapEscape($group->getName());

        $ldapGroup = $this->ldapGroupRepository->find($ldapName);
        if (!$ldapGroup instanceof LdapGroup) {
            $this->logger->error('PERS&GRP: Invalid Ldap Group to delete for the specified Group name '.$ldapName);
            throw new LoggedException('Invalid Ldap Group to delete for the specified Group name '.$ldapName);
        }

        $ldapGroup->setOperation('REMOVE');
        $this->em->persist($ldapGroup);
        $this->em->flush();
        $this->ldapProxy->syncLdapGroupWithDBGroup($ldapGroup);
    }

    /**
     * Create all the OU from the groups.
     */
    public function checkLdapOUs()
    {
        $this->logger->info('check groups not present in ldap');
        $groups = $this->groupRepository->findBy(['manageManually' => false]);

        foreach ($groups as $group) {
            try {
                $this->syncGroupWithLdapOU($group);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapOUs: ".$e->getMessage());
                }
            }
        }

        $this->em->flush();
    }

    /**
     * Create the OU of a group, if it not exists.
     *
     * @param Group $group
     */
    public function syncGroupWithLdapOU(Group $group)
    {
        $provider = $group->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (!$providerSettings['ldapCreateOU']) {
            return;
        }

        $prefix = $providerSettings['ldapOUPrefix'] ?: '';

        $this->ldapProxy->createOuIfNotExists($prefix.$group->getName(), $providerSettings['ldapOUPath'] ?: '');
    }

    /**
     * Synchronize all the Student and Teacher with the Ldap users.
     */
    public function checkLdapUsers()
    {
        $this->logger->info('check users not present in ldap');
        $students = $this->studentRepository->findAll();
        foreach ($students as $student) {
            try {
                $this->syncPersonWithLdapUser($student);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapUsers: ".$e->getMessage());
                }
            }
        }

        $teachers = $this->teacherRepository->findAll();
        foreach ($teachers as $teacher) {
            try {
                $this->syncPersonWithLdapUser($teacher);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapUsers-teacher: ".$e->getMessage());
                }
            }
        }
        $this->em->flush();
    }

    /**
     * Create or Synchronize an Ldap/AD account with the Person data.
     *
     * @param PersonAbstract $person
     * @param bool           $immediate
     *
     * @throws \Exception
     */
    public function syncPersonWithLdapUser(PersonAbstract $person, $immediate = false)
    {
        $personId = $person->getId();
        $providerSettings = $person->getProviderSettings();
        $modified = false;
        $ldapUser = $this->ldapUserRepository->findOneByDistinguishedId($personId);

        //create ldap user if not exist
        if (!$ldapUser instanceof LdapUser) {
            if($person->getUsername() != "" && $this->ldapUserRepository->usernameAlreadyExists($person->getUsername())) {
                $this->logger->error("User with id = {$personId} NOT found in LdapUsers, but the username {$person->getUsername()} is already used in Ldap");
                return;
            }

            $ldapUser = new LdapUser();
            $ldapUser->setOperation('CREATE');
            $ldapUser->setDistinguishedId($personId);

            $this->logger->info("User with id = {$personId} NOT found in LdapUsers set creation into LDAP");
        }
        //set username of ldap user if is empty or different from the person one
        if ($ldapUser->getUsername() == '' || $person->getUsername() != $ldapUser->getUsername()) {
            //generate person username if is empty and the autocreate is active
            $username = ($person->getUsername() == '' && $providerSettings['autoCreateUsername']) ? $this->createUsername($person, $this->configurationManager->getActiveDirectoryUsernameStyle()) : strtolower($person->getUsername());

            if ($username != '') {
                $ldapUser->setUsername($username);
                $person->setUsername($username);
                $this->ldapAccountsUsername[] = $username;
                $modified = true;
                $this->logger->info("Set username {$ldapUser->getUsername()} for user {$personId}");
            } else {
                $this->logger->error('PERS&GRP: Username empty for person '.$person->getId());
                throw new LoggedException('Username empty for person '.$person->getId());
            }
        }
        //modify the ldapuser firstname or lastname if is changed in the person instance
        if ($ldapUser->getFirstname() != $person->getFirstname() || $ldapUser->getLastname() != $person->getLastname()) {
            $ldapUser->setFirstname($person->getFirstname());
            $ldapUser->setLastname($person->getLastname());
            $this->logger->info("Set name {$ldapUser->getLastname()} {$ldapUser->getFirstname()} for user {$ldapUser->getUsername()}");
            $modified = true;
        }

        //prepare the ldapuser attributes
        $currentAttributesJson = $ldapUser->getAttributes();
        $currentAttributes = (array) json_decode($currentAttributesJson);
        $attributes = [
            'description' => 'Creato da igroove'.$this->configurationManager->getActiveDirectorySyncSuffix(),
            'homedrive' => (strlen($this->configurationManager->getActiveDirectoryHomeDrive()) == 1) ? strtoupper(
                $this->configurationManager->getActiveDirectoryHomeDrive().':'
            ) : null,
            'homedirectory' => (strlen(
                    $this->configurationManager->getActiveDirectoryHomeFolder()
                ) > 2) ? $this->configurationManager->getActiveDirectoryHomeFolder().'\\'.$ldapUser->getUsername() : null,
            'email' => 'no@email',
            'radius-vlan' => null,
            'objectGUID' => isset($currentAttributes['objectGUID']) ? $currentAttributes['objectGUID'] : '',
        ];

        //set the ldapuser email from the person instance, or autogenerate if enabled
        if ($person->getEmail() != '') {
            $attributes['email'] = $person->getEmail();
        } elseif ((strlen($providerSettings['googleAppClientId']) > 0) && $providerSettings['googleAppAutoEmail']) {
            $attributes['email'] = $ldapUser->getUsername().'@'.$providerSettings['googleAppDomain'];
        }

        //set the vlan id from the group, if the person instance is an user
        if ($person instanceof Student) {
            $mainClassroom = $person->getNotManuallyManagedGroups()->first();
            if ($mainClassroom instanceof Group && (int) $mainClassroom->getRadiusVlan() > 0) {
                $attributes['radius-vlan'] = (int) $mainClassroom->getRadiusVlan();
            }
        }

        //update the attributes if is changed
        if ($currentAttributes != $attributes) {
            $modified = true;
            $ldapUser->setAttributes(json_encode($attributes));
            $this->logger->info(
                'user '.$ldapUser->getUsername().' attributes has changed ('.$currentAttributesJson.') -> ('.$ldapUser->getAttributes().')'
            );
        }

        //force, generate or set the password if
        if ($providerSettings['forceImportedPassword']) {
            $ldapUser->setStartingPassword($person->getStartingPassword());
            $this->logger->info('user '.$ldapUser->getUsername().' password forced');
            $modified = true;
        } elseif (strlen(trim($person->getStartingPassword())) == 0) {
            $ldapUser->setStartingPassword($this->getRandomPassword($this->configurationManager->getActiveDirectoryPasswordComplexity(), $this->configurationManager->getActiveDirectoryPasswordMinChar()));
            $person->setStartingPassword($ldapUser->getStartingPassword());
            $this->logger->info('user '.$ldapUser->getUsername().' password has changed');
            $modified = true;
        } elseif ($ldapUser->getOperation() == 'CREATE') {
            $ldapUser->setStartingPassword($person->getStartingPassword());
            $this->logger->info('user '.$ldapUser->getUsername().' password has been set');
        }

        if ($ldapUser->getOperation() == 'CREATE') {
            $this->em->persist($ldapUser);
        } elseif ($modified) {
            $ldapUser->setOperation('MODIFY');
        }

        if ($immediate) {
            $this->em->flush();
            $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
            $this->em->flush();
        }
    }

    /**
     * Reset the Ldap/AD password to the one set in the Person data.
     *
     * @param PersonAbstract $person
     * @param bool           $immediate
     *
     * @throws \Exception
     */
    public function resetPersonLdapUserPassword(PersonAbstract $person, $immediate = true)
    {
        $ldapUser = $this->ldapUserRepository->findOneByUsername($person->getUsername());
        if (!$ldapUser instanceof LdapUser) {
            $this->logger->error('PERS&GRP: Invalid Ldap User for the specified Person '.$person->getUsername());
            throw new LoggedException('Invalid Ldap User for the specified Person '.$person->getUsername());
        }

        $ldapUser->setStartingPassword($person->getStartingPassword());
        $ldapUser->setOperation('MODIFY');

        if ($immediate) {
            $this->em->flush();
            $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
            $this->em->flush();
        }
    }

    /**
     * Remove the Ldap/AD account for a specific Person.
     *
     * @param PersonAbstract $person
     *
     * @throws \Exception
     */
    public function removePersonLdapUser(PersonAbstract $person)
    {
        $ldapUser = $this->ldapUserRepository->findOneByDistinguishedId($person->getId());
        if (!$ldapUser instanceof LdapUser) {
            $this->logger->error('PERS&GRP: Invalid Ldap User to delete for the specified Person '.$person->getFiscalCode());
            throw new LoggedException('Invalid Ldap User to delete for the specified Person '.$person->getFiscalCode());
        }

        $ldapUser->setOperation('REMOVE');
        $this->em->persist($ldapUser);
        $this->em->flush();
        $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
    }

    /**
     * Synchronize the Memberships of all the Person Ldap/AD Account with the Ldap/AD Group.
     */
    public function checkLdapUsersMembership()
    {
        $students = $this->studentRepository->findAll();
        foreach ($students as $student) {
            try {
                $this->syncPersonMembershipsWithLdapGroups($student);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapUsersMembership: ".$e->getMessage());
                }
            }
        }

        $teachers = $this->teacherRepository->findAll();
        foreach ($teachers as $teacher) {
            try {
                $this->syncPersonMembershipsWithLdapGroups($teacher);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapUsersMembership-teacher: ".$e->getMessage());
                }
            }
        }

        $providers = $this->providerRepository->findBy(['active' => true]);
        foreach ($providers as $provider) {
            try {
                $this->syncProviderGroupLdapMembers($provider);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapUsersMembership-provider: ".$e->getMessage());
                }
            }

            try {
                $this->syncProviderGroupLdapMembers($provider, true);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapUsersMembership-provider-teacher: ".$e->getMessage());
                }
            }
        }
        $this->em->flush();
    }

    /**
     * Synchronize the membership of a Person Ldap/AD account with the Ldap Group.
     *
     * @param PersonAbstract $person
     * @param bool           $immediate
     *
     * @throws \Exception
     */
    public function syncPersonMembershipsWithLdapGroups(PersonAbstract $person, $immediate = false)
    {
        $ldapUser = $this->ldapUserRepository->findOneByDistinguishedId($person->getId());
        if (!$ldapUser instanceof LdapUser) {
            return;
        }

        $ldapGroups = [];
        $providersLdapGroupName = $this->getProvidersLdapGroupName();
        foreach ($ldapUser->getMemberOf($this->em) as $ldapGroupName) {
            $ldapGroupName = strtolower($ldapGroupName);
            if(in_array($ldapGroupName,$providersLdapGroupName)) {
                continue;
            }
            $ldapGroups[] = $ldapGroupName;
        }

        $personGroupsName = $toUpdateGroups = [];

        //retrive groups from student or teacher
        if ($person instanceof Student) {
            $personGroups = $person->getMemberOf();
            $groupPrefix = $this->active_directory_generated_group_prefix;
        } elseif ($person instanceof Teacher) {
            if ($this->active_directory_generated_teacher_group_prefix == '') {
                return;
            }

            $personGroups = $person->getGroups();
            $groupPrefix = $this->active_directory_generated_teacher_group_prefix;
        } else {
            $this->logger->error('PERS&GRP: Person type undefined for membership sync');
            throw new LoggedException('Person type undefined for membership sync');
        }

        foreach ($personGroups as $personGroup) {
            $personGroupsName[] = strtolower($groupPrefix.$this->ldapEscape($personGroup->getName()));
        }

        //remove the person instance to the ldap group
        foreach (array_diff($ldapGroups, $personGroupsName) as $ldapGroupName) {
            $ldapGroup = $this->ldapGroupRepository->find($ldapGroupName);
            if ($ldapGroup) {
                $this->logger->info('user '.$ldapUser->getUsername().' removed from ldap group '.$ldapGroupName);
                $ldapGroup->removeMember('user', $ldapUser->getUsername());
                if (!isset($toUpdateGroups[$ldapGroup->getName()])) {
                    $toUpdateGroups[$ldapGroup->getName()] = $ldapGroup;
                }
            }
        }

        //add the person instance to the ldap group
        foreach (array_diff($personGroupsName, $ldapGroups) as $ldapGroupName) {
            $ldapGroup = $this->ldapGroupRepository->find($ldapGroupName);
            if ($ldapGroup) {
                $this->logger->info('user '.$ldapUser->getUsername().' added to ldap group '.$ldapGroupName);
                $ldapGroup->addMember('user', $ldapUser->getUsername());
                if (!isset($toUpdateGroups[$ldapGroup->getName()])) {
                    $toUpdateGroups[$ldapGroup->getName()] = $ldapGroup;
                }
            }
        }

        if ($immediate) {
            $this->em->flush();
            foreach ($toUpdateGroups as $toUpdateGroup) {
                $this->ldapProxy->syncLdapGroupMembershipWithDB($toUpdateGroup);
            }
            $this->em->flush();
        }
    }

    /**
     * Synchronize all the Ldap/AD User Memberships in a Group.
     *
     * @param Group $group
     * @param bool  $teacher
     *
     * @throws \Exception
     */
    public function syncGroupMembershipWithLdapGroupMembership(Group $group, $teacher = false)
    {
        if ($teacher && $this->active_directory_generated_teacher_group_prefix == '') {
            return;
        }

        $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
        $ldapName = strtolower($groupPrefix.$this->ldapEscape($group->getName()));
        $ldapGroup = $this->ldapGroupRepository->find($ldapName);

        if (!$ldapGroup instanceof LdapGroup) {
            return;
        }

        $this->logger->info("checking {$group->getName()} ldap group members");

        $ldapUsers = $personNames = [];
        foreach ($ldapGroup->getMembersList('user') as $username) {
            if(is_string($username) && $username !== "") {
                $ldapUsers[] = strtolower($username);
            }
        }

        if ($teacher) {
            foreach ($group->getTeachers() as $teacher) {
                if($teacher->getUsername() != "") {
                    $personNames[] = strtolower($teacher->getUsername());
                }
            }
        } else {
            foreach ($group->getStudents() as $student) {
                if($student->getUsername() != "") {
                    $personNames[] = strtolower($student->getUsername());
                }
            }
        }

        foreach (array_diff($ldapUsers, $personNames) as $username) {
            $this->logger->info('user '.$username.' removed from ldap group '.$ldapGroup->getName());
            $ldapGroup->removeMember('user', $username);
        }

        foreach (array_diff($personNames, $ldapUsers) as $username) {
            $this->logger->info('user '.$username.' added to ldap group '.$ldapGroup->getName());
            $ldapGroup->addMember('user', $username);
        }

        $this->em->flush();
        $this->ldapProxy->syncLdapGroupMembershipWithDB($ldapGroup);
        $this->em->flush();
    }

    public function syncProviderGroupLdapMembers(Provider $provider, $teacher = false) {
        $providerLdapGroup = $this->checkProviderLdapGroup($provider, $teacher);
        if(!$providerLdapGroup instanceof LdapGroup || ($teacher && $this->active_directory_generated_teacher_group_prefix == "")) {
            return;
        }

        $this->logger->info("checking provider {$provider->getName()} ldap group members");

        $populateWithUsers = $teacher ? $this->configurationManager->getActiveDirectoryGeneratedTeacherProviderGroupWithUsers() : $this->configurationManager->getActiveDirectoryGeneratedProviderGroupWithUsers();
        if($populateWithUsers) {
            $ldapUsers = $personNames = [];
            foreach ($providerLdapGroup->getMembersList('user') as $username) {
                if(is_string($username) && $username != "") {
                    $ldapUsers[] = strtolower($username);
                }
            }

            if ($teacher) {
                foreach ($provider->getTeachers() as $teacher) {
                    if($teacher->getUsername() != "") {
                        $personNames[] = strtolower($teacher->getUsername());
                    }
                }

                foreach ($provider->getAdditionalTeachers() as $teacher) {
                    if($teacher->getUsername() != "") {
                        $personNames[] = strtolower($teacher->getUsername());
                    }
                }
            } else {
                foreach ($provider->getStudents() as $student) {
                    if($student->getUsername() != "") {
                        $personNames[] = strtolower($student->getUsername());
                    }
                }

                foreach ($provider->getAdditionalStudents() as $student) {
                    if($student->getUsername() != "") {
                        $personNames[] = strtolower($student->getUsername());
                    }
                }
            }

            foreach (array_diff($ldapUsers, $personNames) as $username) {
                $this->logger->info('user '.$username.' removed from provider group '.$providerLdapGroup->getName());
                $providerLdapGroup->removeMember('user', $username);
            }

            foreach (array_diff($personNames, $ldapUsers) as $username) {
                $this->logger->info('user '.$username.' added to provider group '.$providerLdapGroup->getName());
                $providerLdapGroup->addMember('user', $username);
            }
        } else {
            $ldapGroups = $groupsNames = [];
            foreach ($providerLdapGroup->getMembersList('group') as $groupName) {
                $ldapGroups[] = strtolower($groupName);
            }

            $groupPrefix = $teacher ? $this->active_directory_generated_teacher_group_prefix : $this->active_directory_generated_group_prefix;
            foreach ($provider->getGroups() as $group) {
                $groupsNames[] = strtolower($groupPrefix.$this->ldapEscape($group->getName()));
            }

            foreach (array_diff($ldapGroups, $groupsNames) as $groupName) {
                $this->logger->info('group '.$groupName.' removed from provider group '.$providerLdapGroup->getName());
                $providerLdapGroup->removeMember('group', $groupName);
            }

            foreach (array_diff($groupsNames, $ldapGroups) as $groupName) {
                $this->logger->info('group '.$groupName.' added to provider group '.$providerLdapGroup->getName());
                $providerLdapGroup->addMember('group', $groupName);
            }
        }
    }

    /**
     * Return the list of all the providers ldap groups name (for teacher and student)
     * @return array
     */
    protected function getProvidersLdapGroupName() {
        static $groupNames;

        if(!isset($groupNames)) {
            $groupNames = [];
            $providers = $this->providerRepository->findAll();
            foreach ($providers as $provider) {
                $groupNames[] = strtolower($this->active_directory_generated_group_prefix.$this->ldapEscape($provider->getName()));
                if($this->active_directory_generated_teacher_group_prefix != "") {
                    $groupNames[] = strtolower($this->active_directory_generated_teacher_group_prefix.$this->ldapEscape($provider->getName()));
                }
            }
        }

        return $groupNames;
    }

    /**
     * Synchronize all the vlan id of the Ldap/AD Account in a Group.
     *
     * @param Group $group
     *
     * @throws \Exception
     */
    public function syncGroupMembersVlan(Group $group)
    {
        if ($group->getManageManually()) {
            return;
        }

        $students = $group->getStudents();
        foreach ($students as $student) {
            if($student->getProvider()->getId() !== $group->getProvider()->getId()) {
                continue;
            }

            $ldapUser = $this->ldapUserRepository->findOneByDistinguishedId($student->getId());
            if (!$ldapUser instanceof LdapUser) {
                continue;
            }
            $attributes = (array) json_decode($ldapUser->getAttributes());
            $attributes['radius-vlan'] = (int) $group->getRadiusVlan() > 0 ? (int) $group->getRadiusVlan() : '';
            $ldapUser->setAttributes(json_encode($attributes));
            $ldapUser->setOperation('MODIFY');
            $this->ldapProxy->syncLdapUserWithDBUser($ldapUser);
        }
    }

    /**
     * Synchronize the Memberships of all the Person Account with the Ldap OU.
     */
    public function checkLdapUsersOUMembership()
    {
        $groups = $this->groupRepository->findBy(['manageManually' => false]);
        foreach ($groups as $group) {
            try {
                $this->syncGroupMembershipsWithLdapOU($group);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapUsersOUMembership: ".$e->getMessage());
                }
            }
        }

        $teachers = $this->teacherRepository->findAll();
        foreach ($teachers as $teacher) {
            try {
                $this->moveTeacherToLdapOU($teacher);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkLdapUsersOUMembership-teacher: ".$e->getMessage());
                }
            }
        }
        $this->em->flush();
    }

    /**
     * Synchronize all the LdapUser OU in a Group.
     *
     * @param Group $group
     *
     * @throws \Exception
     */
    public function syncGroupMembershipsWithLdapOU(Group $group)
    {
        $provider = $group->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (!$providerSettings['ldapCreateOU']) {
            return;
        }

        $ouName = $providerSettings['ldapOUPrefix'] ? $providerSettings['ldapOUPrefix'].$group->getName() : $group->getName();
        $path = $providerSettings['ldapOUPath'] ?: '';
        $members = $group->getStudents();
        $studentsInGroup = [];
        foreach ($members as $member) {
            if ($member->getUsername() != '' && $member->getProvider()->getId() === $provider->getId()) {
                $studentsInGroup[] = $member->getUsername();
            }
        }

        $this->ldapProxy->createOuIfNotExists($ouName, $path);
        $this->ldapProxy->updateUsersIntoOu($ouName, $studentsInGroup, $path);
    }

    /**
     * Move a LdapUser into the teacher OU.
     *
     * @param Teacher $teacher
     *
     * @throws \Exception
     */
    public function moveTeacherToLdapOU(Teacher $teacher)
    {
        $provider = $teacher->getProvider();
        $providerSettings = $provider->getTeacherSettings();
        if (!$providerSettings['ldapCreateOU'] || $teacher->getUsername() == '') {
            return;
        }

        $prefix = $providerSettings['ldapOUPrefix'] ?: 'Teachers';
        $path = $providerSettings['ldapOUPath'] ?: '';

        $this->ldapProxy->createOuIfNotExists($prefix, $path);
        $this->ldapProxy->moveUserIntoOu($prefix, $teacher->getUsername(), $path);
    }

    /**
     * Move a LdapUser into the OU specified.
     *
     * @param Student $student
     * @param $ouName
     *
     * @throws \Exception
     */
    public function moveStudentToLdapOU(Student $student, $ouName)
    {
        $provider = $student->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (!$providerSettings['ldapCreateOU'] || $student->getUsername() == '' || $ouName == '') {
            return;
        }

        $prefix = $providerSettings['ldapOUPrefix'] ?: '';
        $path = $providerSettings['ldapOUPath'] ?: '';

        $this->ldapProxy->createOuIfNotExists($prefix.$ouName, $path);
        $this->ldapProxy->moveUserIntoOu($prefix.$ouName, $student->getUsername(), $path);
    }

    /**
     * Check that all the Groups in database are present in GApps.
     */
    public function checkGoogleAppsGroups()
    {
        $groups = $this->groupRepository->findAll();

        foreach ($groups as $group) {
            try {
                $this->syncGroupWithGoogleGroup($group);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsGroups: ".$e->getMessage());
                }
            }

            try {
                $this->syncGroupWithGoogleGroup($group, null, true);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsGroups-teacher: ".$e->getMessage());
                }
            }
        }

        $providers = $this->providerRepository->findBy(['active' => true]);
        foreach ($providers as $provider) {
            try {
                $this->syncProviderGroupWithGoogleGroup($provider);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsGroups-provider: ".$e->getMessage());
                }
            }

            try {
                $this->syncProviderGroupWithGoogleGroup($provider, true);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsGroups-provider-teacher: ".$e->getMessage());
                }
            }
        }
    }

    /**
     * Synchronize or create one Group in GApps based on a Group Datas.
     *
     * @param Group  $group
     * @param string $previousGroupName
     * @param bool   $teacher
     * @param bool   $immediate
     *
     * @throws \Exception
     */
    public function syncGroupWithGoogleGroup(Group $group, $previousGroupName = '', $teacher = false, $immediate = false)
    {
        $provider = $group->getProvider();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0 || !$providerSettings['googleAppCreateGroup']) {
            return;
        }
        $groupName = ($providerSettings['googleAppGroupPrefix'] ?: '').$group->getName();
        $fullPreviousGroupName = ($providerSettings['googleAppGroupPrefix'] ?: '').$previousGroupName;


        $gam = $this->getGoogleAppsManagerForProvider($provider, $teacher);
        if ($gam->groupExists($groupName, !$immediate)) {
            return;
        }

        if ($previousGroupName != '' && $fullPreviousGroupName != $groupName) {
            if ($immediate) {
                $gam->renameGroup($groupName, $fullPreviousGroupName);
            } else {
                $this->sendRabbitMQMessage('renameGroup', $provider, $teacher,
                    ['groupName' => $groupName, 'previousGroupName' => $fullPreviousGroupName]
                );
            }
        } else {
            if ($immediate) {
                $gam->createGroup($groupName);
            } else {
                $this->sendRabbitMQMessage('createGroup', $provider, $teacher, ['groupName' => $groupName]);
            }
        }

        if($providerSettings['googleAppCreateProviderGroup'] && $immediate) {
            $gam->addUserToGroup($groupName, ($providerSettings['googleAppGroupPrefix'] ?: '').$provider->getName());
        }
    }

    /**
     * Syncronize all the members of a provider in the relative GApps group
     *
     * @param Provider $provider
     * @param bool $teacher
     * @param bool $immediate
     * @throws \Exception
     */
    public function syncProviderGroupWithGoogleGroup(Provider $provider, $teacher = false, $immediate = false) {
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0 || !$providerSettings['googleAppCreateProviderGroup']) {
            return;
        }

        $groupPrefix = ($providerSettings['googleAppGroupPrefix'] ?: '');
        $groupName = $groupPrefix.$provider->getName();
        $gam = $this->getGoogleAppsManagerForProvider($provider, $teacher);
        if (!$gam->groupExists($groupName, !$immediate)) {
            $gam->createGroup($groupName);
        }

        $membersInGroup = [];

        if($providerSettings['googleAppUseUserInProviderGroup']) {
            if ($teacher) {
                foreach ($provider->getTeachers() as $teacher) {
                    if($teacher->getEmail() != "" && filter_var($teacher->getEmail(), FILTER_VALIDATE_EMAIL)) {
                        $membersInGroup[] = $teacher->getEmail();
                    }
                }

                foreach ($provider->getAdditionalTeachers() as $teacher) {
                    if($teacher->getEmail() != "" && filter_var($teacher->getEmail(), FILTER_VALIDATE_EMAIL)) {
                        $membersInGroup[] = $teacher->getEmail();
                    }
                }
            } else {
                foreach ($provider->getStudents() as $student) {
                    if($student->getEmail() != "" && filter_var($student->getEmail(), FILTER_VALIDATE_EMAIL)) {
                        $membersInGroup[] = $student->getEmail();
                    }
                }

                foreach ($provider->getAdditionalStudents() as $student) {
                    if($student->getEmail() != "" && filter_var($student->getEmail(), FILTER_VALIDATE_EMAIL)) {
                        $membersInGroup[] = $student->getEmail();
                    }
                }
            }
        } else {
            foreach ($provider->getGroups() as $group) {
                $membersInGroup[] = $gam->getCleanedGroupName($groupPrefix.$group, true);
            }
        }

        if($providerSettings['googleAppProviderGroupExtraEmail'] != "") {
            foreach (explode(",",$providerSettings['googleAppProviderGroupExtraEmail']) as $email) {
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $membersInGroup[] = trim($email);
                }
            }
        }

        if ($immediate) {
            $gam->updateGroupMembers($membersInGroup, $groupName);
        } else {
            $this->sendRabbitMQMessage('updateGroupMembers', $provider, $teacher,
                [
                    'studentsInGroup' => $membersInGroup,
                    'groupName' => $groupName,
                ]
            );
        }
    }

    /**
     * Remove the Group from GApps.
     *
     * @param Group $group
     * @param bool  $teacher
     * @param bool  $immediate
     */
    public function removeGroupGoogleAppsGroup(Group $group, $teacher = false, $immediate = false)
    {
        $provider = $group->getProvider();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0 || !$providerSettings['googleAppCreateGroup']) {
            return;
        }
        $groupName = ($providerSettings['googleAppGroupPrefix'] ?: '').$group->getName();

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, $teacher)->removeGroup($groupName);
        } else {
            $this->sendRabbitMQMessage('removeGroup', $provider, $teacher, ['groupName' => $groupName]);
        }
    }

    /**
     * Check that all the Groups in database are present as GApps OU.
     */
    public function checkGoogleAppsOUs()
    {
        $groups = $this->groupRepository->findBy(['manageManually' => false]);

        foreach ($groups as $group) {
            try {
                $this->syncGroupWithGoogleOU($group);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsOUs: ".$e->getMessage());
                }
            }
        }
    }

    /**
     * Synchronize or create one Group as GApps OU.
     *
     * @param Group $group
     * @param bool  $immediate
     *
     * @throws \Exception
     */
    public function syncGroupWithGoogleOU(Group $group, $immediate = false)
    {
        $provider = $group->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0 || !$providerSettings['googleAppCreateOU']) {
            return;
        }
        $prefix = $providerSettings['googleAppOUPrefix'] ?: '';
        $gam = $this->getGoogleAppsManagerForProvider($provider, false);

        if($gam->ouExists($prefix.$group->getName(), !$immediate)) {
            return;
        }

        if ($immediate) {
            $gam->createOU($prefix.$group->getName());
        } else {
            $this->sendRabbitMQMessage('createOU', $provider, false, ['ouName' => $prefix.$group->getName()]);
        }
    }

    /**
     * Synchronize all the Student and Teacher with the Gapps account.
     */
    public function checkGoogleAppsUsers()
    {
        $students = $this->studentRepository->findAll();
        foreach ($students as $student) {
            try {
                $this->syncPersonWithGoogleAppsUser($student);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsUsers: ".$e->getMessage());
                }
            }
        }

        $teachers = $this->teacherRepository->findAll();
        foreach ($teachers as $teacher) {
            try {
                $this->syncPersonWithGoogleAppsUser($teacher);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsUsers-teacher: ".$e->getMessage());
                }
            }
        }

        $this->em->flush();
    }

    /**
     * Create or Synchronize a GApps account with the Person data.
     *
     * @param PersonAbstract $person
     * @param string         $previousEmail
     * @param bool           $immediate
     *
     * @throws \Exception
     */
    public function syncPersonWithGoogleAppsUser(PersonAbstract $person, $previousEmail = '', $immediate = false)
    {
        $provider = $person->getProvider();
        $teacher = $person instanceof Teacher;
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0) {
            return;
        }

        $gam = $this->getGoogleAppsManagerForProvider($provider, $teacher);

        $email = $person->getEmail();
        if ($email == '' && $providerSettings['googleAppAutoEmail'] && $providerSettings['googleAppDomain'] != '') {
            if ($providerSettings['googleAppAutoEmailStyle'] != '') {
                $email = $this->createUsername($person, $providerSettings['googleAppAutoEmailStyle'], $gam).'@'.$providerSettings['googleAppDomain'];
            } elseif ($person->getUsername() != '') {
                $email = $person->getUsername().'@'.$providerSettings['googleAppDomain'];
            }
            $this->logger->info("Setting email for user {$person->getUsername()} to {$email}");
        }

        if($email == '' && !$providerSettings['googleAppAutoEmail']) {
            return;
        }

        if ($email == '' || !$gam->emailInDomain($email)) {
            $this->logger->error('PERS&GRP-syncPersWithGApU: Invalid email for user with id '.$person->getId());
            throw new LoggedException('Invalid email for user with id '.$person->getId());
        }

        if ($gam->userExists($email, !$immediate)) {
            if ($person->getEmail() != $email) {
                $person->setEmail($email);
                $this->em->flush();
            }

            return;
        }

        if($person->getStartingPassword() == "") {
            $this->logger->error("Empty password for user {$person->getUsername()}. Cannot create email {$email}");
            return;
        }

        $this->gappsAccountsUsername[$providerSettings['googleAppDomain']][] = $email;

        if ($previousEmail != '' && $previousEmail != $email) {
            if ($immediate) {
                $gam->renameUser($email, $previousEmail, $person->getFirstname(), $person->getLastname());
                if ($person->getEmail() != $email) {
                    $person->setEmail($email);
                    $this->em->flush();
                }
            } else {
                $this->sendRabbitMQMessage('renameUser', $provider, $teacher,
                    [
                        'email' => $email,
                        'firstname' => $person->getFirstname(),
                        'lastname' => $person->getLastname(),
                        'previousEmail' => $previousEmail,
                        'userId' => $person->getId(),
                        'teacher' => $teacher,
                    ]
                );
            }
        } else {
            if ($immediate) {
                $gam->createUser($email, $person->getFirstname(), $person->getLastname(), $person->getStartingPassword());
                if ($person->getEmail() != $email) {
                    $person->setEmail($email);
                    $this->em->flush();
                }
            } else {
                $this->sendRabbitMQMessage('createUser', $provider, $teacher,
                    [
                        'email' => $email,
                        'firstname' => $person->getFirstname(),
                        'lastname' => $person->getLastname(),
                        'password' => $person->getStartingPassword(),
                        'userId' => $person->getId(),
                        'teacher' => $teacher,
                    ]
                );
            }
        }
    }

    /**
     * Reset the Google Apps account password to the one set in the Person data.
     *
     * @param PersonAbstract $person
     * @param bool           $immediate
     *
     * @throws \Exception
     */
    public function resetPersonGoogleAppsUserPassword(PersonAbstract $person, $immediate = true)
    {
        $provider = $person->getProvider();
        $teacher = $person instanceof Teacher;
        $email = $person->getEmail();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0) {
            return;
        }

        if ($email == '') {
            $this->logger->error('PERS&GRP-resetGAppPwd: Invalid email');
            throw new LoggedException('Invalid email');
        }

        $gam = $this->getGoogleAppsManagerForProvider($provider, $teacher);

        if ($immediate) {
            $gam->resetUserPassword($email, $person->getStartingPassword());
        } else {
            $this->sendRabbitMQMessage('resetUserPassword', $provider, $teacher,
                [
                    'email' => $email,
                    'password' => $person->getStartingPassword(),
                ]
            );
        }
    }

    /**
     * Remove the GApps account for a Person.
     *
     * @param PersonAbstract $person
     * @param bool           $immediate
     *
     * @throws \Exception
     */
    public function removePersonGoogleAppsUser(PersonAbstract $person, $immediate = false)
    {
        $email = $person->getEmail();
        $provider = $person->getProvider();
        $providerSettings = $person->getProviderSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0 || $email == '') {
            return;
        }

        $gam = $this->getGoogleAppsManagerForProvider($provider, $person instanceof Teacher);
        if (!$gam->emailInDomain($email)) {
            return;
        }

        if ($immediate) {
            $gam->removeUser($email);
        } else {
            $this->sendRabbitMQMessage('removeUser', $provider, $person instanceof Teacher, ['email' => $email]);
        }
    }

    /**
     * Synchronize the Memberships of all the Person GApps Account with the GApps Group.
     */
    public function checkGoogleAppsUsersMembershipToGroup()
    {
        $groups = $this->groupRepository->findAll();
        foreach ($groups as $group) {
            try {
                $this->syncGroupMembershipsWithGoogleGroupMemberships($group);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsUsersMembershipToGroup: ".$e->getMessage());
                }
            }

            try {
                $this->syncGroupMembershipsWithGoogleGroupMemberships($group, true);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsUsersMembershipToGroup-teacher: ".$e->getMessage());
                }
            }
        }
    }

    /**
     * Synchronize all the GApps account Memberships in a Group.
     *
     * @param Group $group
     * @param bool  $teacher
     * @param bool  $immediate
     *
     * @throws \Exception
     */
    public function syncGroupMembershipsWithGoogleGroupMemberships(Group $group, $teacher = false, $immediate = false)
    {
        $provider = $group->getProvider();
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0 || !$providerSettings['googleAppCreateGroup']) {
            return;
        }

        $groupName = $providerSettings['googleAppGroupPrefix'] ? $providerSettings['googleAppGroupPrefix'].$group->getName() : $group->getName();
        $members = $teacher ? $group->getTeachers() : $group->getStudents();
        $personsInGroup = [];
        foreach ($members as $member) {
            if ($member->getEmail() != '') {
                $personsInGroup[] = $member->getEmail();
            }
        }

        if($providerSettings['googleAppGroupExtraEmail'] != "") {
            foreach (explode(",",$providerSettings['googleAppGroupExtraEmail']) as $email) {
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $personsInGroup[] = trim($email);
                }
            }
        }

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, $teacher)->updateGroupMembers($personsInGroup, $groupName);
        } else {
            $this->sendRabbitMQMessage('updateGroupMembers', $provider, $teacher,
                [
                    'studentsInGroup' => $personsInGroup,
                    'groupName' => $groupName,
                ]
            );
        }
    }

    /**
     * Synchronize the Memberships of all the Person GApps Account with the GApps OU.
     */
    public function checkGoogleAppsUsersMembershipToOU()
    {
        $groups = $this->groupRepository->findBy(['manageManually' => false]);
        foreach ($groups as $group) {
            try {
                $this->syncGroupMembershipsWithGoogleOU($group);
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsUsersMembershipToOU: ".$e->getMessage());
                }
            }
        }

        $ouToSync = [];
        $providers = $this->providerRepository->findBy(['active' => true]);
        foreach ($providers as $provider) {
            try {
                $providerSettings = $provider->getTeacherSettings();
                if (strlen($providerSettings['googleAppClientId']) == 0 || !$providerSettings['googleAppCreateOU']) {
                    return;
                }

                $ouName = $providerSettings['googleAppOUPrefix'];
                if(!isset($ouToSync[$ouName])) {
                    $ouToSync[$ouName] = [];
                }

                $teachers = $this->teacherRepository->findBy(['provider' => $provider->getId()]);
                foreach ($teachers as $teacher) {
                    if($teacher->getEmail()) {
                        $ouToSync[$ouName][] = $teacher->getEmail();
                    }
                }
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsUsersMembershipToOU-prov: ".$e->getMessage());
                }
            }
        }

        foreach ($ouToSync as $ouName => $members) {
            try {
                $this->sendRabbitMQMessage('updateOUMembers', $provider, true,
                    ['studentsInOU' => $members, 'ouName' => $ouName]
                );
            } catch (\Exception $e) {
                if(get_class($e) != "Zen\IgrooveBundle\Exception\LoggedException" && get_class($e) != "Zen\IgrooveBundle\Exception\ConnectionException") {
                    $this->logger->error("PERS&GRP-checkGoogleAppsUsersMembershipToOU-send: ".$e->getMessage());
                }
            }
        }

//        $teachers = $this->teacherRepository->findAll();
//        foreach ($teachers as $teacher) {
//            try {
//                $this->moveTeacherToGoogleAppsOU($teacher);
//            } catch (\Exception $e) {
//            }
//        }
    }

    /**
     * Synchronize all the GApps account OU in a Group.
     *
     * @param Group $group
     * @param bool  $immediate
     *
     * @throws \Exception
     */
    public function syncGroupMembershipsWithGoogleOU(Group $group, $immediate = false)
    {
        $provider = $group->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0 || !$providerSettings['googleAppCreateOU']) {
            return;
        }

        $ouName = $providerSettings['googleAppOUPrefix'] ? $providerSettings['googleAppOUPrefix'].$group->getName() : $group->getName();
        $members = $group->getStudents();
        $studentsInGroup = [];
        foreach ($members as $member) {
            if ($member->getEmail() != '' && $member->getProvider()->getId() === $provider->getId()) {
                $studentsInGroup[] = $member->getEmail();
            }
        }

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, false)->updateOUMembers($studentsInGroup, $ouName);
        } else {
            $this->sendRabbitMQMessage('updateOUMembers', $provider, false,
                [
                    'studentsInOU' => $studentsInGroup,
                    'ouName' => $ouName,
                ]
            );
        }
    }

    /**
     * Move a GApps account into the teacher OU.
     *
     * @param Teacher $teacher
     * @param bool    $immediate
     *
     * @throws \Exception
     */
    public function moveTeacherToGoogleAppsOU(Teacher $teacher, $immediate = false, $gAppsOu = null)
    {
        $provider = $teacher->getProvider();
        $providerSettings = $provider->getTeacherSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0 || !$providerSettings['googleAppCreateOU'] || $teacher->getEmail() == '') {
            return;
        }

        $gam = $this->getGoogleAppsManagerForProvider($provider, true);
        if (!$gam->emailInDomain($teacher->getEmail()) || !$gam->userExists($teacher->getEmail(), !$immediate)) {
            return;
        }

        $prefix = $gAppsOu !== null ? $gAppsOu : $providerSettings['googleAppOUPrefix'] ?: '';

        //@todo controllare se il docente non è già nell'ou, e nel caso non eseguire il comando

        if (!$gam->ouExists($prefix, !$immediate)) {
            if ($immediate) {
                $this->getGoogleAppsManagerForProvider($provider, true)->createOU($prefix);
            } else {
                $this->sendRabbitMQMessage('createOU', $provider, true, ['ouName' => $prefix]);
            }
        }

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, true)->addUserToOU($teacher->getEmail(), $prefix);
        } else {
            $this->sendRabbitMQMessage('addUserToOU', $provider, true, ['email' => $teacher->getEmail(), 'ouName' => $prefix]);
        }
    }

    /**
     * Move a GApps account into the OU specified.
     *
     * @param Student $student
     * @param $ouName
     * @param bool $immediate
     *
     * @throws \Exception
     */
    public function moveStudentToGoogleAppsOU(Student $student, $ouName, $immediate = false)
    {
        $email = $student->getEmail();
        $provider = $student->getProvider();
        $providerSettings = $provider->getStudentSettings();
        if (strlen($providerSettings['googleAppClientId']) == 0 || !$providerSettings['googleAppCreateOU'] || $email == '' || $ouName == '') {
            return;
        }

        $gam = $this->getGoogleAppsManagerForProvider($provider, false);
        if (!$gam->emailInDomain($email)) { // || !$gam->userExists($email)
            return;
        }

        $prefix = $providerSettings['googleAppOUPrefix'] ?: '';

        if (!$gam->ouExists($prefix.$ouName, !$immediate)) {
            if ($immediate) {
                $this->getGoogleAppsManagerForProvider($provider, false)->createOU($prefix.$ouName);
            } else {
                $this->sendRabbitMQMessage('createOU', $provider, false, ['ouName' => $prefix.$ouName]);
            }
        }

        if ($immediate) {
            $this->getGoogleAppsManagerForProvider($provider, false)->addUserToOU($email, $prefix.$ouName);
        } else {
            $this->sendRabbitMQMessage('addUserToOU', $provider, false, ['email' => $email, 'ouName' => $prefix.$ouName]);
        }
    }

    public function prepareFormErrorMessage($message, \Exception $e)
    {
        if ($this->inDebug) {
            throw $e;
        }

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $message .= ":  {$e->getMessage()}"; //({$e->getFile()}:{$e->getLine()})
        }

        return $message;
    }

    public function addErrorsToForm(\Exception $e, Form $form)
    {
        if ($this->inDebug) {
            throw $e;
        }

        foreach (explode(PHP_EOL, $e->getMessage()) as $message) {
            $form->addError(new FormError($message));
        }
    }

    /**
     * Get the GoogleApps Manager configured with the data of the provider.
     *
     * @param Provider $provider
     * @param bool     $teacher
     *
     * @return GoogleApps
     *
     * @throws \Exception
     */
    protected function getGoogleAppsManagerForProvider(Provider $provider, $teacher)
    {
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();
        $key = ($teacher ? 't_' : '').$providerSettings['googleAppDomain'];

        if (!isset($this->googleAppManagers[$key])) {
            $this->googleAppManagers[$key] = new GoogleApps(
                $providerSettings['googleAppDomain'],
                $providerSettings['googleAppClientId'],
                $providerSettings['googleAppClientSecret'],
                $providerSettings['googleAppOUPath'],
                $this->kernelRootDir,
                $this->logger
            );
        }

        if (!$this->googleAppManagers[$key] instanceof GoogleApps) {
            $this->logger->error('PERS&GRP-getGappMang: Impossible to connect to the Gapps Service');
            throw new LoggedException('Impossible to connect to the Gapps Service');
        }

        return $this->googleAppManagers[$key];
    }

    /**
     * Prepare and send a RabbitMQ Command.
     *
     * @param $command
     * @param $provider
     * @param $teacher
     * @param $parameters
     */
    protected function sendRabbitMQMessage($command, $provider, $teacher, $parameters)
    {
        if ($command == '' || !is_array($parameters) || !$provider instanceof Provider) {
            return;
        }

        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();

        $parameters = $parameters + [
                'domain' => $providerSettings['googleAppDomain'],
                'clientId' => $providerSettings['googleAppClientId'],
                'clientSecret' => $providerSettings['googleAppClientSecret'],
                'ouPath' => $providerSettings['googleAppOUPath'],
            ];
        $msg = array(
            'command' => $command,
            'parameters' => $parameters,
        );
        $this->rabbitMQclient->publish(serialize($msg));
    }

    /**
     * Create a username for an Ldap User, that is not currently used.
     *
     * @param PersonAbstract  $person
     * @param string          $usernameStyle
     * @param GoogleApps|null $gam
     *
     * @return string
     *
     * @throws \Exception
     */
    public function createUsername(PersonAbstract $person, $usernameStyle, $gam = null)
    {
        if ($gam instanceof GoogleApps) {
            $emailDomain = $gam->getDomain();
            if (!isset($this->gappsAccountsUsername[$emailDomain])) {
                $this->gappsAccountsUsername[$emailDomain] = [];
                foreach ($gam->getUsersList() as $user) {
                    $this->gappsAccountsUsername[$emailDomain][] = str_replace('@'.$emailDomain, '', strtolower($user->getPrimaryEmail()));
                }
            }

            $existentUsernames = $this->gappsAccountsUsername[$emailDomain];
        } else {
            if ($this->ldapAccountsUsername === null) {
                $this->ldapAccountsUsername = [];
                $ldapAllUsers = $this->ldapUserRepository->findAll();
                foreach ($ldapAllUsers as $user) {
                    $this->ldapAccountsUsername[] = strtolower($user->getUsername());
                }

                $studentsUsername = $this->studentRepository->getUsernamesNotInLdapUser();
                $teachersUsername = $this->teacherRepository->getUsernamesNotInLdapUser();

                $this->ldapAccountsUsername = array_merge($this->ldapAccountsUsername, $studentsUsername, $teachersUsername);
            }

            $existentUsernames = $this->ldapAccountsUsername;
        }

        if ($usernameStyle == 'INIZIALENOME.COGNOME') {
            $lastname = $this->ldapEscape(str_replace(array(' ', '\''), '', $person->getLastname()));
            $firstname = $this->ldapEscape(str_replace(array(' ', '\''), '', $person->getFirstname()));
            $tryUsername = false;
            for ($i = 1; $i <= strlen($firstname); ++$i) {
                $tryUsername = substr(strtolower(substr($firstname, 0, $i).'.'.$lastname), 0, 20);
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                    break;
                }
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower(substr("$firstname$i.$lastname", 0, 20));
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error("PERS&GRP: Error generating username with style INIZIALENOME.COGNOME for user ".$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style INIZIALENOME.COGNOME");
            }
        } elseif ($usernameStyle == 'INIZIALENOMECOGNOME') {
            $lastname = $this->ldapEscape(str_replace(array(' ', '\''), '', $person->getLastname()));
            $firstname = $this->ldapEscape(str_replace(array(' ', '\''), '', $person->getFirstname()));
            $tryUsername = false;
            for ($i = 1; $i <= strlen($firstname); ++$i) {
                $tryUsername = substr(strtolower(substr($firstname, 0, $i).$lastname), 0, 20);
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                    break;
                }
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower(substr($firstname.$i.$lastname, 0, 20));
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error("PERS&GRP: Error generating username with style INIZIALENOMECOGNOME for user ".$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style INIZIALENOMECOGNOME");
            }
        }  elseif ($usernameStyle == 'ANNOINIZIALENOMECOGNOME') {
            $lastname = $this->ldapEscape(str_replace(array(' ', '\''), '', $person->getLastname()));
            $firstname = $this->ldapEscape(str_replace(array(' ', '\''), '', $person->getFirstname()));
            $currentYear = (int)date('m') > 5 ? (int)date('Y') : (int)date('Y') - 1;
            $tryUsername = false;
            for ($i = 1; $i <= strlen($firstname); ++$i) {
                $tryUsername = substr(strtolower($currentYear.substr($firstname, 0, $i).$lastname), 0, 20);
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                    break;
                }
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower(substr($currentYear.$firstname.$i.$lastname, 0, 20));
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error("PERS&GRP: Error generating username with style ANNOINIZIALENOMECOGNOME for user ".$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style ANNOINIZIALENOMECOGNOME");
            }
        } elseif ($usernameStyle == 'NOME.COGNOME') {
            $lastname = $this->ldapEscape(str_replace(array(' ', '\''), '', ($person->getLastname())));
            $firstname = $this->ldapEscape(str_replace(array(' ', '\''), '', ($person->getFirstname())));

            $tryUsername = strtolower(substr("$firstname.$lastname", 0, 20));
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower(substr("$firstname$i.$lastname", 0, 20));
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error("PERS&GRP: Error generating username with style NOME.COGNOME for user".$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style NOME.COGNOME");
            }
        } elseif ($usernameStyle == 'COGNOME.NOME') {
            $firstname = $this->ldapEscape(str_replace(array(' ', '\''), '', ($person->getFirstname())));
            $lastname = $this->ldapEscape(str_replace(array(' ', '\''), '', ($person->getLastname())));

            $tryUsername = strtolower(substr("$lastname.$firstname", 0, 20));
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = false;
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower(substr("$lastname$i.$firstname", 0, 20));
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error("PERS&GRP: Error generating username with style COGNOME.NOME for user".$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style COGNOME.NOME");
            }
        } elseif ($usernameStyle == 'PRIMONOME.COGNOME') {
            $explodedFirstname = explode(' ', $person->getFirstname());
            $firstFirstname = count($explodedFirstname) > 1 ? $explodedFirstname[0] : $person->getFirstname();
            $firstname = $this->ldapEscape(str_replace(array(' ', '\''), '', $firstFirstname));
            $fullFirstname = $this->ldapEscape(str_replace(array(' ', '\''), '', $person->getFirstname()));
            $lastname = $this->ldapEscape(str_replace(array(' ', '\''), '', ($person->getLastname())));

            $tryUsername = strtolower(substr("$firstname.$lastname", 0, 20));
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = strtolower(substr("$fullFirstname.$lastname", 0, 20));
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                }
            }

            if (!isset($username)) {
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower(substr("$firstname$i.$lastname", 0, 20));
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }

                    $tryUsername = strtolower(substr("$fullFirstname$i.$lastname", 0, 20));
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error("PERS&GRP: Error generating username with style NOME.COGNOME for user".$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style NOME.COGNOME");
            }
        } elseif ($usernameStyle == 'COGNOME.PRIMONOME') {
            $explodedFirstname = explode(' ', $person->getFirstname());
            $firstFirstname = count($explodedFirstname) > 1 ? $explodedFirstname[0] : $person->getFirstname();
            $firstname = $this->ldapEscape(str_replace(array(' ', '\''), '', $firstFirstname));
            $fullFirstname = $this->ldapEscape(str_replace(array(' ', '\''), '', $person->getFirstname()));
            $lastname = $this->ldapEscape(str_replace(array(' ', '\''), '', ($person->getLastname())));

            $tryUsername = strtolower(substr("$lastname.$firstname", 0, 20));
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!isset($username)) {
                $tryUsername = strtolower(substr("$lastname.$fullFirstname", 0, 20));
                if (!in_array($tryUsername, $existentUsernames)) {
                    $username = $tryUsername;
                }
            }

            if (!isset($username)) {
                $tryUsername = false;
                for ($i = 2; $i <= 9; ++$i) {
                    $tryUsername = strtolower(substr("$lastname$i.$firstname", 0, 20));
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }

                    $tryUsername = strtolower(substr("$lastname$i.$fullFirstname", 0, 20));
                    if (!in_array($tryUsername, $existentUsernames)) {
                        $username = $tryUsername;
                        break;
                    }
                }
            }

            if (!$tryUsername) {
                $this->logger->error("PERS&GRP: Error generating username with style COGNOME.PRIMONOME for user".$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style COGNOME.PRIMONOME");
            }
        } elseif ('NOMEUTENTE-DA-EMAIL' == $usernameStyle) {
            $tryUsername = strtolower(substr($person->getEmail(), 0, min(20, strpos($person->getEmail(), '@'))));
            if (!in_array($tryUsername, $existentUsernames)) {
                $username = $tryUsername;
            }

            if (!$tryUsername) {
                $this->logger->error('PERS&GRP: Error generating username with style NOMEUTENTE-DA-EMAIL for user'.$person->getFirstname().' '.$person->getLastname());
                throw new LoggedException("Impossible to create an username for the user {$person->getId()} with style COGNOME.PRIMONOME");
            }
        }

        if (!isset($username) || $username == '' || $username == '.') {
            $this->logger->error("PERS&GRP: Impossible to create an username for the user {$person->getId()}");
            throw new LoggedException("Impossible to create an username for the user {$person->getId()}");
        }

        if (substr($username, -1) == '.') {
            $username = substr($username, 0, -1);
        }

        return $username;
    }

    /**
     * Generate a random password for a user.
     *
     * @param bool $passwordComplexity
     * @param int  $passwordMinChar
     *
     * @return string
     */
    public function getRandomPassword($passwordComplexity = true, $passwordMinChar = 8)
    {
        $password = '';
        if ($passwordComplexity == true) {
            $password = 'Az-';
        }
        $len = $passwordMinChar - strlen($password);
        $password .= rand(pow(10, $len - 1), pow(10, $len) - 1);

        return $password;
    }

    /**
     * @return string
     */
    public function ldapEscape($subject)
    {
        return LdapGroupRepository::ldapEscape($subject);
    }
}
