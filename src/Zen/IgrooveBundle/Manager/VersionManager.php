<?php

namespace Zen\IgrooveBundle\Manager;

use Guzzle\Http\Client;
use Guzzle\Http\Exception\BadResponseException;
use Symfony\Component\Yaml\Yaml;


class VersionManager {

    public function __construct($kernelRootDir, $serializer, $configurationManager, $em) {
        $this->kernelRootDir = $kernelRootDir;
    }

    public function getVersions() {
        $versionFilesDir = $this->kernelRootDir . '/../src/Zen/IgrooveBundle/Resources/config/version/';

        $versionInstalled = ['versionInstalled' => "", 'buildInstalled' => ""];

        try {
            $versionInstalled = Yaml::parse(file_get_contents($versionFilesDir . 'versionInstalled.yml'));
        } catch (\Exception $e) {}

        $versionAvailable = ['versionAvailable' => "", 'buildAvailable' => ""];

        try {
            if(file_exists($versionFilesDir.'versionAvailable.yml'))
                $versionAvailable = Yaml::parse(file_get_contents($versionFilesDir . 'versionAvailable.yml'));
            else {
                $versionAvailable = Yaml::parse(file_get_contents($versionFilesDir . 'versionAvailable.yml.dist'));
                //@todo richiamare rabbitmq?
            }
        } catch (\Exception $e) {}

        return array_merge($versionAvailable['parameters'], $versionInstalled['parameters']);
    }

    public function checkNewVersion() {
        $guzzleClient = new Client();
        try {
            $request = $guzzleClient->get('https://bitbucket.org/vittorezen/igroove/raw/master/src/Zen/IgrooveBundle/Resources/config/version/version.yml?anti_cache=' . date('Ymdhis'));
            $response = $request->send();
            $versionAvailable = trim((string)$response->getBody());
            file_put_contents($this->kernelRootDir . '/../src/Zen/IgrooveBundle/Resources/config/version/versionAvailable.yml', $versionAvailable);
        } catch (BadResponseException $e) {
            echo "\r\nOps Error getting new version";
        }
    }


}
