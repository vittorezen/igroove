<?php

namespace Zen\IgrooveBundle\Manager;

use adLDAP\adLDAP;
use adLDAP\adLDAPException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Zen\IgrooveBundle\Entity\LdapUser;

class MicrosoftLdapService
{
    /**
     * Active Directory Interface.
     *
     * @var adLDAP
     */
    protected $adldap;

    /**
     * Active Directory Connection Configurations.
     *
     * @var array
     */
    protected $configurationAD;

//    /**
//     * Prefix for the AD group generated from iGroove.
//     *
//     * @var string
//     */
//    protected $generated_group_prefix;

    /**
     * Setup the Active Directory Configurations.
     *
     * @param $configurationAD
     * @throws \Exception
     */
    public function setParameters($configurationAD)
    {
        $this->configurationAD = $configurationAD;
        $this->adldap = new adLDAP($configurationAD);
//        $this->generated_group_prefix = $configurationAD['generated_group_prefix'];
    }

    /**
     * Return all igroove users in AD.
     *
     * @return array
     */
    public function getAllUsers()
    {
        $fields = ['samaccountname',
            'displayname',
            'description',
            'department',
            'givenname',
            'sn',
            'mail',
            'homedirectory',
            'homedrive',
            'msradius-framedinterfaceid',
            'objectguid',
        ];

        try {
            $ldapMembers = $this->adldap->user()->all($fields);
        } catch (AdldapException $e) {
            return [];
        }

        $ldapLists = array();
        if ($ldapMembers) {
            foreach ($ldapMembers as $v) {
                if (strpos($v['description'], 'igroove'.$this->configurationAD['sync_suffix'])) {
                    $ldapLists[] = array(
                        'username' => $v['samaccountname'],
                        'dn' => $v['displayname'],
                        'firstname' => $v['givenname'],
                        'lastname' => $v['sn'],
                        'distinguishedId' => $v['department'],
                        'parameters' => array(
                            'email' => $v['mail'],
                            'homedirectory' => $v['homedirectory'],
                            'homedrive' => $v['homedrive'],
                            'description' => $v['description'],
                            'radius-vlan' => (int) $v['msradius-framedinterfaceid'] > 0 ? (int) $v['msradius-framedinterfaceid'] : null,
                            'objectGUID' => $this->adldap->utilities()->binaryToText($v['objectguid']),
                        ),
                    );
                }
            }
        }

        return $ldapLists;
    }

    /**
     * Read the data of an user in AD.
     *
     * @param $username
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getUser($username)
    {
        $fields = ['samaccountname',
            'displayname',
            'description',
            'department',
            'givenname',
            'sn',
            'mail',
            'homedirectory',
            'homedrive',
            'msradius-framedinterfaceid',
            'objectguid',
        ];

        try {
            $ldapMemberData = $this->adldap->user()->info($username, $fields);
        } catch (AdldapException $e) {
            throw new \Exception("Error querying the ad user with username {$username}: ".$e->getMessage(), 0, $e);
        }

        if (!isset($ldapMemberData[0])) {
            throw new \Exception("Ad User with specified username {$username} does not exists");
        }

        $ldapMember = [];

        foreach ($fields as $field) {
            $ldapMember[$field] = isset($ldapMemberData[0][$field]) && isset($ldapMemberData[0][$field][0]) ? $ldapMemberData[0][$field][0] : null;
        }

        if (!strpos($ldapMember['description'], 'igroove'.$this->configurationAD['sync_suffix'])) {
            throw new \Exception("Ad User with specified username {$username} is not created with igroove");
        }

        return [
            'username' => $ldapMember['samaccountname'],
            'dn' => $ldapMember['displayname'],
            'firstname' => $ldapMember['givenname'],
            'lastname' => $ldapMember['sn'],
            'distinguishedId' => $ldapMember['department'],
            'parameters' => [
                'email' => $ldapMember['mail'],
                'homedirectory' => $ldapMember['homedirectory'],
                'homedrive' => $ldapMember['homedrive'],
                'description' => $ldapMember['description'],
                'radius-vlan' => (int) $ldapMember['msradius-framedinterfaceid'] > 0 ? (int) $ldapMember['msradius-framedinterfaceid'] : null,
                'objectGUID' => $this->adldap->utilities()->binaryToText($ldapMember['objectguid']),
            ],
        ];
    }

    /**
     * Get the ObjectGUID of an AD user from his username.
     *
     * @param $username
     *
     * @return string
     */
    public function getObjectGUIDFromUsername($username)
    {
        return $this->adldap->user()->usernameToGuid($username);
    }

    /**
     * Return the groups of an AD user.
     *
     * @param string $username
     *
     * @return array
     */
    public function getUserMembership($username)
    {
        try {
            $members = $this->adldap->user()->groups($username, false);
        } catch (adLDAPException $e) {
            $members = [];
        }

        return is_array($members) ? $members : [];
    }

    /**
     * Create a user in AD.
     *
     * @param LdapUser $ldapUser
     *
     * @throws \Exception
     */
    public function createUser(LdapUser $ldapUser)
    {
        $ldapUserAttributes = json_decode($ldapUser->getAttributes(), true);

        $attributes = array(
            'username' => $ldapUser->getUsername(),
            'display_name' => $ldapUser->getLastName().' '.$ldapUser->getFirstname(),
            'firstname' => $ldapUser->getFirstname(),
            'logon_name' => $ldapUser->getUsername().$this->configurationAD['account_suffix'],
            'surname' => trim($ldapUser->getLastName()),
            'description' => $ldapUserAttributes['description'],
            'department' => $ldapUser->getDistinguishedId(),
            'enabled' => 1,
            'password' => $ldapUser->getStartingPassword(),
            'container' => array(),
            'email' => $ldapUserAttributes['email'],
        );

        if (preg_match_all('/\d+/', $ldapUser->getUsername(), $matchedNumbers) > 0 && isset($matchedNumbers[0])) {
            $attributes['display_name'] .= " ".implode(" ", $matchedNumbers[0]);
        }

        $attributes['display_name'] = trim($attributes['display_name']);

        if (strlen($ldapUserAttributes['homedirectory']) > 3) {
            $attributes['home_drive'] = $ldapUserAttributes['homedrive'];
            $attributes['home_directory'] = $ldapUserAttributes['homedirectory'];
        }

        $attributes['msRADIUS-FramedInterfaceId'] = ((isset($ldapUserAttributes['radius-vlan'])) && (int) $ldapUserAttributes['radius-vlan'] > 0) ? $ldapUserAttributes['radius-vlan'] : null;

        try {
            $this->adldap->user()->create($attributes);
        } catch (AdldapException $e) {
            throw new \Exception("Error creating ad user {$ldapUser->getUsername()}: ".$e->getMessage(), 0, $e);
        }

        $ldapUser->setStartingPassword('');
    }

    /**
     * Modify the data of an user in AD.
     *
     * @param LdapUser $ldapUser
     *
     * @throws \Exception
     */
    public function modifyUser(LdapUser $ldapUser)
    {
        $ldapUserAttributes = json_decode($ldapUser->getAttributes(), true);

        $attributes = array(
            'username' => $ldapUser->getUsername(),
            'display_name' => $ldapUser->getLastName().' '.$ldapUser->getFirstname(),
            'firstname' => $ldapUser->getFirstname(),
            'logon_name' => $ldapUser->getUsername().$this->configurationAD['account_suffix'],
            'surname' => trim($ldapUser->getLastName()),
            'description' => $ldapUserAttributes['description'],
            'department' => ($ldapUser->getDistinguishedId()),
            'enabled' => 1,
            'container' => array(),
            'email' => $ldapUserAttributes['email'],
        );

        if (preg_match_all('/\d+/', $ldapUser->getUsername(), $matchedNumbers) > 0 && isset($matchedNumbers[0])) {
            $attributes['display_name'] .= " ".implode(" ", $matchedNumbers[0]);
        }

        $attributes['display_name'] = trim($attributes['display_name']);

        if ($ldapUser->getStartingPassword() != '') {
            $attributes['password'] = $ldapUser->getStartingPassword();
        }

        if (strlen($ldapUserAttributes['homedirectory']) > 3) {
            $attributes['home_drive'] = $ldapUserAttributes['homedrive'];
            $attributes['home_directory'] = $ldapUserAttributes['homedirectory'];
        } else {
            $attributes['home_drive'] = $attributes['home_directory'] = null;
        }

        $attributes['msRADIUS-FramedInterfaceId'] = ((isset($ldapUserAttributes['radius-vlan'])) && (int) $ldapUserAttributes['radius-vlan'] > 0) ? $ldapUserAttributes['radius-vlan'] : null;

        try {
            if (isset($ldapUserAttributes['objectGUID']) && $ldapUserAttributes['objectGUID'] != '') {
                $this->adldap->user()->modify($ldapUserAttributes['objectGUID'], $attributes, true);
            } else {
                $this->adldap->user()->modify(strtolower($ldapUser->getUsername()), $attributes);
            }
        } catch (AdldapException $e) {
            throw new \Exception("Error modifying ad user {$ldapUser->getUsername()}: ".$e->getMessage(), 0, $e);
        }

        if ($ldapUser->getStartingPassword() != '') {
            $ldapUser->setStartingPassword('');
        }
    }

    /**
     * Delete an user from AD.
     *
     * @param string $username
     *
     * @throws \Exception
     */
    public function removeUser($username)
    {
        try {
            $this->adldap->user()->delete($username);
        } catch (AdldapException $e) {
            throw new \Exception("Error deleting ad user {$username}: ".$e->getMessage(), 0, $e);
        }
    }

    /**
     * Change the password of a user in AD
     *
     * @param $username
     * @param $password
     * @throws \Exception
     */
    public function changeUserPassword($username, $password)
    {
        try {
            $user = $this->adldap->user()->infoCollection($username, array('samaccountname'));
        } catch (AdldapException $e) {
            throw new \Exception("Error querying the ad user with username {$username}: ".$e->getMessage(), 0, $e);
        }

        try {
            $this->adldap->user()->password($user->samaccountname, $password);
        } catch (adLDAPException $e) {
            throw new \Exception("Impossible to set the new password for the user {$username}: ".$e->getMessage(), 0, $e);
        }
    }

    /**
     * Check the validity of a user password
     *
     * @param $username
     * @param $password
     * @return bool
     */
    public function checkUserPassword($username, $password, $ignoreChangePasswordBindError = false)
    {
        try {
            return $this->adldap->user()->authenticate($username, $password, FALSE, $ignoreChangePasswordBindError);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * List all groups in AD.
     *
     * @return array
     */
    public function getAllGroups()
    {
        try {
            $groups = $this->adldap->group()->all(true);
        } catch (AdldapException $e) {
            return [];
        }

        if (!is_array($groups)) {
            return [];
        }

        $groupNames = [];
        foreach ($groups as $groupName => $description) {
            if (strpos($description, 'igroove'.$this->configurationAD['sync_suffix']) !== false) {
                $groupNames[] = $groupName;
            }
        }

        return $groupNames;
    }

    /**
     * Return the groups of an AD group.
     *
     * @param string $groupName
     *
     * @return array
     */
    public function getGroupMembership($groupName)
    {
        try {
            $list = $this->adldap->group()->inGroup($groupName, false);
        } catch (AdldapException $e) {
            return [];
        }

        $array = array();
        if (is_array($list)) {
            foreach ($list as $dn) {
                if (!strpos($dn, ',')) {
                    continue;
                }

                $array[] = substr($dn, 3, strpos($dn, ',') - 3);
            }
        }

        return $array;
    }

    /**
     * Create a group in AD.
     *
     * @param string $groupName
     *
     * @throws \Exception
     */
    public function createGroup($groupName)
    {
//        if (substr($groupName, 0, strlen($this->generated_group_prefix)) != $this->generated_group_prefix) {
//            throw new \Exception('Invalid group name to create in ad');
//        }

        $attributes = array(
            'group_name' => $groupName,
            'container' => array(),
            'description' => 'Creato da igroove'.$this->configurationAD['sync_suffix'],
        );

        try {
            $this->adldap->group()->create($attributes);
        } catch (AdldapException $e) {
            throw new \Exception("Error creating ad group {$groupName}: ".$e->getMessage(), 0, $e);
        } catch (ContextErrorException $e) {
            throw new \Exception("Error creating ad group {$groupName}: ".$e->getMessage(), 0, $e);
        }
    }

    /**
     * Rename a group in AD.
     *
     * @param string $groupName
     * @param string $newGroupName
     *
     * @throws \Exception
     */
    public function renameGroup($groupName, $newGroupName)
    {
        try {
            $this->adldap->group()->rename($groupName, $newGroupName, []);
        } catch (AdldapException $e) {
            throw new \Exception("Error renaming ad group {$groupName} to {$newGroupName}: ".$e->getMessage(), 0, $e);
        } catch (ContextErrorException $e) {
            throw new \Exception("Error renaming ad group {$groupName} to {$newGroupName}: ".$e->getMessage(), 0, $e);
        }
    }

    /**
     * Delete a group from AD.
     *
     * @param string $groupName
     *
     * @throws \Exception
     */
    public function removeGroup($groupName)
    {
        try {
            $this->adldap->group()->delete($groupName);
        } catch (AdldapException $e) {
            throw new \Exception("Error deleting ad group {$groupName}: ".$e->getMessage());
        }
    }

    /**
     * Update the users in an AD group.
     *
     * @param string $groupName
     * @param array  $users
     *
     * @throws \Exception
     */
    public function updateUsersIntoGroup($groupName, $users)
    {
        try {
            $groupMembers = $this->adldap->group()->members($groupName, false);
        } catch (adLDAPException $e) {
            throw new Exception("Error retrieving current group {$groupName} users: ".$e->getMessage());
        }

        if ($groupMembers === false) {
            $groupMembers = array();
        }

        $toRemove = array_diff($groupMembers, $users);
        $toAdd = array_diff($users, array_intersect($groupMembers, $users));
        $errors = [];

        foreach ($toRemove as $user) {
            try {
                $this->adldap->group()->removeUser($groupName, $user);
            } catch (adLDAPException $e) {
                $errors[] = "Error removing user {$user} from group {$groupName}: ".$e->getMessage();
            }
        }

        foreach ($toAdd as $user) {
            try {
                $this->adldap->group()->addUser($groupName, $user);
            } catch (adLDAPException $e) {
                $errors[] = "Error adding user {$user} to group {$groupName}: ".$e->getMessage();
            }
        }

        if (count($errors) > 0) {
            throw new Exception(implode("\n", $errors));
        }
    }

    /**
     * Update the AD groups into a group.
     *
     * @param string $groupName
     * @param array  $groups
     */
    public function updateGroupsIntoGroup($groupName, $groups)
    {
        try {
            $groupMembersLdap = @$this->adldap->group()->inGroup($groupName);
        } catch (adLDAPException $e) {
            throw new Exception("Error retrieving current group {$groupName} groups: ".$e->getMessage());
        }

        $groupMembers = array();
        if (is_array($groupMembersLdap)) {
            foreach ($groupMembersLdap as $DN) {
                $groupMembers[] = substr($DN, 3, strpos($DN, ',') - 3);
            }
        }

        $toRemove = array_diff($groupMembers, $groups);
        $toAdd = array_diff($groups, array_intersect($groupMembers, $groups));
        $errors = [];

        foreach ($toRemove as $group) {
//            if (substr($group, 0, strlen($this->generated_group_prefix)) != $this->generated_group_prefix) {
//                continue;
//            }

            try {
                $this->adldap->group()->removeGroup($groupName, $group);
            } catch (adLDAPException $e) {
                $errors[] = "Error removing group {$group} from group {$groupName}: ".$e->getMessage();
            }
        }

        foreach ($toAdd as $group) {
//            if (substr($group, 0, strlen($this->generated_group_prefix)) != $this->generated_group_prefix) {
//                continue;
//            }

            try {
                $this->adldap->group()->addGroup($groupName, $group);
            } catch (adLDAPException $e) {
                $errors[] = "Error adding group {$group} to group {$groupName}: ".$e->getMessage();
            }
        }

        if (count($errors) > 0) {
            throw new Exception(implode("\n", $errors));
        }
    }

    /**
     * Check if the ou specified exists in Ldap/Ad.
     *
     * @param string $ouName
     *
     * @return bool
     */
    public function ouExists($ouName, $path = '')
    {
        $path = $path != '' ? explode('/', $path) : [];
        $path[] = $ouName;
        $exists = false;

        try {
            $this->adldap->folder()->listing(array_reverse($path), adLdap::ADLDAP_FOLDER, false);
            $exists = true;
        } catch (adLDAPException $e) {
        }

        return $exists;
    }

    /**
     * Create the specified ou in Ldap/Ad.
     *
     * @param string $ouName
     * @param string $path
     *
     * @throws \Exception
     */
    public function createOu($ouName, $path = '')
    {
        $attributes = [
            'ou_name' => $ouName,
            'container' => $path != '' ? explode('/', $path) : [],
        ];

        try {
            $this->adldap->folder()->create($attributes);
        } catch (AdldapException $e) {
            throw new \Exception("Error creating ad ou {$ouName}: ".$e->getMessage(), 0, $e);
        }
    }

    /**
     * Move the specified username into the specified ou.
     *
     * @param string $ouName
     * @param string $username
     * @param string $path
     *
     * @throws \Exception
     */
    public function moveUserIntoOu($ouName, $username, $path = '')
    {
        $path = $path != '' ? explode('/', $path) : [];
        $path[] = $ouName;

        try {
            $this->adldap->user()->move($username, $path);
        } catch (adLDAPException $e) {
            throw new adLDAPException("Error moving user {$username} to ou {$ouName}: ".$e->getMessage());
        }
    }

    /**
     * Move the specified users username into the specified ou.
     *
     * @param string $ouName
     * @param array  $users
     * @param string $path
     *
     * @throws adLDAPException
     */
    public function updateUsersIntoOu($ouName, $users, $path = '')
    {
        $path = $path != '' ? explode('/', $path) : [];
        $path[] = $ouName;

        $errors = [];
        foreach ($users as $user) {
            try {
                $this->adldap->user()->move($user, $path);
            } catch (adLDAPException $e) {
                $errors[] = "{$user}: ".$e->getMessage();
            }
        }

        if (count($errors) > 0) {
            throw new adLDAPException("Error moving users to ou {$ouName}: ".implode("\n", $errors));
        }
    }

    /**
     * Move the specified group to the specified ou.
     *
     * @param $ouName
     * @param $groupName
     * @param string $path
     *
     * @throws adLDAPException
     */
    public function moveGroupIntoOu($ouName, $groupName, $path = '')
    {
        $path = $path != '' ? explode('/', $path) : [];
        $path[] = $ouName;

        try {
            $this->adldap->group()->rename($groupName, $groupName, $path);
        } catch (adLDAPException $e) {
            throw new adLDAPException("Error moving group {$groupName} to ou {$ouName}: ".$e->getMessage());
        }
    }

    /**
     * Move the specified groups into the specified ou.
     *
     * @param string $ouName
     * @param array  $groups
     * @param string $path
     *
     * @throws adLDAPException
     */
    public function updateGroupsIntoOu($ouName, $groups, $path = '')
    {
        $path = $path != '' ? explode('/', $path) : [];
        $path[] = $ouName;

        $errors = [];
        foreach ($groups as $group) {
            try {
                $this->adldap->group()->rename($group, $group, $path);
            } catch (adLDAPException $e) {
                $errors[] = "{$group}: ".$e->getMessage();
            }
        }

        if (count($errors) > 0) {
            throw new adLDAPException("Error moving group to ou {$ouName}: ".implode("\n", $errors));
        }
    }

    /**
     * Extract the OUs path from a dn AD path.
     *
     * @param $dnPath
     *
     * @return array
     */
    public function dnPathToOUList($dnPath)
    {
        $path = str_replace($this->configurationAD['base_dn'], '', $dnPath);
        $explodedPath = explode(',', $path);
        $ous = [];
        foreach ($explodedPath as $p) {
            if (substr($p, 0, 3) == 'OU=') {
                $ous[] = substr($p, 3);
            }
        }

        return $ous;
    }

    /**
     * Refresh connection to AD.
     *
     * @throws adLDAPException
     */
    public function refreshConnection()
    {
        $this->adldap->close();
        $this->adldap->connect();
    }

    public function getConnectedServerHostname()
    {
        $ldapConnection = $this->adldap->getLdapConnection();
        ldap_get_option($ldapConnection, LDAP_OPT_HOST_NAME, $returnVal);
        return $returnVal;
    }
}
