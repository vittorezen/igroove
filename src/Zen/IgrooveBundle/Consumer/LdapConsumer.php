<?php

namespace Zen\IgrooveBundle\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Zen\IgrooveBundle\Manager\LdapProxy;
use Doctrine\ORM\EntityManager;

class LdapConsumer extends AbstractConsumer
{
    private $ldapProxy;
    private $em;
    private $startTime;

    public function __construct(LdapProxy $ldapProxy, EntityManager $em)
    {
        $this->ldapProxy=$ldapProxy;
        $this->em = $em;
        $this->startTime = time();
    }

    public function execute(AMQPMessage $msg)
    {
        $message = unserialize($msg->body);
        if (!array_key_exists('command', $message)) {
            return true;
        }

        $this->em->clear();
        if(time()-$this->startTime > 300) {
            $this->em->getConnection()->close();
            $this->em->getConnection()->connect();
            $this->ldapProxy->refreshConnection();
        }

        try {
            switch ($message['command']) {
                case "syncInternetAccessLdapGroup":
                    $this->ldapProxy->syncInternetAccessLdapGroup();
                    break;

                case "purgeAndPopulate":
                    $this->ldapProxy->purgeAndPopulateAll();
                    break;

                case "syncLDAPfromDB";
                    $this->ldapProxy->syncLDAPfromDB();
                    break;
            }
        } catch(\Throwable $e) {
            return $this->retryOnError($msg, $e);
        }

        return ConsumerInterface::MSG_ACK;
    }
}