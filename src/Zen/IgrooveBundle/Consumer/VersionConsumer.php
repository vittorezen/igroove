<?php

namespace Zen\IgrooveBundle\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Zen\IgrooveBundle\Exception\ConnectionException;

class VersionConsumer extends AbstractConsumer
{
    private $versionManager;

    public function __construct($versionManager)
    {
        $this->versionManager = $versionManager;
    }

    public function execute(AMQPMessage $msg)
    {
        $message = unserialize($msg->body);
        if (!array_key_exists('command', $message)) {
            return ConsumerInterface::MSG_ACK;
        }

        try {
            if ($message['command'] == 'checkNewVersion') {
                $this->versionManager->checkNewVersion();
            }
        } catch(\Throwable $e) {
            return $this->retryOnError($msg, $e);
        }

        return ConsumerInterface::MSG_ACK;
    }
}