<?php

namespace Zen\IgrooveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Student;
use Zen\IgrooveBundle\LdapTool;
use adLDAP\adLDAP;
use Zen\IgrooveBundle\Manager\GoogleApps;

class TestMikrotikCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('test:mikrotik')
            ->setDescription('test');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $mikrotikManager = $this->getContainer()->get('zen.igroove.mikrotik');
        var_dump($mikrotikManager->addPools());
        var_dump($mikrotikManager->manageMacToHotspot());
        var_dump($mikrotikManager->addIpToIpList());
        var_dump($mikrotikManager->addIpReservation());


    }


}