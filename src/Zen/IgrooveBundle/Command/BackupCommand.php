<?php

namespace Zen\IgrooveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use Zen\IgrooveBundle\Manager\ConfigurationManager;


class BackupCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('backup')
            ->setDescription('Backup database and relevant files');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $em = $this->getContainer()->get('doctrine')->getManager();
        $configurationManager = $this->getContainer()->get('zen.igroove.configuration');

        if (!$configurationManager->getDizdaCloudBackupUsername()) {
            die("Manca lo username Dropbox nella configurazione\r\n");
        }
        if (!$configurationManager->getDizdaCloudBackupPassword()) {
            die("Manca la password Dropbox nella configurazione\r\n");
        }

        $yaml = Yaml::parse(
            file_get_contents($this->getContainer()->get('kernel')->getRootDir() . '/config/config.yml')
        );
        $yaml['dizda_cloud_backup']['cloud_storages']['dropbox'] = array(
            'user' => $configurationManager->getDizdaCloudBackupUsername(),
            'password' => $configurationManager->getDizdaCloudBackupPassword()
        );
        $new_yaml = Yaml::dump($yaml, 5);
        file_put_contents($this->getContainer()->get('kernel')->getRootDir() . '/config/config.yml', $new_yaml);

        $args = array(
            'command' => 'dizda:backup:start'
        );
        $command = new ArrayInput($args);
        $this->getApplication()->run($command);

        $this->output->writeln("<info>All done!</info>");
    }

}
