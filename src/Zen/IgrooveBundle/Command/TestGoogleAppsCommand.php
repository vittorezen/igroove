<?php

namespace Zen\IgrooveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Student;
use Zen\IgrooveBundle\LdapTool;
use adLDAP\adLDAP;
use Zen\IgrooveBundle\Manager\GoogleApps;

class TestGoogleAppsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('test:google-apps')
            ->setDescription('test');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger = $this->getContainer()->get('logger');

        $providers = $em->getRepository('ZenIgrooveBundle:Provider')->findBy(array('active' => true));
        foreach ($providers as $provider) {

            if ($provider->getStudentGoogleAppDomain()) {
                $output->writeln("<info>" . $provider->getStudentGoogleAppDomain() . "</info>");
                $googleApps = new GoogleApps(
                    $provider->getStudentGoogleAppDomain(),
                    $provider->getStudentGoogleAppClientId(),
                    $provider->getStudentGoogleAppclientSecret(),
                    $provider->getStudentGoogleAppOUPath(),
                    '/var/www/igroove/app',
                    $logger
                );
                $n = rand(1, 10);
//                $n = 7;
                $output->writeln("Crea utente: test-utente-" . $n);
                $googleApps->createUser('test-utente-' . $n, 'nome' . $n, 'cognome' . $n);
                $output->writeln("Crea gruppo: test-utente-" . $n);
                $googleApps->createGroup('test-gruppo-' . $n);
                $output->writeln("Aggiungi utente al gruppo");
                $googleApps->addUserToGroup('test-utente-' . $n, 'test-gruppo-' . $n);
                $output->writeln("Crea OU: test-ou-" . $n);
                $googleApps->createOU('test-ou-' . $n);
                $output->writeln("Sposta utente nella OU");
                $googleApps->addUserToOU('test-utente-' . $n, 'test-ou-' . $n);

//                $output->writeln("Sposta utenti nell gruppo");
//                $googleApps->updateGroupMembers(['test-utente-4', 'test-utente-7'], 'test-gruppo-7');
//                $output->writeln("Sposta utenti nella OU");
//                $googleApps->updateOUMembers(['test-utente-4', 'test-utente-7'], 'test-ou-1');
            }

        }
    }


}