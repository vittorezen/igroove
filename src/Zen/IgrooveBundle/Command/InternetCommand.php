<?php

namespace Zen\IgrooveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zen\IgrooveBundle\Entity\LdapGroup;
use Zen\IgrooveBundle\LdapTool;
use Zen\IgrooveBundle\MikrotikPool;
use adLDAP\adLDAP;
use Zen\IgrooveBundle\Manager\InternetAccessManager;
use Zen\IgrooveBundle\Manager\ConfigurationManager;


class InternetCommand extends ContainerAwareCommand
{

//    /**
//     * @var \Doctrine\ORM\EntityManager
//     */
    protected $em;
    /**
     * @var OutputInterface
     */
    protected $output;
    /**
     * @var \Zen\IgrooveBundle\Repository\InternetOpenRepository
     */
    protected $internetOpenRepository;
    /**
     * @var \Zen\IgrooveBundle\Repository\LdapGroupRepository
     */
    protected $ldapGroupRepository;
    protected $adGeneratedGroupPrefix;
    protected $ldapNeedSync = false;

    protected function configure()
    {
        $this
            ->setName('internet')
            ->setDescription('Check default internet behavior');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->logger = $this->getContainer()->get('logger');
        $this->printAndLogInfo("Starting internet command");
        $configurationManager = $this->getContainer()->get('zen.igroove.configuration');
        $this->adGeneratedGroupPrefix = $configurationManager->getActiveDirectoryGeneratedGroupPrefix();
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->internetOpenRepository = $this->em->getRepository('ZenIgrooveBundle:InternetOpen');
        $this->ldapGroupRepository = $this->em->getRepository('ZenIgrooveBundle:LdapGroup');

        //print the current active entity
        $this->printAndLogInfo("-Current active users, groups and ip lists:");
        $currentActiveEntities = $this->internetOpenRepository->findAll();
        $currentActiveEntitiesByType = ['group' => [], 'user' => [], 'ipList' => []];
        foreach($currentActiveEntities as $entity) {
            $currentActiveEntitiesByType[$entity->getType()][] = $entity->getAccount();
        }

        foreach ($currentActiveEntitiesByType as $type => $accounts) {
            if(empty($accounts)) {
                continue;
            }

            $this->printAndLogInfo("--[{$type}] ".implode(" ", $accounts));
        }

        $this->checkProvidersActionsAndStatus();
        $this->checkInternetOpensTimeout();

        if($this->ldapNeedSync) {
            $this->em->flush();
            $this->printAndLogInfo("-LdapGroup Updated");

            $this->printAndLogInfo('-syncInternetAccessLdapGroup via RabbitMQ');
            $msg = array('command' => 'syncInternetAccessLdapGroup', 'parameters' => array());
            $ldapclient = $this->getContainer()->get('old_sound_rabbit_mq.ldap_service_producer');
            $ldapclient->publish(serialize($msg));
        }

        $mikrotikManager = $this->getContainer()->get('zen.igroove.mikrotik');
        $this->printAndLogInfo("-Mikrotik KickOffUsers");
        $mikrotikManager->KickOffUsers();

        if($configurationManager->getWirelessUnifiController() != "" && $configurationManager->getWirelessUnifiKickExpired()) {
            $unifiManager = $this->getContainer()->get('zen.igroove.unifi');
            $this->printAndLogInfo("-Unifi kickOffClientExpiredClients");
            try {
                $unifiManager->kickOffClientExpiredClients();
            } catch (\Exception $e) {
                $this->printAndLogInfo("--Errore durante Unifi kickOffClientExpiredClients: ".$e->getMessage());
            }
        }

        $this->printAndLogInfo("All done!");
        echo PHP_EOL;

        $this->em->getRepository('ZenIgrooveBundle:Cron')->setLatestRun('internet');
    }
    
    protected function checkProvidersActionsAndStatus()
    {
        //check available command to execute in providers
        $providers = $this->em->getRepository('ZenIgrooveBundle:Provider')->findBy(array('active' => true));
        $nowOneMinuteDown = new \DateTime('now -1 minutes');
        $nowOneMinuteUp = new \DateTime('now +1 minutes');
        $commandsToSet = array();

        foreach ($providers as $provider) {
            $rows = explode("\r\n", trim($provider->getInternetOpenAccessRange()));
            foreach ($rows as $row) {
                if (strlen(trim($row)) == 0) {
                    continue;
                }
                list($time, $check) = explode(": ", trim($row));
                $time = trim($time);
                $check = trim($check);
                $timeToCheck = new \DateTime($time);
                if (($timeToCheck >= $nowOneMinuteDown) and ($timeToCheck <= $nowOneMinuteUp)) {
                    $commandsToSet[$provider->getName()] = $check;
                }
            }
        }

        //get current internet access group member
        $internetAccessGroup = $this->getInternetAccessGroup();

        //cycle command to set from provider
        $this->printAndLogInfo("-Check providers command");
        foreach ($commandsToSet as $name => $toSet) {
            $providerGroupMembers = $this->getLdapGroupMembers($name);
            if(empty($providerGroupMembers->group) || !is_array($providerGroupMembers->group))
                continue;

            if ($toSet == 'on') {
                //add all classroom groups in the provider into the internet access group
                foreach ($providerGroupMembers->group as $groupName) {
                    $internetAccessGroup->addMember('group', $groupName);
                }

                $this->ldapNeedSync = true;
                $this->printAndLogInfo("--Groups added to internetaccess group: ".implode(",",$providerGroupMembers->group));
            } elseif($toSet == 'off') {
                foreach($providerGroupMembers->group as $providerClassroomGroup) {
                    $providerClassroomGroupMembers = $this->getLdapGroupMembers($providerClassroomGroup);

                    //remove user in current classroom group from internetaccess group and internetopen table
                    if(is_array($providerClassroomGroupMembers->user)) {
                        foreach($providerClassroomGroupMembers->user as $providerClassroomGroupUser) {
                            $iouEntities = $this->internetOpenRepository->findBy(array('type' => 'user', 'account' => strtolower($providerClassroomGroupUser)));
                            foreach ($iouEntities as $entity) {
                                $this->em->remove($entity);
                            }

                            $internetAccessGroup->removeMember('user', $providerClassroomGroupUser);
                        }
                    }

                    //remove the classroom group from internetaccess group and internetopen table
                    $internetAccessGroup->removeMember('group', $providerClassroomGroup);

                    $iogEntities = $this->internetOpenRepository->findBy(array('type' => 'group', 'account' => strtolower($providerClassroomGroup)));
                    foreach ($iogEntities as $entity) {
                        $this->em->remove($entity);
                    }
                    $this->em->flush();

                    $this->ldapNeedSync = true;
                    $this->printAndLogInfo("--Group {$providerClassroomGroup} and his users removed from internetaccess group");
                }
            }
        }
    }

    protected function checkInternetOpensTimeout()
    {
        $internetAccessGroup = $this->getInternetAccessGroup();
        $personalDeviceAccessGroup = $this->getPersonalDeviceAccessGroup();

        //query the expired activation
        $this->printAndLogInfo("-Check single user, group and ip list timeout from internetaccess table");
        $date = new \DateTime();
        $query = $this->internetOpenRepository
            ->createQueryBuilder('i')
            ->select('i.id, i.type, i.account, i.permitPersonalDevices')
            ->where(' i.close_at <= :date')
            ->setParameter('date', $date)
            ->getQuery();
        $internetOpens = $query->getResult();

        $updateIpList = false;
        foreach ($internetOpens as $internetOpen) {
            if ($internetOpen['type'] == 'group') {
                $this->printAndLogInfo("--remove group: " . $internetOpen['account']);

                if($internetAccessGroup->memberExists('group', $internetOpen['account'])) {
                    $internetAccessGroup->removeMember('group', $internetOpen['account']);
                    $this->ldapNeedSync = true;
                }

                if($internetOpen['permitPersonalDevices'] && $personalDeviceAccessGroup->memberExists('group', $internetOpen['account'])) {
                    $personalDeviceAccessGroup->removeMember('group', $internetOpen['account']);
                    $this->ldapNeedSync = true;
                }
            } elseif ($internetOpen['type'] == 'user') {
                $this->printAndLogInfo("--remove user " . $internetOpen['account']);

                if($internetAccessGroup->memberExists('user', $internetOpen['account'])) {
                    $internetAccessGroup->removeMember('user', $internetOpen['account']);
                    $this->ldapNeedSync = true;
                }

                if($internetOpen['permitPersonalDevices'] && $personalDeviceAccessGroup->memberExists('user', $internetOpen['account'])) {
                    $personalDeviceAccessGroup->removeMember('user', $internetOpen['account']);
                    $this->ldapNeedSync = true;
                }
            } elseif($internetOpen['type'] == 'ipList') {
                $this->printAndLogInfo("--remove iplist " . $internetOpen['account']);
                $updateIpList = true;
            }

            //remove the internetOpen entity
            $internetOpen = $this->em->getReference('ZenIgrooveBundle:InternetOpen', $internetOpen['id']);
            $this->em->remove($internetOpen);
            $this->em->flush();
        }

        if($updateIpList) {
            $this->printAndLogInfo('-syncInternetAccessLdapGroup via RabbitMQ');
            $msg = array('command' => 'addIpToBypassedList', 'parameters' => array());
            $mktclient = $this->getContainer()->get('old_sound_rabbit_mq.mikrotik_service_producer');
            $mktclient->publish(serialize($msg));
        }
    }

    protected function getInternetAccessGroup() {
        $internetAccessGroup = $this->ldapGroupRepository->findOneBy(['name' => $this->adGeneratedGroupPrefix . 'InternetAccess']);
        if(!$internetAccessGroup instanceof LdapGroup) {
            $personsAndGroups = $this->getContainer()->get('personsAndGroups');
            $internetAccessGroup = $personsAndGroups->checkInternetAccessLdapGroups();
        }

        return $internetAccessGroup;
    }

    protected function getPersonalDeviceAccessGroup() {
        $personalDeviceAccessGroup = $this->ldapGroupRepository->findOneBy(['name' => $this->adGeneratedGroupPrefix . 'PersonalDeviceAccess']);
        if(!$personalDeviceAccessGroup instanceof LdapGroup) {
            $personsAndGroups = $this->getContainer()->get('personsAndGroups');
            $personalDeviceAccessGroup = $personsAndGroups->checkPersonalDeviceAccessLdapGroups();
        }

        return $personalDeviceAccessGroup;
    }

    protected function getLdapGroupMembers($nameOrGroup) {
        $groupMembers = $group = null;
        if(is_string($nameOrGroup)) {
            $group = $this->ldapGroupRepository->findOneBy(['name' => $this->adGeneratedGroupPrefix . $this->ldapGroupRepository::ldapEscape($nameOrGroup)]);
        } elseif($nameOrGroup instanceof LdapGroup) {
            $group = $nameOrGroup;
        }

        if($group instanceof LdapGroup && $group->getMembers() != "") {
            $groupMembers = json_decode($group->getMembers());
        }

        if($groupMembers === null) {
            $groupMembers = new \stdClass();
        }

        if(!isset($groupMembers->group) || $groupMembers->group == null) {
            $groupMembers->group = [];
        } elseif (is_object($groupMembers->group)) {
            $groupMembers->group = (array)$groupMembers->group;
        }

        if(!isset($groupMembers->user)) {
            $groupMembers->user = [];
        } elseif (is_object($groupMembers->user)) {
            $groupMembers->user = (array)$groupMembers->user;
        }

        return $groupMembers;
    }

    protected function printAndLogInfo($message) {
        $this->output->writeln('<info>'.$message.'</info>');
        $this->logger->info("CRON:".$message);
    }
}
