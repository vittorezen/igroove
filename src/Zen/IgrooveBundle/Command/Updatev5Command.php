<?php

namespace Zen\IgrooveBundle\Command;

use Doctrine\DBAL\Schema\SchemaDiff;
use Doctrine\ORM\NativeQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;
use Zen\IgrooveBundle\Entity\DeviceIp;
use Zen\IgrooveBundle\Manager\ConfigurationManager;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\DBAL\Schema\Comparator;
use Doctrine\ORM\Query\ResultSetMapping;

class Updatev5Command extends ContainerAwareCommand
{

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    

    protected function configure()
    {
        $this
            ->setName('updatev5')
            ->setDescription('Update database to version 5 of igroove')
            ->addOption('start-point', 'sp', InputOption::VALUE_OPTIONAL, '', '')
            ->addOption('list-start-point', 'lsp', InputOption::VALUE_NONE)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        /* @var $em \Doctrine\ORM\EntityManager */
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->metadatas = $this->em->getMetadataFactory()->getAllMetadata();

        if ( empty($this->metadatas)) {
            $output->writeln('No Metadata Classes to process.');
            return 0;
        }

        $migFunctions = [];
        foreach (get_class_methods($this) as $method) {
            if (substr($method, 0, 4) == "mig_") {
                $migFunctions[] = $method;
            }
        }

        if($input->getOption('list-start-point')) {
            $output->writeln('Migration function list:');
            foreach ($migFunctions as $k => $name) {
                $output->writeln("{$k} => ".substr($name,4));
            }
            return;
        }

        $startPoint = $input->getOption('start-point');
        $startMig = 0;
        if($startPoint != "") {
            $startMig = (int)$startPoint;
        }

        for($i=$startMig;$i < count($migFunctions); $i++) {
            $funcName = $migFunctions[$i];
            if($funcName != "") {
                $this->output->writeln('<info>Call func '.$i.' '.$funcName."</info>");
                $this->$funcName();
            }
        }

        $this->em->flush();

        $this->output->writeln("<info>All done!</info>");
        return 0;
    }
    
    
    protected function mig_renameMacToDevice() {
        $this->output->writeln("<info>Rename Mac table to Device table</info>");
        $this->em->getConnection()->query("RENAME TABLE `Mac` TO `Device`");
    }
    
    protected function mig_renameUsersgroupsToStudentgroups() {
        $this->output->writeln("<info>Rename users_groups table to students_groups table</info>");
        $this->em->getConnection()->query("RENAME TABLE `users_groups` TO `students_groups`");
    }
    
    protected function mig_deleteHomework() {
        $this->output->writeln("<info>Delete Homework Table</info>");
        $this->em->getConnection()->query("DROP TABLE `Homework`");
    }
    
    protected function mig_alterProviderFields() {
        $this->output->writeln("<info>Alter provider table fields</info>");
        $this->em->getConnection()->query("ALTER TABLE Provider
                 CHANGE `autoCreateUsername` `studentAutoCreateUsername` tinyint(1) DEFAULT NULL,
                 CHANGE `forceImportedPassword` `studentForceImportedPassword` tinyint(1) DEFAULT NULL,
                 CHANGE `googleAppDomain` `studentGoogleAppDomain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                 CHANGE `googleAppClientId` `studentGoogleAppClientId` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                 CHANGE `googleAppClientSecret` `studentGoogleAppClientSecret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                 CHANGE `googleAppAutoEmail` `studentGoogleAppAutoEmail` tinyint(1) DEFAULT NULL,
                 CHANGE `googleAppCreateGroup` `studentGoogleAppCreateGroup` tinyint(1) DEFAULT NULL,
                 CHANGE `googleAppGroupPrefix` `studentGoogleAppGroupPrefix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                 CHANGE `googleAppCreateOU` `studentGoogleAppCreateOU` tinyint(1) DEFAULT NULL,
                 CHANGE `googleAppOUPrefix` `studentGoogleAppOUPrefix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                 CHANGE `googleAppOUPath` `studentGoogleAppOUPath` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL");

    }
    
    protected function mig_firstSchema() {
        $schemaTool = new SchemaTool($this->em);
        $sm = $this->em->getConnection()->getSchemaManager();
        $fromSchema = $sm->createSchema();
        $toSchema = $schemaTool->getSchemaFromMetadata($this->metadatas);

        $comparator = new Comparator();
        $schemaDiff = $comparator->compare($fromSchema, $toSchema);

        if(isset($schemaDiff->changedTables['device'])) {
            unset($schemaDiff->changedTables['device']);
        }

        if(isset($schemaDiff->changedTables['mikrotikpool'])) {
            $schemaDiff->changedTables['mikrotikpool']->changedColumns = [];
            $schemaDiff->changedTables['mikrotikpool']->removedColumns = [];
            $schemaDiff->changedTables['mikrotikpool']->removedIndexes = [];
        }

        if(isset($schemaDiff->changedTables['student'])) {
            unset($schemaDiff->changedTables['student']);
        }

        if(isset($schemaDiff->changedTables['group'])) {
            $schemaDiff->changedTables['group']->addedIndexes = [];
            $schemaDiff->changedTables['group']->changedIndexes = [];
        }

        if(isset($schemaDiff->changedTables['provider'])) {
            $schemaDiff->changedTables['provider']->removedColumns = [];
        }

        if(isset($schemaDiff->newTables['teachersubjectgroup'])) {
            unset($schemaDiff->newTables['teachersubjectgroup']);
        }

        if(isset($schemaDiff->changedTables['students_groups'])) {
            $schemaDiff->changedTables['students_groups']->addedColumns = [];
            $schemaDiff->changedTables['students_groups']->addedIndexes = [];
            $schemaDiff->changedTables['students_groups']->addedForeignKeys = [];
            $schemaDiff->changedTables['students_groups']->changedIndexes = [];
            $schemaDiff->changedTables['students_groups']->removedIndexes = [];
            $schemaDiff->changedTables['students_groups']->removedColumns = [];
//            $schemaDiff->changedTables['students_groups']->removedForeignKeys = [];
        }
        

        $this->output->writeln("<info>Executing first schema update</info>");
        if(!$this->applySchema($schemaDiff)) {
            throw new \Exception();
        }
        $this->output->writeln("<info>First schema update executed</info>");
    }
    
    protected function mig_updateMikrotikpoolIpToInteger() {
        $this->output->writeln("<info>Updating mikrotikpool ip to integer</info>");
        $macsPrepstat = $this->em->getConnection()->prepare('SELECT p.id, p.ipStart, p.ipEnd FROM MikrotikPool p' );
        $macsPrepstat->execute();
        $mikrotikPools = $macsPrepstat->fetchAll();
        foreach($mikrotikPools as $mikrotikPoolData) {
            if(!isset($mikrotikPoolData['id']) || !isset($mikrotikPoolData['ipStart']) || !isset($mikrotikPoolData['ipEnd']))
                continue;

            $mikrotikPool = $this->em->getRepository('ZenIgrooveBundle:MikrotikPool')->find($mikrotikPoolData['id']);
            $mikrotikPool->setDottedIpStart($mikrotikPoolData['ipStart']);
            $mikrotikPool->setDottedIpEnd($mikrotikPoolData['ipEnd']);
            $this->em->persist($mikrotikPool);
            $this->output->writeln("* {$mikrotikPool->getNome()} updated");
        }
        $this->em->flush();
    }

    protected function mig_InsertingUuidFroGroup() {
        $this->output->writeln("<info>Inserting uuid for group</info>");
        $this->em->getConnection()->query("UPDATE `Group` Set id=uuid()");
    }

    protected function mig_moveIdColumnInGroupTable() {
        $this->output->writeln("<info>Moving id column in group table</info>");
        $this->em->getConnection()->query("ALTER TABLE `Group` CHANGE id id char(36) NOT NULL COMMENT '(DC2Type:guid)' FIRST");
    }

    protected function mig_updateProviderFilterData() {
        $this->output->writeln("<info>Updating provider filter data</info>");
        $providerPrepstat = $this->em->getConnection()->prepare('SELECT p.id, p.jsonKey, p.filter, p.uri FROM Provider p' );
        $providerPrepstat->execute();
        $providers = $providerPrepstat->fetchAll();
        foreach($providers as $providerData) {
            if(!isset($providerData['id']) || !isset($providerData['filter']) || !isset($providerData['jsonKey']) || $providerData['filter'] == "" || $providerData['jsonKey'] == "")
                continue;

            $provider = $this->em->getRepository('ZenIgrooveBundle:Provider')->find($providerData['id']);
            $filterData = [];
            switch($providerData['filter']) {
                case "gsd":
                    $filterData['uri'] = $providerData['uri'];
                    $filterData['secretKey'] = $providerData['jsonKey'];
                    break;
                case "segremat":
                    $filterData['uri'] = $providerData['uri'];
                    $filterData['secretKey'] = $providerData['jsonKey'];
                    break;
                case "segrematweb":
                    $filterData['uri'] = $providerData['uri'];
                    $filterData['secretKey'] = $providerData['jsonKey'];
                    break;
                case "mafol":
                    $providerDataExploded = explode("==",$providerData['jsonKey']);
                    $filterData['uri'] = $providerData['uri'];
                    $filterData['idSede'] = $providerDataExploded[0];
                    $filterData['secretKey'] = $providerDataExploded[1];
                    break;

                case "spaggiari":
                    $providerDataExploded = explode("==",$providerData['jsonKey']);
                    $filterData['as'] = $providerDataExploded[0];
                    $filterData['guid'] = $providerDataExploded[1];
                    $filterData['cid'] = $providerDataExploded[2];
                    $filterData['login'] = $providerDataExploded[3];
                    $filterData['password'] = $providerDataExploded[4];
                    break;

            }

            $provider->setFilterData($filterData);
            $this->em->persist($provider);
            $this->em->flush();
            $this->output->writeln("* {$provider->getName()} updated");
        }
    }

    protected function mig_secondSchema() {
        $schemaTool = new SchemaTool($this->em);
        $sm = $this->em->getConnection()->getSchemaManager();
        $fromSchema = $sm->createSchema();
        $toSchema = $schemaTool->getSchemaFromMetadata($this->metadatas);

        $comparator = new Comparator();
        $schemaDiff = $comparator->compare($fromSchema, $toSchema);

        if(isset($schemaDiff->changedTables['device'])) {
            $schemaDiff->changedTables['device']->removedColumns = [];
        }

        if(isset($schemaDiff->changedTables['mikrotikpool'])) {
            $schemaDiff->changedTables['mikrotikpool']->addedColumns = [];
            $schemaDiff->changedTables['mikrotikpool']->removedIndexes = [];
        }

        if(isset($schemaDiff->changedTables['student'])) {
            $schemaDiff->changedTables['student']->addedIndexes = [];
        }

        if(isset($schemaDiff->changedTables['group'])) {
            $schemaDiff->changedTables['group']->addedColumns = [];
            $schemaDiff->changedTables['group']->addedForeignKeys = [];
            $schemaDiff->changedTables['group']->changedColumns = [];
        }

        if(isset($schemaDiff->changedTables['provider'])) {
            $schemaDiff->changedTables['provider'] = clone $schemaDiff->changedTables['provider'];
            $schemaDiff->changedTables['provider']->addedColumns = [];
        }

        if(isset($schemaDiff->newTables['teachersubjectgroup'])) {
            unset($schemaDiff->newTables['teachersubjectgroup']);
        }

        if(isset($schemaDiff->changedTables['students_groups'])) {
            $schemaDiff->changedTables['students_groups']->changedIndexes = [];
            $schemaDiff->changedTables['students_groups']->changedColumns = [];
            $schemaDiff->changedTables['students_groups']->removedColumns = [];
            $schemaDiff->changedTables['students_groups']->removedForeignKeys = [];
            $schemaDiff->changedTables['students_groups']->renamedIndexes = [];
            $schemaDiff->changedTables['students_groups']->addedForeignKeys = [];
        }

        $this->output->writeln("<info>Executing second schema update</info>");
        if(!$this->applySchema($schemaDiff)) {
            throw new \Exception();
        }
        $this->output->writeln("<info>Second schema update executed</info>");
    }

    protected function mig_copyStudentFiscalCodeIntoFiscalcodeField() {
        $this->output->writeln("<info>Copying Student Fiscal code from id to fiscalCode</info>");
        $this->em->getConnection()->query("UPDATE `Student` SET `fiscalCode` = `id`");
    }

    protected function mig_changeStudentIdToUuid() {
        $this->output->writeln("<info>Change id for student to uuid</info>");
        $this->em->getConnection()->query("UPDATE `Student` Set id=uuid()");
    }

    protected function mig_updateStudentIdInOtherTables() {
        $this->output->writeln("<info>Updating in students_groups and LdapUser</info>");
        $newStudents = $this->em->getRepository('ZenIgrooveBundle:Student')->findAll();
        foreach ($newStudents as $student) {
            $this->em->getConnection()->query("UPDATE `students_groups` SET student_id = '{$student->getId()}' WHERE user_id = '{$student->getFiscalCode()}'");
            $this->em->getConnection()->query("UPDATE `LdapUser` SET distinguishedId = '{$student->getId()}', operation = 'MODIFY' WHERE distinguishedId = '{$student->getFiscalCode()}'");
            $this->output->writeln("* {$student->getFiscalCode()} updated");
        }
    }

    protected function mig_setGroupidInStudentgroupTable() {
        $this->output->writeln("<info>Updating student-group relationship with group_id instead of group_name</info>");
        $newGroups = $this->em->getRepository('ZenIgrooveBundle:Group')->findAll();
        foreach($newGroups as $newGroup) {
            $this->em->getConnection()->query("UPDATE `students_groups` SET group_id = '{$newGroup->getId()}' WHERE group_name = '{$newGroup->getName()}'");
            $this->output->writeln("* {$newGroup->getName()} updated");
        }
    }

    protected function mig_insertNumericIpInDeviceIpTable() {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('Zen\IgrooveBundle\Entity\Device', 'm');
        $rsm->addFieldResult('m', 'id', 'id');
        $rsm->addFieldResult('m', 'device', 'device');
        $rsm->addFieldResult('m', 'ips', 'mac');

        $query = $this->em->createNativeQuery('SELECT m.id, m.ips, m.device FROM Device m', $rsm);
        $macs = $query->getResult();

        $uniqueIp = [];
        $deviceIps = $this->em->getRepository('ZenIgrooveBundle:DeviceIp')->findAll();
        foreach ($deviceIps as $deviceIp) {
            $uniqueIp[$deviceIp->getIp()] = $deviceIp->getIp();
        }

        $this->output->writeln("<info>Inserting ip address in deviceip table</info>");
        foreach($macs as $mac) {
            $ips = trim($mac->getMac());
            if($ips == "")
                continue;

            foreach(explode(",",$ips) as $ip) {
                if(isset($uniqueIp[$ip])) {
                    $this->output->writeln("<warning>* duplicated ip {$ip} for device {$mac->getDevice()}</warning>");
                    continue;
                }

                $deviceIp = new DeviceIp();
                $deviceIp->setIp($ip);
                $deviceIp->setMac($mac);
                if($pool = $this->em->getRepository('ZenIgrooveBundle:MikrotikPool')->searchPoolForIp($deviceIp->getRawIp())) {
                    $deviceIp->setMikrotikPool($pool);
                }

                $this->em->persist($deviceIp);
                $this->em->flush();
                $this->output->writeln("* {$ip} inserted");
                $uniqueIp[$ip] = $ip;
            }
        }
    }

    protected function mig_thirdSchema() {
        $schemaTool = new SchemaTool($this->em);
        $sm = $this->em->getConnection()->getSchemaManager();
        $fromSchema = $sm->createSchema();
        $toSchema = $schemaTool->getSchemaFromMetadata($this->metadatas);

        $comparator = new Comparator();
        $schemaDiff = $comparator->compare($fromSchema, $toSchema);

        $this->output->writeln("<info>Executing third schema update</info>");
        if(!$this->applySchema($schemaDiff)) {
            throw new \Exception();
        }
        $this->output->writeln("<info>Third schema update executed</info>");
    }
    

    protected function applySchema(SchemaDiff $schemaDiff) {
        $updateSchemaSql = $schemaDiff->toSql($this->em->getConnection()->getDatabasePlatform());
        $conn = $this->em->getConnection();

        foreach ($updateSchemaSql as $sql) {
            try {
                $conn->executeQuery($sql);
            } catch(\Doctrine\DBAL\DBALException $e) {
                $this->output->writeln("<warning>There was an error executing query ({$sql}): {$e->getMessage()}</warning>");
                return false;
            }
        }

        return true;
    }

}
