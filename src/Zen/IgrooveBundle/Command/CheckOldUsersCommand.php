<?php

namespace Zen\IgrooveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Zen\IgrooveBundle\Manager\GoogleApps;
use Zen\IgrooveBundle\Repository\LdapUserRepository;
use Zen\IgrooveBundle\Repository\StudentRepository;

class CheckOldUsersCommand extends ContainerAwareCommand
{
    protected $em;
    /**
     * @var OutputInterface
     */
    protected $output;
    /**
     * @var StudentRepository
     */
    protected $studentRepository;
    /**
     * @var LdapUserRepository
     */
    protected $ldapRepository;
    protected $ldapProxy;
    protected $configurationManager;
    protected $logger;

    protected function configure()
    {
        $this
            ->setName('checkOldUsers')
            ->setDescription('Check users that doesn\'t exists in providers')->addOption(
                'compareLdap',
                null,
                InputOption::VALUE_NONE,
                'Compare ldap users with students'
            )->addOption(
                'oldUsersGroup',
                null,
                InputOption::VALUE_NONE,
                'Check if the student is associated with old group'
            )->addOption(
                'delete',
                null,
                InputOption::VALUE_NONE,
                'User found that should not exist will be deleted'
            )->addOption(
                'deleteEmail',
                null,
                InputOption::VALUE_NONE,
                'Email of users found will be deleted'
            )->addOption(
                'moveEmailInOu',
                null,
                InputOption::VALUE_OPTIONAL,
                'If used, the email will be moved into OU specified instead of being deleted'
            );


    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->logger = $this->getContainer()->get('logger');

        $this->logAndDisplay("Inizio controllo nei provider\n");
        if($input->getOption('delete')){
            $this->logAndDisplay("*Eliminazione Attivata*\n");

            if($input->getOption('moveEmailInOu') && $input->getOption('moveEmailInOu') != ""){
                $this->logAndDisplay("*Spostamento email in OU {$input->getOption('moveEmailInOu')}*\n");
            } elseif ($input->getOption("deleteEmail")) {
                $this->logAndDisplay("*Eliminazione Email Attivata*\n");
            }
        }

        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->configurationManager = $this->getContainer()->get('zen.igroove.configuration');
        $this->ldapProxy = $this->getContainer()->get('zen.igroove.microsoftLdap');
        $this->ldapProxy->setParameters($this->configurationManager->getActiveDirectoryConfiguration());
        $this->studentRepository = $this->em->getRepository('ZenIgrooveBundle:Student');
        $this->ldapRepository = $this->em->getRepository('ZenIgrooveBundle:LdapUser');

        if($input->getOption("compareLdap")) {
            $this->checkLdapUsers($input->getOption('delete'));
        } else {
            $providers = $this->em->getRepository('ZenIgrooveBundle:Provider')->findBy(array('active' => true));
            foreach ($providers as $provider) {
                if($input->getOption("oldUsersGroup")) {
                    $this->checkUsersOldGroup($provider, $input->getOption('delete'));
                } else {
                    $this->checkUsersOfProvider($provider, $input->getOption('delete'), $input->getOption('deleteEmail'), $input->getOption('moveEmailInOu'));
                }
            }
        }


        $this->logAndDisplay("Fine");
    }

    private function checkUsersOfProvider($provider, $delete = false, $deleteEmail = false, $moveEmailInOU = "") {
        if (!$provider->getFilter() || $provider->getFilter() == "databaseinterno") {
            return;
        }

        $this->logAndDisplay("[".$provider->getName()."]");

        $filter = $this->getContainer()->get($provider->getFilter());
        $filter->setUri($provider->getUri());
        $filter->setSecretKey($provider->getJsonKey());
        $providerStudents = $filter->getElements();
        $currentStudents = $this->studentRepository->findByProvider($provider->getId());

        if($provider->getGoogleAppClientId() != "") {
            $googleApps = new GoogleApps(
                $provider->getGoogleAppDomain(),
                $provider->getGoogleAppClientId(),
                $provider->getGoogleAppClientSecret(),
                $provider->getGoogleAppOUPath(),
                $this->getContainer()->getParameter('kernel.cache_dir'),
                $this->logger
            );

            if($delete && $moveEmailInOU != "" && $googleApps->createOU($moveEmailInOU)) {
                $this->logAndDisplay("-creazione della OU '{$moveEmailInOU}' se non esistente su googleapps");
            }
        } else {
            $googleApps = false;
        }

        $this->logAndDisplay("-".count($providerStudents)." studenti dal provider");
        $this->logAndDisplay("-".count($currentStudents)." studenti del provider in igroove");

        $currentStudentsById = [];
        foreach($currentStudents as $currentStudent) {
            $currentStudentsById[$currentStudent->getId()] = $currentStudent;
        }

        $this->logAndDisplay("-Lista utenti del provider non presenti in igroove:");
        foreach($providerStudents as $providerStudent) {
            if(isset($currentStudentsById[$providerStudent['id']])) {
                unset($currentStudentsById[$providerStudent['id']]);
            } else {
                $this->logAndDisplay("-- ".$providerStudent['id']." ".$providerStudent['lastname']." ".$providerStudent['firstname']." ".$providerStudent['group']);
            }
        }

        $this->logAndDisplay("\n-Lista utenti non presenti nel provider (".count($currentStudentsById)."):");
        foreach($currentStudentsById as $student) {
            $this->logAndDisplay("-- ".$student->getUsername()." ".$student->getId()." ".$student->getLastname()." ".$student->getFirstname());

            if($delete) {
                $deletedFrom = "--- Deleted from DB ";
                if($this->ldapProxy->removeUser($student->getUsername())) {
                    $deletedFrom .= "LDAP ";

                    if($ldapUser = $this->ldapRepository->find($student->getUsername())) {
                        $deletedFrom .= "LDAPdb ";
                        $this->em->remove($ldapUser);
                    }
                }

                if($googleApps instanceof GoogleApps) {
                    if($moveEmailInOU != "") {
                        if($googleApps->addUserToOU($student->getUsername(),$moveEmailInOU)) {
                            $deletedFrom .= "GMAIL (MOVED) ";
                        }
                    } else if($deleteEmail && $googleApps->removeUser($student->getUsername())) {
                        $deletedFrom .= "GMAIL ";
                    }
                }

                $this->em->remove($student);

                $this->logAndDisplay($deletedFrom);
            }
        }

        $this->em->flush();
    }

    private function checkLdapUsers($delete = false) {
        $currentStudents = $this->studentRepository->findAll();
        $ldapUsers =  $this->ldapRepository->findAll();

        $this->logAndDisplay("-".count($ldapUsers)." studenti in ldap");
        $this->logAndDisplay("-".count($currentStudents)." studenti in igroove");

        $currentStudentsByUsername = [];
        foreach($currentStudents as $currentStudent) {
            $currentStudentsByUsername[$currentStudent->getUsername()] = $currentStudent;
        }

        $this->logAndDisplay("-Lista utenti ldap non presenti in igroove:");
        foreach($ldapUsers as $ldapUser) {
            if(isset($currentStudentsByUsername[$ldapUser->getUsername()])) {
                unset($currentStudentsByUsername[$ldapUser->getUsername()]);
                continue;
            }

            $this->logAndDisplay("-- ".$ldapUser->getUsername()." ".$ldapUser->getDistinguishedId());

            if($delete && $this->ldapProxy->removeUser($ldapUser->getUsername())) {
                $this->logAndDisplay("--- Deleted from LDAP ");
                $this->em->remove($ldapUser);
            }
        }

        $this->logAndDisplay("\n-Lista utenti non presenti in ldap (".count($currentStudentsByUsername)."):");
        foreach($currentStudentsByUsername as $student) {
            $this->logAndDisplay("-- ".$student->getUsername()." ".$student->getId()." ".$student->getLastname()." ".$student->getFirstname());
        }
        $this->em->flush();
    }

    private function checkUsersOldGroup($provider, $delete = false) {
        if (!$provider->getFilter() || $provider->getFilter() == "databaseinterno") {
            return;
        }

        $this->logAndDisplay("[".$provider->getName()."]");

        $filter = $this->getContainer()->get($provider->getFilter());
        $filter->setUri($provider->getUri());
        $filter->setSecretKey($provider->getJsonKey());
        $providerStudents = $filter->getElements();
        $currentStudents = $this->studentRepository->findByProvider($provider->getId());

        $providerStudentsById = [];
        foreach($providerStudents as $providerStudent) {
            $providerStudentsById[$providerStudent['id']] = $providerStudent;
        }

        $this->logAndDisplay("-Lista gruppi in avanzo:");
        foreach ($currentStudents as $currentStudent) {
            if(!isset($providerStudentsById[$currentStudent->getId()]) || $providerStudentsById[$currentStudent->getId()]['group'] == "") {
                continue;
            }

            $currentStudentGroups = $currentStudent->getNotManuallyManagedMember();
            if(count($currentStudentGroups) < 2) {
                continue;
            }

            $providerGroup = $providerStudentsById[$currentStudent->getId()]['group'];
            foreach ($currentStudentGroups as $currentStudentGroup) {
                if($providerGroup == $currentStudentGroup->getName()) {
                    continue;
                }

                $this->logAndDisplay("--".$currentStudentGroup->getName()." da ".$currentStudent->getUsername());

                if($delete) {
                    $currentStudent->removeMemberOf($currentStudentGroup);
                }
            }
        }
        $this->em->flush();
    }

    private function logAndDisplay($message) {
        $this->logger->info($message);
        $this->output->writeln($message);
    }
}
