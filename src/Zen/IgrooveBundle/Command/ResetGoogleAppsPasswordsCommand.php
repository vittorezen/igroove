<?php

namespace Zen\IgrooveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Student;
use Zen\IgrooveBundle\LdapTool;
use adLDAP\adLDAP;
use Zen\IgrooveBundle\Manager\GoogleApps;

class ResetGoogleAppsPasswordsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('google-apps-reset-password')
            ->setDescription('reset all the users email password to the starting password in igroove');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger = $this->getContainer()->get('logger');

        $providers = $em->getRepository('ZenIgrooveBundle:Provider')->findBy(array('active' => true));
        foreach ($providers as $provider) {
            if(!$provider->getGoogleAppDomain())
                return;

            $googleApps = new GoogleApps(
                $provider->getGoogleAppDomain(),
                $provider->getGoogleAppClientId(),
                $provider->getGoogleAppclientSecret(),
                $provider->getGoogleAppOUPath(),
                '/var/www/igroove/app',
                $logger
            );

            foreach($provider->getStudents() as $student) {
                if($student->getEmail() == "")
                    continue;

                $googleApps->resetUserPassword($student->getEmail(),$student->getStartingPassword());
            }
        }
    }
}