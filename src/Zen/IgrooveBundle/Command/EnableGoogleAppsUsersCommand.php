<?php

namespace Zen\IgrooveBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Student;
use Zen\IgrooveBundle\LdapTool;
use adLDAP\adLDAP;
use Zen\IgrooveBundle\Manager\GoogleApps;

class EnableGoogleAppsUsersCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('g-suite-enable-all-users')
            ->setDescription('re-enable all the users account');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $logger = $this->getContainer()->get('logger');

        $providers = $em->getRepository('ZenIgrooveBundle:Provider')->findBy(array('active' => true));
        foreach ($providers as $provider) {
            if(!$provider->getStudentGoogleAppDomain())
                return;

            $googleApps = new GoogleApps(
                $provider->getStudentGoogleAppDomain(),
                $provider->getStudentGoogleAppClientId(),
                $provider->getStudentGoogleAppclientSecret(),
                $provider->getStudentGoogleAppOUPath(),
                '/var/www/igroove/app',
                $logger
            );

            foreach($provider->getStudents() as $student) {
                if($student->getEmail() == "")
                    continue;
                try {
                    $googleApps->setUnsuspendedUser($student->getEmail());
                } catch (\Exception $gse) {
                    echo "GOOGLEAPPS: User ".$student->getEmail()." not found: " . $gse->getMessage();
                }
            }
        }
    }
}