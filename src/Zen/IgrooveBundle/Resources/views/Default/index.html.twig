{% extends '::base.html.twig' %}

{% block body %}
    {% if is_granted('ROLE_RECEPTION') %}
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Reception</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="{{ url('guest') }}">Gestione degli ospiti</a></li>
                </ul>
            </div>
        </div>
    {% endif %}

    {% if not menu_render.onlyReception %}
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Utente</h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked">
                    {% if menu_render.userCanChangeOwnPassword %}
                        <li><a href="{{ url ('changeMyPassword') }}">Cambia la tua password</a></li>
                    {% endif %}

                    {% if menu_render.showBadge %}
                        <li><a href="{{ url ('printMyBadge' ) }}">Stampa il tuo badge informatico</a></li>
                    {% endif %}
                </ul>
            </div>
        </div>


        {% if is_granted('ROLE_TEACHER') %}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Insegnante</h3>
                </div>
                <div class="panel-body">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href="{{ url('group_providers_list') }}">Visualizza gli elenchi di classe</a></li>
                    </ul>
                </div>
            </div>
        {% endif %}
    {% endif %}

    {% if is_granted('ROLE_ADMIN') %}
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Amministratore</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="{{ url('student_list') }}" data-prefetch>Lista Studenti</a></li>
                            <li><a href="{{ url('teacher_list') }}" data-prefetch>Lista Docenti</a></li>
                            {#<li role="separator" class="divider"><hr /></li>#}
                            <li><a href="{{ url('config_edit') }}" data-prefetch>Configurazione igroove</a></li>
                            <li><a href="{{ url('config_theme') }}" data-prefetch>Cambia il look di igroove</a></li>
                            <li><a href="{{ url('service') }}" data-prefetch>Riavvia i servizi</a></li>
                            <li><a href="{{ path('sonata_admin_dashboard') }}" target="_blank">Database dashboard</a>
                            </li>
                            <li><a href="{{ app.request.getSchemeAndHttpHost() }}:15672/#/queues" target="_blank">Queues
                                    status</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Mikrotik</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="{{ url('mikrotik') }}" data-prefetch>Router</a></li>
                            <li><a href="{{ url('device') }}" data-prefetch>Gestione ip e dispositivi</a></li>
                            <li><a href="{{ url('mikrotik_pool') }}" data-prefetch>Pool di indirizzi IP</a></li>
                            <li><a href="{{ url('mikrotik_list') }}" data-prefetch>Liste IP</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        {% if version.buildAvailable>version.buildInstalled %}
            <div class="alert alert-dismissable alert-info">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Ci sono novità!</strong> Stai usando igroove versione {{ version.versionInstalled }}
                build {{ version.buildInstalled }} mentre è
                disponibile la versione {{ version.versionAvailable }} build {{ version.buildAvailable }}.
            </div>
        {% endif %}
    {% endif %}

    {% if is_granted('ROLE_TEACHER') %}
        {% if showStats %}

            <script src="//code.highcharts.com/4.0.1/highcharts.js"></script>
            <script src="//code.highcharts.com/4.0.1/modules/exporting.js"></script>

            <script type="text/javascript">
                $(function () {

                    $('#chartInternet').highcharts({
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: 0,
                            plotShadow: false
                        },
                        title: {
                            text: 'Utenti<br>su internet',
                            align: 'center',
                            verticalAlign: 'middle',
                            y: 50
                        },
                        tooltip: {
                            pointFormat: 'Utenti: <b>{point.y}</b>'
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                    enabled: true,
                                    distance: 10,
                                    style: {
                                        color: 'black'

                                    }
                                },
                                startAngle: -90,
                                endAngle: 90,
                                center: ['50%', '75%']
                            }
                        },
                        series: [{
                            type: 'pie',
                            name: 'Utenti connessi',
                            innerSize: '50%',
                            data: []
                        }]
                    });
                    $('#chartWireless').highcharts({
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: 0,
                            plotShadow: false
                        },
                        title: {
                            text: 'Utenti<br>su wireless',
                            align: 'center',
                            verticalAlign: 'middle',
                            y: 50
                        },
                        tooltip: {
                            pointFormat: 'Utenti: <b>{point.y}</b>'
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                    enabled: true,
                                    distance: 10,
                                    style: {
                                        color: 'black'
                                    }
                                },
                                startAngle: -90,
                                endAngle: 90,
                                center: ['50%', '75%']
                            }
                        },
                        series: [{
                            type: 'pie',
                            name: 'Utenti connessi',
                            innerSize: '50%',
                            data: []
                        }]
                    });


                    var chartInternet = $('#chartInternet').highcharts();
                    var chartWireless = $('#chartWireless').highcharts();

                    function updateChart() {
                        if (chartInternet) {
                            $.ajax({
                                type: "GET",
                                dataType: "json",
                                url: '{{ path('stats_utenti',{'server':'all'}) }}',
                                success: function (data) {
                                    chartInternet.series[0].setData(data, true);
                                },
                                cache: false
                            })
                        }
                        if (chartWireless) {
                            $.ajax({
                                type: "GET",
                                dataType: "json",
                                url: '{{ path('stats_utenti',{'server':wirelessHotspotServer}) }}',
                                success: function (data) {
                                    chartWireless.series[0].setData(data, true);
                                },
                                cache: false
                            })
                        }
                    }


                    // Bring life to the dials
                    setInterval(function () {

                        updateChart();
                    }, 10000);

                    updateChart();
                });
            </script>
            <div class="row">
                <div id="chartInternet" class="col-md-6"></div>
                <div id="chartWireless" class="col-md-6"></div>
            </div>

        {% endif %}
    {% endif %}
{% endblock %}


        {% block menu %}
            <div class="navbar navbar-default navbar-fixed-top">
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ path('homepage') }}" alt="igroove">igroove</a>
                </div>
                <div class="navbar-collapse collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('help') }}">Aiuto</a></li>
                        <li><a href="{{ url('logout') }}">Esci</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ path('credits') }}">Credits</a></li>
                        <li><a href="https://bitbucket.org/vittorezen/igroove/issues?status=new&status=open">Segnala un
                                problema</a></li>
                    </ul>
                </div>
            </div>
        {% endblock %}
