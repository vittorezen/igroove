<?php namespace Zen\IgrooveBundle\RepositoryAction;

use Zen\IgrooveBundle\Entity\Group;
use Zen\IgrooveBundle\Manager\PersonsAndGroups;

class GroupRepositoryAction {

    protected $personsAndGroups;

    /**
     * StudentRepositoryAction constructor.
     */
    public function __construct(PersonsAndGroups $personsAndGroups) {
        $this->personsAndGroups = $personsAndGroups;
    }


    public function executeAfterCreate(Group $group) {
        $errors = [];
        
        try {
            $this->personsAndGroups->syncGroupWithLdapGroup($group, null, false, true);
            $this->personsAndGroups->syncGroupWithLdapGroup($group, null, true, true);
        } catch (\Exception $e) {
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante la creazione del gruppo su AD", $e);
        }

        try {
            $this->personsAndGroups->syncGroupWithGoogleGroup($group, "", false, true);
            $this->personsAndGroups->syncGroupWithGoogleGroup($group, "", true, true);
        } catch (\Exception $e) {
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante la creazione del gruppo su GApps", $e);
        }

        if(count($errors) > 0) {
            throw new \Exception(join(PHP_EOL, $errors));
        }
    }

    public function executeAfterUpdate(Group $group, $oldName) {
        $errors = [];

        if($group->getName() != $oldName) {
            try {
                $this->personsAndGroups->syncGroupWithLdapGroup($group, $oldName, false, true);
                $this->personsAndGroups->syncGroupWithLdapGroup($group, $oldName, true, true);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante il cambio di nome del gruppo su AD", $e);
            }

            try {
                $this->personsAndGroups->syncGroupWithGoogleGroup($group, $oldName, false, true);
                $this->personsAndGroups->syncGroupWithGoogleGroup($group, $oldName, true, true);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante il cambio di nome del gruppo su GApps", $e);
            }
        }

        try {
            $this->personsAndGroups->syncGroupMembersVlan($group);
        } catch (\Exception $e) {
            $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'aggiornamento delle VLAN dei membri del gruppo su AD", $e);
        }

        if(count($errors) > 0) {
            throw new \Exception(join(PHP_EOL, $errors));
        }
    }

    public function executeBeforeRemove(Group $group, $deleteOnLdap=false, $deleteOnGApps=false) {
        $errors = [];

        if($deleteOnLdap) {
            try {
                $this->personsAndGroups->removeGroupLdapGroup($group);
                $this->personsAndGroups->removeGroupLdapGroup($group, true);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione del gruppo da AD", $e);
            }
        }

        if($deleteOnGApps) {
            try {
                $this->personsAndGroups->removeGroupGoogleAppsGroup($group);
                $this->personsAndGroups->removeGroupGoogleAppsGroup($group,true);
            } catch (\Exception $e) {
                $errors[] = $this->personsAndGroups->prepareFormErrorMessage("Errore durante l'eliminazione del gruppo da GApps", $e);
            }
        }

        //@todo remove OU

        if(count($errors) > 0) {
            throw new \Exception(join(PHP_EOL, $errors));
        }
    }

}