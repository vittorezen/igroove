<?php namespace Zen\IgrooveBundle\ImporterFilter\ImportedEntity;

class Group extends AbstractEntity {

    protected $fieldToCheck = ['name'];

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $sectorId;

    /**
     * @var \Zen\IgrooveBundle\Entity\Sector
     */
    protected $sector;

    /**
     * Group constructor.
     *
     * @param string $id
     * @param string $name
     * @param string $sectorId
     */
    public function __construct($id, $name, $sectorId) {
        $this->name = $name;
        $this->sectorId = $sectorId;
        parent::__construct($id);
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSectorId() {
        return $this->sectorId;
    }

    /**
     * @param string $sectorId
     */
    public function setSectorId($sectorId) {
        $this->sectorId = $sectorId;
    }

    /**
     * @return \Zen\IgrooveBundle\Entity\Sector
     */
    public function getSector() {
        return $this->sector;
    }

    /**
     * @param \Zen\IgrooveBundle\Entity\Sector $sector
     */
    public function setSector($sector) {
        $this->sector = $sector;
    }

    /**
     * @return array
     */
    public function toArray() {
        return [
            'name'              => $this->name,
            'sectorId'          => $this->sectorId,
            'internalSectorId'  => $this->sector instanceof \Zen\IgrooveBundle\Entity\Sector ? $this->sector->getId() : null,
        ];
    }
}