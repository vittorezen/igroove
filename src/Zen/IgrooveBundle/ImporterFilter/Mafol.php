<?php

namespace Zen\IgrooveBundle\ImporterFilter;

use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Group;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student;

class Mafol extends AbstractFilter
{
    static $name = "Mafol";
    static $internalName = "mafol";
    static $parametersUi = ['uri' => ['title' => "URI della fonte dati", 'type' => "text"], 'secretKey' => ['title' => "Chiave Segreta", 'type' => "text"], 'idSede' => ['title' => "Id Sede", 'type' => "text"]];
    protected $uri;
    protected $secretKey;
    protected $idSede;

    public function setParameters($parameters)
    {
        parent::setParameters($parameters);
    }

    public function parseRemoteData()
    {

        $token = md5($this->parameters['secretKey'] . date('ymd'));

        $client = new \SoapClient($this->parameters['uri'], array('trace' => 1));
        $parameters = array('idSede' => $this->parameters['idSede'], 'token' => $token);

        $response = $client->__soapCall('getAlunni', array($parameters));

        $resultArray = json_decode($response->getAlunniResult, true);
        if (sizeof($resultArray) < 3) {
            return;
        }

        foreach ($resultArray as $k => $v) {


            $remove = array(
                '^',
                ',',
                '.',
                ':',
                '/',
                '\\',
                ',',
                '=',
                '+',
                '<',
                '>',
                ';',
                '"',
                '#',
                "'",
                '(',
                ')',
                "'",
                "\x00",
                '?',
                '.',
                '-',
                '!',
                '°',
                '*'
            );
            $classe = trim($v['Group']);
            $classe = str_replace($remove, '', $classe);
            $idClasse = md5(strtolower($classe));


            if (trim(strtolower($v['id'])) == '') {
                continue;
            }
            if (strlen(trim($classe)) == 0) {
                continue;
            }
            $this->groups[$idClasse] = new Group($idClasse, $classe, 0);


            $id = $v['id'];
            if (isset($v['mafol-ID'])) {
                $mafolId = $v['mafol-ID'];

                $this->students[$mafolId] = new Student($mafolId, trim(strtolower($v['id'])), trim(ucwords(strtolower($v['FirstName']))), trim(ucwords(strtolower($v['LastName']))), $idClasse, trim(strtolower($v['Email'])));
            }
        }
    }
}

