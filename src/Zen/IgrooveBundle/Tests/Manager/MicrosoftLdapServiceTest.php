<?php

namespace Zen\IgrooveBundle\Tests\Manager;

use adLDAP\adLDAP;
use adLDAP\adLDAPException;
use Zen\IgrooveBundle\Entity\LdapUser;
use Zen\IgrooveBundle\Manager\MicrosoftLdapService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class MicrosoftLdapServiceTest extends KernelTestCase {
    
    protected static $configurationAD;
    /**
     * @var adLDAP
     */
    protected static $adLdap;

    public static $testUserData = [
        'username' => "test2.user",
        'dn' => "Test User 2",
        'firstname' => "User",
        'lastname' => "Test",
        'distinguishedId' => "XXXXXX00X00XXXXX",
        'parameters' => [
            "description" => "Creato da igroovetest",
            "homedrive" => "H:",
            "homedirectory" => "\\\\testsrv\\test2",
            "email" => "test2.user@test",
            "radius-vlan" => "12"
        ]
    ];

    protected static $changedTestUserData = [
        'username' => "test3.user",
        'dn' => "Test changed User changed 3",
        'firstname' => "User changed",
        'lastname' => "Test changed",
        'distinguishedId' => "XXXXXX00X00XXXXY",
        'parameters' => [
            "description" => "Creato da igroovetest",
            "homedrive" => "F:",
            "homedirectory" => "\\\\testsrv\\test3",
            "email" => "test3.user@test",
            "radius-vlan" => ""
        ]
    ];

    protected static $testGroupName = "A Test Group";
    protected static $testOuName = "A Test Ou";

    public static function setUpBeforeClass() {
        self::bootKernel();
        self::$configurationAD = static::$kernel->getContainer()->getParameter('adLdap_configuration');
        self::$adLdap = new adLDAP(self::$configurationAD);
        self::$testGroupName = self::$configurationAD['generated_group_prefix'] . self::$testGroupName;
    }

    public function getTestLdapUser() {
        $ldapUser = new LdapUser();
        $ldapUser->setUsername(self::$testUserData['username']);
        $ldapUser->setDistinguishedId(self::$testUserData['distinguishedId']);
        $ldapUser->setFirstname(self::$testUserData['firstname']);
        $ldapUser->setLastname(self::$testUserData['lastname']);
        $ldapUser->setStartingPassword("Az-12345");
        $ldapUser->setAttributes(json_encode(self::$testUserData['parameters']));

        return $ldapUser;
    }

    public function testCreateUser() {
        $ldapUser = $this->getTestLdapUser();

        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->createUser($ldapUser);

        $userFromLdap = self::$adLdap->user()->info(self::$testUserData['username'], ['description']);

        $this->assertNotEmpty($userFromLdap);
        $this->assertArraySubset([0 => ['description' => [0 => self::$testUserData['parameters']['description']]]], $userFromLdap);
    }

    public function testCreateDuplicateUserFail() {
        $ldapUser = $this->getTestLdapUser();

        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $this->expectException(\Exception::class);

        $microsoftLdapService->createUser($ldapUser);
    }

    public function testGetUser() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $userFromLdap = $microsoftLdapService->getUser(self::$testUserData['username']);

        $this->assertTrue(is_array($userFromLdap));
        $this->assertArraySubset(self::$testUserData, $userFromLdap);
    }

    public function testGetAllUsers() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $usersFromLdap = $microsoftLdapService->getAllUsers();

        $this->assertNotEmpty($usersFromLdap);

        $testUser = null;
        foreach ($usersFromLdap as $userFromLdap) {
            if(isset($userFromLdap['username']) && $userFromLdap['username'] == self::$testUserData['username']) {
                $testUser = $userFromLdap;
                break;
            }
        }

        $this->assertNotNull($testUser);
        $this->assertArraySubset(self::$testUserData, $testUser);
    }


    public function testCreateGroup() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->createGroup(self::$testGroupName);

        $groupFromAd = self::$adLdap->group()->info(self::$testGroupName);

        $this->assertNotEmpty($groupFromAd);
        $this->assertArrayHasKey(0, $groupFromAd);
        $this->assertArrayHasKey('description', $groupFromAd[0]);
    }

    public function testCreateDuplicateGroupFail() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $this->expectException(\Exception::class);

        $microsoftLdapService->createGroup(self::$testGroupName);
    }

    public function testGetAllGroups() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $groupsFromLdap = $microsoftLdapService->getAllGroups();

        $this->assertNotEmpty($groupsFromLdap);
        $this->assertNotFalse(array_search(self::$testGroupName,$groupsFromLdap));
    }

    public function testUpdateUsersIntoGroup() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->updateUsersIntoGroup(self::$testGroupName, [self::$testUserData['username']]);

        $groupMember = self::$adLdap->group()->members(self::$testGroupName);

        $this->assertNotEmpty($groupMember);
        $this->assertArraySubset([self::$testUserData['username']], $groupMember);
    }

    public function testUpdateNotExistingUsersIntoGroupFail() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $this->expectException(\Exception::class);

        $microsoftLdapService->updateUsersIntoGroup(self::$testGroupName, [self::$testUserData['username'], 'random.F.akeN.ame']);
    }

    public function testGetUserMembership() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $userMembership = $microsoftLdapService->getUserMembership(self::$testUserData['username']);

        $this->assertArraySubset([self::$testGroupName], $userMembership);
    }

    public function testUpdateGroupsIntoGroup() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->createGroup(self::$testGroupName." 2");

        $microsoftLdapService->updateGroupsIntoGroup(self::$testGroupName, [self::$testGroupName." 2"]);

        $groupMember = self::$adLdap->group()->inGroup(self::$testGroupName);

        $this->assertNotEmpty($groupMember);
    }

    public function testGetGroupMembership() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $groupMembership = $microsoftLdapService->getGroupMembership(self::$testGroupName);

        $this->assertArraySubset([self::$testGroupName." 2"], $groupMembership);
    }

    public function testModifyUser() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $userFromLdap = $microsoftLdapService->getUser(self::$testUserData['username']);
        $ldapUser = $this->getTestLdapUser();
        self::$changedTestUserData['parameters']['objectGUID'] = $userFromLdap['parameters']['objectGUID'];
        $ldapUser->setUsername(self::$changedTestUserData['username']);
        $ldapUser->setDistinguishedId(self::$changedTestUserData['distinguishedId']);
        $ldapUser->setFirstname(self::$changedTestUserData['firstname']);
        $ldapUser->setLastname(self::$changedTestUserData['lastname']);
        $ldapUser->setAttributes(json_encode(self::$changedTestUserData['parameters']));

        $microsoftLdapService->modifyUser($ldapUser);

        $renamedUserFromLdap = self::$adLdap->user()->info(self::$changedTestUserData['username'], ['description']);

        $this->assertNotEmpty($renamedUserFromLdap);
        $this->assertArraySubset([0 => ['description' => [0 => self::$changedTestUserData['parameters']['description']]]], $renamedUserFromLdap);

        $changedUserData = $microsoftLdapService->getUser(self::$changedTestUserData['username']);

        $this->assertTrue(is_array($changedUserData));
        $this->assertArraySubset(self::$changedTestUserData, $changedUserData);
    }

    public function testRenameGroup() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->renameGroup(self::$testGroupName . " 2", self::$testGroupName . " 3");

        $groupFromAd = self::$adLdap->group()->info(self::$testGroupName . " 3");

        $this->assertNotEmpty($groupFromAd);
        $this->assertArrayHasKey(0, $groupFromAd);
        $this->assertArrayHasKey('description', $groupFromAd[0]);
    }

    public function testCreateOu() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->createOu(self::$testOuName);

        $folder = self::$adLdap->folder()->listing([self::$testOuName], adLdap::ADLDAP_FOLDER, false);
        $this->assertTrue(is_array($folder));

        $microsoftLdapService->createOu(self::$testOuName." Sub", self::$testOuName);

        $folder = self::$adLdap->folder()->listing([self::$testOuName." Sub", self::$testOuName], adLdap::ADLDAP_FOLDER, false);
        $this->assertTrue(is_array($folder));
    }

    public function testOuExists() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $this->assertTrue($microsoftLdapService->ouExists(self::$testOuName));
        $this->assertFalse($microsoftLdapService->ouExists(self::$testOuName."2"));
    }

    public function testMoveUserIntoOu() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->moveUserIntoOu(self::$testOuName, self::$changedTestUserData['username']);

        $user = self::$adLdap->user()->info(self::$changedTestUserData['username'], ['distinguishedname']);

        $this->assertNotEmpty($user);
        $this->assertContains(self::$testOuName, $user[0]['dn']);
    }

    public function testUpdateUsersIntoOu() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->updateUsersIntoOu(self::$testOuName." Sub", [self::$changedTestUserData['username']], self::$testOuName);

        $user = self::$adLdap->user()->info(self::$changedTestUserData['username'], ['distinguishedname']);

        $this->assertNotEmpty($user);
        $this->assertContains(self::$testOuName, $user[0]['dn']);
    }

    public function testMoveGroupIntoOu() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->moveGroupIntoOu(self::$testOuName, self::$testGroupName);

        $group = self::$adLdap->group()->info(self::$testGroupName, ['distinguishedname']);

        $this->assertNotEmpty($group);
        $this->assertContains(self::$testOuName, $group[0]['dn']);
    }

    public function testUpdateGroupsIntoOu() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->updateGroupsIntoOu(self::$testOuName." Sub", [self::$testGroupName], self::$testOuName);

        $group = self::$adLdap->group()->info(self::$testGroupName, ['distinguishedname']);

        $this->assertNotEmpty($group);
        $this->assertContains(self::$testOuName." Sub", $group[0]['dn']);
    }

    public function testDnPathToOUList() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $user = self::$adLdap->user()->info(self::$changedTestUserData['username'], ['distinguishedname']);

        $this->assertNotEmpty($user);
        $ous = $microsoftLdapService->dnPathToOUList($user[0]['dn']);

        $this->assertArraySubset([self::$testOuName." Sub"], $ous);
    }

    public function testRemoveUser() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->removeUser(self::$changedTestUserData['username']);

        $this->assertEmpty(self::$adLdap->user()->info(self::$changedTestUserData['username'], ['description']));
    }

    public function testRemoveGroup() {
        $microsoftLdapService = new MicrosoftLdapService();
        $microsoftLdapService->setParameters(self::$configurationAD);

        $microsoftLdapService->removeGroup(self::$testGroupName);

        $groupFromAd = self::$adLdap->group()->info(self::$testGroupName, ['description']);
        $this->assertArrayNotHasKey(0, $groupFromAd);
    }

    public static function tearDownAfterClass() {
        try {
            self::$adLdap->user()->delete(self::$testUserData['username']);
        } catch (\Exception $e) {}

        try {
            self::$adLdap->user()->delete(self::$changedTestUserData['username']);
        } catch (\Exception $e) {}

        try {
            self::$adLdap->group()->delete(self::$testGroupName);
        } catch (\Exception $e) {}

        try {
            self::$adLdap->group()->delete(self::$testGroupName . " 2");
        } catch (\Exception $e) {}

        try {
            self::$adLdap->group()->delete(self::$testGroupName . " 3");
        } catch (\Exception $e) {}

        try {
            self::$adLdap->folder()->delete("OU=".self::$testOuName." Sub,OU=".self::$testOuName.",".self::$adLdap->getBaseDn());
        } catch (\Exception $e) {}

        try {
            self::$adLdap->folder()->delete("OU=".self::$testOuName.",".self::$adLdap->getBaseDn());
        } catch (\Exception $e) {}
    }
}
