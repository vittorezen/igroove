<?php

namespace Zen\IgrooveBundle\Manager;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use JMS\DiExtraBundle\Tests\Fixture\NullLogger;
use OldSound\RabbitMqBundle\RabbitMq\Producer;
use PHPUnit\Framework\TestCase;
use Doctrine\Common\Persistence\ObjectManager;
use Zen\IgrooveBundle\Entity\Group;
use Zen\IgrooveBundle\Entity\LdapGroup;
use Zen\IgrooveBundle\Entity\LdapUser;
use Zen\IgrooveBundle\Entity\Provider;
use Zen\IgrooveBundle\Entity\Student;
use Zen\IgrooveBundle\Entity\Subject;
use Zen\IgrooveBundle\Entity\Teacher;
use Zen\IgrooveBundle\Entity\TeacherSubjectGroup;
use Zen\IgrooveBundle\ImporterFilter\DatabaseInterno;
use Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Sector;
use Zen\IgrooveBundle\Repository\GroupRepository;
use Zen\IgrooveBundle\Repository\LdapGroupRepository;
use Zen\IgrooveBundle\Repository\LdapUserRepository;
use Zen\IgrooveBundle\Repository\StudentRepository;
use Zen\IgrooveBundle\Repository\SubjectRepository;
use Zen\IgrooveBundle\Repository\TeacherRepository;
use Zen\IgrooveBundle\Tests\TestUtilityTraits;

class PersonsAndGroupsTest extends TestCase {

    use TestUtilityTraits;

    /**
     * PersonsAndGroupsTest constructor.
     */
    public function __construct() {
        $this->defaultRepositories = ['Group', 'Student', 'Teacher', 'LdapGroup', 'LdapUser', 'Provider'];
        parent::__construct();
    }


    protected function getConfigurationManagerMock($values=[]) {
        if(!isset($values['getActiveDirectoryGeneratedGroupPrefix'])) {
            $values['getActiveDirectoryGeneratedGroupPrefix'] = "test_";
        }

        if(!isset($values['getActiveDirectoryGeneratedTeacherGroupPrefix'])) {
            $values['getActiveDirectoryGeneratedTeacherGroupPrefix'] = "";
        }

        $fakeConfigurationManager = $this->createPartialMock(ConfigurationManager::class,array_keys($values));

        foreach($values as $methodName => $value) {
            $fakeConfigurationManager->expects($this->atLeastOnce())
                ->method($methodName)
                ->will($this->returnValue($value));
        }

        return $fakeConfigurationManager;
    }

    protected function createRabbitMQAnswer($command, Provider $provider, $teacher, $parameters) {
        $providerSettings = $teacher ? $provider->getTeacherSettings() : $provider->getStudentSettings();

        $parameters = $parameters + [
                'domain' => $providerSettings['googleAppDomain'],
                'clientId' => $providerSettings['googleAppClientId'],
                'clientSecret' => $providerSettings['googleAppClientSecret'],
                'ouPath' => $providerSettings['googleAppOUPath'],
            ];
        $msg = array(
            'command' => $command,
            'parameters' => $parameters
        );

        return serialize($msg);
    }

    protected function prepareGoogleAppsStudentSettings(Provider $provider, $gPrefix = "gPrefix_") {
        $provider->setStudentGoogleAppDomain("test.domain");
        $provider->setStudentGoogleAppClientId("123456");
        $provider->setStudentGoogleAppClientSecret("s3cr37");
        $provider->setStudentGoogleAppOUPath("/Student/");
        $provider->setStudentGoogleAppCreateGroup(true);
        $provider->setStudentGoogleAppAutoEmail(true);
        $provider->setStudentGoogleAppCreateOU(true);
        $provider->setStudentGoogleAppGroupPrefix($gPrefix);
        $provider->setStudentGoogleAppOUPrefix($gPrefix);
        $provider->setStudentGoogleAppCreateProviderGroup(true);
        $provider->setStudentGoogleAppGroupExtraEmail('extra.email@test.domain');
        $provider->setStudentGoogleAppProviderGroupExtraEmail('extra.provideremail@test.domain');
    }

    protected function prepareGoogleAppsTeacherSettings(Provider $provider, $gPrefix = "gTeacher_") {
        $provider->setTeacherGoogleAppDomain("teach.test.domain");
        $provider->setTeacherGoogleAppClientId("123456");
        $provider->setTeacherGoogleAppClientSecret("s3cr37");
        $provider->setTeacherGoogleAppOUPath("/Teacher/");
        $provider->setTeacherGoogleAppAutoEmail(true);
        $provider->setTeacherGoogleAppCreateGroup(true);
        $provider->setTeacherGoogleAppCreateOU(true);
        $provider->setTeacherGoogleAppGroupPrefix($gPrefix);
        $provider->setTeacherGoogleAppOUPrefix($gPrefix);
        $provider->setTeacherGoogleAppCreateProviderGroup(true);
        $provider->setTeacherGoogleAppGroupExtraEmail('extra.email@test.domain');
        $provider->setTeacherGoogleAppProviderGroupExtraEmail('extra.provideremail@test.domain');
    }


    public function testImportElements() {
        $provider = new Provider();
        $this->setVariablesOnClass($provider, ['id' => 11]);

        $filter = new DatabaseInterno();

        $dataToImport = [
            'students' => [12 => new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student(12, 'XXXXXX', 'Test', 'Student', 13)],
            'groups' => [13 => new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Group(13, 'Test Group', 16)],
            'teachers' => [14 => new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Teacher(14, 'XXXXXY', 'Test', 'Teacher')],
            'subjects' => [15 => new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Subject(15, 'Test Subject')],
            'sectors' => [16 => new Sector(16, 'Test Sector')],
            'teacherSubjectGroupRelation' => [['teacher_id' => 14, 'group_id' => 13, 'subject_id' => 15]],
        ];

        $teacher = new Teacher($dataToImport['teachers'][14]->getId());
        $group = new Group();
        $subject = new Subject();

        $tsg = new TeacherSubjectGroup();
        $tsg->setProvider($provider);
        $tsg->setTeacher($teacher);
        $tsg->setGroup($group);
        $tsg->setSubject($subject);

        $this->setVariablesOnClass($filter, $dataToImport);

        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['importEntity']);

        $fakeQuery = $this->getMockForAbstractClass(AbstractQuery::class, [], "", false, false, true, ['execute']);
        $fakeQuery->expects($this->once())->method('execute')->willReturn([]);

        $fakeQueryBuilder = $this->createPartialMock(QueryBuilder::class, ['innerJoin', 'where', 'setParameter', 'getQuery']);
        $fakeQueryBuilder->expects($this->once())->method('innerJoin')->with('tsg.group', 'g')->willReturn($fakeQueryBuilder);
        $fakeQueryBuilder->expects($this->once())->method('where')->with('tsg.provider = :pid AND g.manageManually = 0')->willReturn($fakeQueryBuilder);
        $fakeQueryBuilder->expects($this->once())->method('setParameter')->with('pid', $provider->getId())->willReturn($fakeQueryBuilder);
        $fakeQueryBuilder->expects($this->once())->method('getQuery')->willReturn($fakeQuery);


        $fakeTsgRepository = $this->createPartialMock(EntityRepository::class,['createQueryBuilder']);
        $fakeTsgRepository->expects($this->once())->method('createQueryBuilder')->with('tsg')->willReturn($fakeQueryBuilder);

        $fakeEm = $this->getMockedEM(['TeacherSubjectGroup' => $fakeTsgRepository], ['persist']);
        $fakeEm->expects($this->once())->method('persist')->with($tsg);

        $this->setVariablesOnClass($personsAndGroups, ['em' => $fakeEm, 'logger' => new NullLogger(), 'importedEntities' => ['Teacher' => [14 => $teacher], 'Group' => [13 => $group], 'Subject' => [15 => $subject]]]);

        $dataToImport['students'][12]->setGroup($group);

        $personsAndGroups->expects($this->at(0))->method('importEntity')->with($dataToImport['sectors'], $provider, "Sector", "name");
        $personsAndGroups->expects($this->at(1))->method('importEntity')->with($dataToImport['groups'], $provider, "Group", "name");
        $personsAndGroups->expects($this->at(2))->method('importEntity')->with($dataToImport['teachers'], $provider, "Teacher", "fiscalCode");
        $personsAndGroups->expects($this->at(3))->method('importEntity')->with($dataToImport['subjects'], $provider, "Subject", "name");
        $personsAndGroups->expects($this->at(4))->method('importEntity')->with($dataToImport['students'], $provider, "Student", "fiscalCode");

        $personsAndGroups->importElements($filter, $provider);
    }

    public function testImportEntity() {
        $provider = new Provider();

        //test con uno da inserire, uno già esistente con idOnProvider, uno già esistente trovato per chiave
        $newEntities = [
            11 => new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student(11, 'AAAAAAAAAAAAAAAA', 'Student 1', 'Test'),
            12 => new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student(12, 'BBBBBBBBBBBBBBBB', 'Student 2', 'Test'),
            13 => new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student(13, 'CCCCCCCCCCCCCCCC', 'Student 3', 'Test'),
            14 => new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student(14, '', 'Student 4', 'Test'),
            15 => new \Zen\IgrooveBundle\ImporterFilter\ImportedEntity\Student(15, 'DDDDDDDDDDDDDDDD', 'Student 5', 'Test'),
        ];

        $student1 = new Student("AAAAAAAAAAAAAAAA");
        $student1->setFirstname("Pippo");
        $student1->setLastname("Paperino");
        $student1->setIdOnProvider(11);
        $this->setVariablesOnClass($student1, ['id' => 11111]);

        $student2 = new Student("BBBBBBBBBBBBBBBB");
        $student2->setFirstname("Pippo 2");
        $student2->setLastname("Paperino");
        $this->setVariablesOnClass($student2, ['id' => 22222]);

        $student5 = new Student("BBBBBBBBBBBBBBBB");
        $student5->setFirstname("Pippo 5");
        $student5->setLastname("Paperino");
        $this->setVariablesOnClass($student5, ['id' => 55555]);

        $fakeStudentRepository = $this->createPartialMock(StudentRepository::class,['findBy', 'findOneBy', 'updateWithImportData', 'createWithImportData', 'updateMembershipWithImportData']);
        $fakeStudentRepository->expects($this->once())->method('findBy')->with(['provider' => $provider->getId()])->willReturn([$student1, $student2]);
        $fakeStudentRepository->expects($this->at(1))->method('updateWithImportData')->with($student1, $newEntities[11], $provider);
        $fakeStudentRepository->expects($this->at(2))->method('updateWithImportData')->with($student2, $newEntities[12], $provider);
        $fakeStudentRepository->expects($this->at(3))->method('findOneBy')->with(['fiscalCode' => strtolower($newEntities[13]->getFiscalCode())])->willReturn(null);
        $fakeStudentRepository->expects($this->once())->method('createWithImportData')->with($newEntities[13], $provider, 13)->willReturn(new Student('kkk'));
        $fakeStudentRepository->expects($this->at(5))->method('findOneBy')->with(['fiscalCode' => strtolower($newEntities[15]->getFiscalCode())])->willReturn($student5);
        $fakeStudentRepository->expects($this->once())->method('updateMembershipWithImportData')->with($student5, $newEntities[15], $provider)->willReturn(new Student('kkk'));

        $fakeEm = $this->getMockedEM(['Student' => $fakeStudentRepository]);

        $personsAndGroups = new PersonsAndGroups($fakeEm, $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $method = new \ReflectionMethod(PersonsAndGroups::class, 'importEntity');
        $method->setAccessible(true);
        $method->invokeArgs($personsAndGroups, [$newEntities, $provider, "Student", "fiscalCode"]);
    }


    public function testCheckLdapGroups() {
        $provider = new Provider();
        $provider->setName("Test Provider");

        $group1 = new Group();
        $group1->setName("Test Group 1");
        $group1->setProvider($provider);

        $group2 = new Group();
        $group2->setName("Test Group 2");
        $group2->setProvider($provider);

        $fakeGroupRepository = $this->createPartialMock(GroupRepository::class,['findAll']);
        $fakeGroupRepository->expects($this->once())->method('findAll')->willReturn([$group1, $group2]);

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->exactly(2))->method('find')->withConsecutive(["test_InternetAccess"], ["test_PersonalDeviceAccess"])->willReturn(null);
        $fakeEM = $this->getMockedEM(['Group' => $fakeGroupRepository, 'LdapGroup' => $fakeLdapGroupRepository]);


        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncGroupWithLdapGroup']);
        $this->setVariablesOnClass($personsAndGroups, ['groupRepository' => $fakeGroupRepository,
                                                       'ldapGroupRepository' => $fakeLdapGroupRepository,
                                                       'em' => $fakeEM,
                                                       'logger' => new NullLogger(),
                                                       'active_directory_generated_group_prefix' => "test_",
                                                       'active_directory_generated_teacher_group_prefix' => "Docente_",
                                                        ]);

        $personsAndGroups->expects($this->at(0))->method('syncGroupWithLdapGroup')->with($group1);
        $personsAndGroups->expects($this->at(1))->method('syncGroupWithLdapGroup')->with($group1, "", true);
        $personsAndGroups->expects($this->at(2))->method('syncGroupWithLdapGroup')->with($group2);
        $personsAndGroups->expects($this->at(3))->method('syncGroupWithLdapGroup')->with($group2, "", true);

        $personsAndGroups->checkLdapGroups();
    }

    public function testSyncGroupWithLdapGroup() {
        $testGroupName = "Test";
        $testProviderGroupName = "Test Provider";
        $adPrefix = "test_";

        $ldapGroup = new LdapGroup();
        $ldapGroup->setName($adPrefix.$testGroupName);
        $ldapGroup->setOperation('CREATE');

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupWithDBGroup']);
        $fakeLdapProxy->expects($this->once())->method('syncLdapGroupWithDBGroup')->with($ldapGroup);

        $fakeGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeGroupRepository->expects($this->once())->method('find')->with($adPrefix.$testGroupName)->willReturn(null);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $provider = new Provider();
        $provider->setName($testProviderGroupName);
        $group = new Group();
        $group->setName($testGroupName);
        $group->setProvider($provider);

        $personsAndGroups->syncGroupWithLdapGroup($group, null, false, true);

        $this->assertEquals("CREATE", $ldapGroup->getOperation());
    }

    public function testSyncGroupWithLdapGroupTeacher() {
        $testGroupName = "Test";
        $testProviderGroupName = "Test Provider";
        $adPrefix = "Docenti ";

        $ldapGroup = new LdapGroup();
        $ldapGroup->setName($adPrefix.$testGroupName);
        $ldapGroup->setOperation('CREATE');

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupWithDBGroup']);
        $fakeLdapProxy->expects($this->once())->method('syncLdapGroupWithDBGroup')->with($ldapGroup);

        $fakeGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeGroupRepository->expects($this->once())->method('find')->with($adPrefix.$testGroupName)->willReturn(null);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(['getActiveDirectoryGeneratedTeacherGroupPrefix' => $adPrefix]), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $provider = new Provider();
        $provider->setName($testProviderGroupName);
        $group = new Group();
        $group->setName($testGroupName);
        $group->setProvider($provider);

        $personsAndGroups->syncGroupWithLdapGroup($group, null, true, true);

        $this->assertEquals("CREATE", $ldapGroup->getOperation());
    }

    public function testSyncGroupWithLdapGroupFail() {
        $testGroupName = "Test";
        $testProviderGroupName = "Test Provider";
        $adPrefix = "test_";

        $ldapGroup = new LdapGroup();
        $ldapGroup->setName($adPrefix.$testGroupName);

        $ldapGroupProvider = new LdapGroup();
        $ldapGroupProvider->setName($adPrefix.$testProviderGroupName);
        $ldapGroupProvider->addMember('group', $ldapGroup->getName());

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupWithDBGroup']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapGroupWithDBGroup')->with($ldapGroup)->willThrowException(new \Exception(''));

        $fakeGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeGroupRepository->expects($this->at(0))->method('find')->with($adPrefix.$testGroupName)->willReturn($ldapGroup);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $provider = new Provider();
        $provider->setName($testProviderGroupName);
        $group = new Group();
        $group->setName($testGroupName);
        $group->setProvider($provider);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncGroupWithLdapGroup($group, null, false, true);
    }

    public function testSyncGroupWithLdapGroupTeacherSkip() {
        $fakeGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeGroupRepository->expects($this->never())->method('find');

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $personsAndGroups->syncGroupWithLdapGroup(new Group(), null,true, true);
    }

    public function testSyncGroupWithLdapGroupRename() {
        $testGroupName = "Test";
        $testProviderGroupName = "Test Provider";
        $adPrefix = "test_";

        $ldapGroup = new LdapGroup();
        $ldapGroup->setName($adPrefix.$testGroupName);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupWithDBGroup']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapGroupWithDBGroup')->with($ldapGroup);

        $fakeGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeGroupRepository->expects($this->once())->method('find')->with($adPrefix.$testGroupName)->willReturn($ldapGroup);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $provider = new Provider();
        $provider->setName($testProviderGroupName);
        $group = new Group();
        $group->setName($testGroupName." 2");
        $group->setProvider($provider);

        $personsAndGroups->syncGroupWithLdapGroup($group, $testGroupName, false, true);

        $this->assertEquals("RENAME:".$adPrefix.$testGroupName." 2", $ldapGroup->getOperation());
    }

    public function testSyncGroupWithLdapGroupRenameTeacher() {
        $testGroupName = "Test";
        $testProviderGroupName = "Test Provider";
        $adPrefix = "docenti_";

        $ldapGroup = new LdapGroup();
        $ldapGroup->setName($adPrefix.$testGroupName);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupWithDBGroup']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapGroupWithDBGroup')->with($ldapGroup);

        $fakeGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeGroupRepository->expects($this->once())->method('find')->with($adPrefix.$testGroupName)->willReturn($ldapGroup);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(['getActiveDirectoryGeneratedTeacherGroupPrefix' => $adPrefix]), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $provider = new Provider();
        $provider->setName($testProviderGroupName);
        $group = new Group();
        $group->setName($testGroupName." 2");
        $group->setProvider($provider);

        $personsAndGroups->syncGroupWithLdapGroup($group, $testGroupName, true, true);

        $this->assertEquals("RENAME:".$adPrefix.$testGroupName." 2", $ldapGroup->getOperation());
    }

    public function testRenameLdapGroupFail() {
        $testGroupName = "Test";
        $testProviderGroupName = "Test Provider";
        $adPrefix = "test_";

        $ldapGroup = new LdapGroup();
        $ldapGroup->setName($adPrefix.$testGroupName);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupWithDBGroup']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapGroupWithDBGroup')->with($ldapGroup)->willThrowException(new \Exception(''));

        $fakeGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeGroupRepository->expects($this->once())->method('find')->with($adPrefix.$testGroupName)->willReturn($ldapGroup);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $provider = new Provider();
        $provider->setName($testProviderGroupName);
        $group = new Group();
        $group->setName($testGroupName." 2");
        $group->setProvider($provider);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncGroupWithLdapGroup($group, $testGroupName, false, true);
    }

    public function testRenameLdapGroupEmptySkip() {
        $fakeGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeGroupRepository->expects($this->never())->method('find');

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $personsAndGroups->syncGroupWithLdapGroup(new Group(), "", false, true);
    }

    public function testRenameLdapGroupTeacherSkip() {
        $fakeGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeGroupRepository->expects($this->never())->method('find');

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $personsAndGroups->syncGroupWithLdapGroup(new Group(), "", true, true);
    }

    public function testRemoveGroupLdapGroup() {
        $group = new Group();
        $group->setName("Test Group");
        $adPrefix = "test_";

        $basicLdapGroup = new LdapGroup();
        $basicLdapGroup->setName($adPrefix.$group->getName());

        $ldapGroup = clone $basicLdapGroup;
        $ldapGroup->setOperation("REMOVE");

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->once())->method('find')->with($basicLdapGroup->getName())->willReturn($basicLdapGroup);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupWithDBGroup']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapGroupWithDBGroup')->with($ldapGroup);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->removeGroupLdapGroup($group);
    }

    public function testRemoveGroupLdapGroupTeacher() {
        $group = new Group();
        $group->setName("Test Group");
        $adPrefix = "docenti_";

        $basicLdapGroup = new LdapGroup();
        $basicLdapGroup->setName($adPrefix.$group->getName());

        $ldapGroup = clone $basicLdapGroup;
        $ldapGroup->setOperation("REMOVE");

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->once())->method('find')->with($basicLdapGroup->getName())->willReturn($basicLdapGroup);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupWithDBGroup']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapGroupWithDBGroup')->with($ldapGroup);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(['getActiveDirectoryGeneratedTeacherGroupPrefix' => $adPrefix]), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->removeGroupLdapGroup($group, true);
    }

    public function testRemoveGroupLdapGroupFail() {
        $group = new Group();
        $group->setName("Test Group");
        $adPrefix = "test_";

        $basicLdapGroup = new LdapGroup();
        $basicLdapGroup->setName($adPrefix.$group->getName());

        $ldapGroup = clone $basicLdapGroup;
        $ldapGroup->setOperation("REMOVE");

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->once())->method('find')->with($basicLdapGroup->getName())->willReturn($basicLdapGroup);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupWithDBGroup']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapGroupWithDBGroup')->with($ldapGroup)->willThrowException(new \Exception(''));;

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->removeGroupLdapGroup($group);
    }

    public function testCheckLdapOUs() {
        $provider = new Provider();
        $provider->setName("Test Provider");

        $group1 = new Group();
        $group1->setName("Test Group 1");
        $group1->setProvider($provider);

        $group2 = new Group();
        $group2->setName("Test Group 2");
        $group2->setProvider($provider);

        $fakeGroupRepository = $this->createPartialMock(GroupRepository::class,['findBy']);
        $fakeGroupRepository->expects($this->once())->method('findBy')->with(['manageManually' => FALSE])->willReturn([$group1, $group2]);
        $fakeEM = $this->getMockedEM(['Group' => $fakeGroupRepository]);


        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncGroupWithLdapOU']);
        $this->setVariablesOnClass($personsAndGroups, ['groupRepository' => $fakeGroupRepository,
                                                       'em' => $fakeEM,
                                                       'logger' => new NullLogger(),
                                                       'active_directory_generated_group_prefix' => "test_",
                                                       'active_directory_generated_teacher_group_prefix' => "Docente_",
        ]);

        $personsAndGroups->expects($this->at(0))->method('syncGroupWithLdapOU')->with($group1);
        $personsAndGroups->expects($this->at(1))->method('syncGroupWithLdapOU')->with($group2);

        $personsAndGroups->checkLdapOUs();
    }

    public function testSyncGroupWithLdapOU() {
        $testGroupName = "Test";
        $prefix = "test_";
        $path = "Pre Ou";

        $provider = new Provider();
        $provider->setStudentLdapCreateOU(true);
        $provider->setStudentLdapOUPrefix($prefix);
        $provider->setStudentLdapOUPath($path);

        $group = new Group();
        $group->setName($testGroupName);
        $group->setProvider($provider);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['createOuIfNotExists']);
        $fakeLdapProxy->expects($this->once())->method('createOuIfNotExists')->with($prefix.$group->getName(), $path);

        $fakeEM = $this->getMockedEM();
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->syncGroupWithLdapOU($group);
    }


    public function testCheckLdapUsers() {
        $provider = new Provider();
        $provider->setName("Test Provider");

        $student = new Student('XXXXX');
        $student->setFirstname("Test");
        $student->setLastname("Student");
        $student->setProvider($provider);

        $teacher = new Teacher('XXXYY');
        $teacher->setFirstname("Test");
        $teacher->setLastname("Teacher");
        $teacher->setProvider($provider);

        $fakeStudentRepository = $this->createPartialMock(StudentRepository::class,['findAll']);
        $fakeStudentRepository->expects($this->once())->method('findAll')->willReturn([$student]);

        $fakeTeacherRepository = $this->createPartialMock(TeacherRepository::class,['findAll']);
        $fakeTeacherRepository->expects($this->once())->method('findAll')->willReturn([$teacher]);

        $fakeEM = $this->getMockedEM(['Student' => $fakeStudentRepository, 'Teacher' => $fakeTeacherRepository]);


        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncPersonWithLdapUser']);
        $this->setVariablesOnClass($personsAndGroups, ['studentRepository' => $fakeStudentRepository,
                                                       'teacherRepository' => $fakeTeacherRepository,
                                                       'em' => $fakeEM,
                                                       'logger' => new NullLogger(),
                                                       'active_directory_generated_group_prefix' => "test_",
                                                       'active_directory_generated_teacher_group_prefix' => "Docente_",
        ]);

        $personsAndGroups->expects($this->at(0))->method('syncPersonWithLdapUser')->with($student);
        $personsAndGroups->expects($this->at(1))->method('syncPersonWithLdapUser')->with($teacher);

        $personsAndGroups->checkLdapUsers();
    }

    public function testSyncPersonWithLdapUser() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $provider->setStudentAutoCreateUsername(true);
        $provider->setStudentGoogleAppClientId("1234");
        $provider->setStudentGoogleAppAutoEmail(true);
        $provider->setStudentGoogleAppDomain("student.test");

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);
        $group->setRadiusVlan(12);
        $group->setManageManually(false);

        $student = new Student('XXXXX');
        $student->setFirstname("Pippo Test");
        $student->setLastname("Student");
        $student->setStartingPassword("Abc12345");
        $student->setProvider($provider);
        $student->addMemberOf($group);

        $ldapUser = new LdapUser();
        $ldapUser->setDistinguishedId($student->getId());
        $ldapUser->setFirstname($student->getFirstname());
        $ldapUser->setLastname($student->getLastname());
        $ldapUser->setUsername("student.pippo");
        $ldapUser->setStartingPassword($student->getStartingPassword());
        $ldapUser->setOperation("CREATE");

        $attributes = [
            "description" => "Creato da igroove",
            "homedrive" => "Z:",
            "homedirectory" => "\\\\SERVER-PROFILI\\".$ldapUser->getUsername(),
            "email" => $ldapUser->getUsername()."@".$provider->getStudentGoogleAppDomain(),
            "radius-vlan" => 12,
            'objectGUID' => ""
        ];

        $ldapUser->setAttributes(json_encode($attributes));

        $configurations = [
            'getActiveDirectoryUsernameStyle' => "COGNOME.PRIMONOME",
            'getActiveDirectoryHomeDrive' => "z",
            'getActiveDirectoryHomeFolder' => "\\\\SERVER-PROFILI",
//            'getActiveDirectoryPasswordComplexity' => true,
//            'getActiveDirectoryPasswordMinChar' => "8",
        ];

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId', 'findAll']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->willReturn(null);
        $fakeLdapUserRepository->expects($this->once())->method('findAll')->willReturn([]);

        $fakeStudentRepository = $this->createPartialMock(StudentRepository::class, ['getUsernamesNotInLdapUser']);
        $fakeStudentRepository->expects($this->once())->method('getUsernamesNotInLdapUser')->willReturn([]);

        $fakeTeacherRepository = $this->createPartialMock(TeacherRepository::class, ['getUsernamesNotInLdapUser']);
        $fakeTeacherRepository->expects($this->once())->method('getUsernamesNotInLdapUser')->willReturn([]);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapUserWithDBUser']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapUserWithDBUser')->with($ldapUser);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository, 'Teacher' => $fakeTeacherRepository, 'Student' => $fakeStudentRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock($configurations), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->syncPersonWithLdapUser($student, true);
    }

    public function testSyncPersonWithLdapUserChange() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $provider->setStudentAutoCreateUsername(true);
        $provider->setStudentForceImportedPassword(true);
        $provider->setStudentGoogleAppClientId("1234");
        $provider->setStudentGoogleAppAutoEmail(true);
        $provider->setStudentGoogleAppDomain("student.test");

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);
        $group->setRadiusVlan(12);
        $group->setManageManually(false);

        $student = new Student('XXXXX');
        $student->setFirstname("Pippo Test");
        $student->setLastname("Student");
        $student->setStartingPassword("Abc12345");
        $student->setProvider($provider);
        $student->addMemberOf($group);

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setDistinguishedId($student->getId());
        $basicLdapUser->setFirstname("kkk");
        $basicLdapUser->setLastname($student->getLastname());

        $ldapUser = new LdapUser();
        $ldapUser->setDistinguishedId($student->getId());
        $ldapUser->setFirstname($student->getFirstname());
        $ldapUser->setLastname($student->getLastname());
        $ldapUser->setUsername("student.pippotest");
        $ldapUser->setStartingPassword($student->getStartingPassword());
        $ldapUser->setOperation("MODIFY");

        $attributes = [
            "description" => "Creato da igroove",
            "homedrive" => "Z:",
            "homedirectory" => "\\\\SERVER-PROFILI\\".$ldapUser->getUsername(),
            "email" => $ldapUser->getUsername()."@".$provider->getStudentGoogleAppDomain(),
            "radius-vlan" => 12,
            'objectGUID' => ""
        ];

        $ldapUser->setAttributes(json_encode($attributes));

        $configurations = [
            'getActiveDirectoryUsernameStyle' => "COGNOME.NOME",
            'getActiveDirectoryHomeDrive' => "z",
            'getActiveDirectoryHomeFolder' => "\\\\SERVER-PROFILI",
//            'getActiveDirectoryPasswordComplexity' => true,
//            'getActiveDirectoryPasswordMinChar' => "8",
        ];

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId', 'findAll']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($basicLdapUser->getDistinguishedId())->willReturn($basicLdapUser);
        $fakeLdapUserRepository->expects($this->once())->method('findAll')->willReturn([]);

        $fakeStudentRepository = $this->createPartialMock(StudentRepository::class, ['getUsernamesNotInLdapUser']);
        $fakeStudentRepository->expects($this->once())->method('getUsernamesNotInLdapUser')->willReturn([]);

        $fakeTeacherRepository = $this->createPartialMock(TeacherRepository::class, ['getUsernamesNotInLdapUser']);
        $fakeTeacherRepository->expects($this->once())->method('getUsernamesNotInLdapUser')->willReturn([]);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapUserWithDBUser']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapUserWithDBUser')->with($ldapUser);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository, 'Teacher' => $fakeTeacherRepository, 'Student' => $fakeStudentRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock($configurations), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->syncPersonWithLdapUser($student, true);
    }

    public function testSyncPersonWithLdapUserEmptyFail() {
        $provider = new Provider();
        $provider->setStudentAutoCreateUsername(false);

        $student = new Student('XXXXX');
        $student->setProvider($provider);

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setDistinguishedId($student->getId());

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($basicLdapUser->getDistinguishedId())->willReturn($basicLdapUser);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncPersonWithLdapUser($student, true);
    }

    public function testSyncPersonWithLdapUserFail() {
        $provider = new Provider();
        $provider->setStudentAutoCreateUsername(false);
        $provider->setStudentGoogleAppClientId(null);

        $student = new Student('XXXXX');
        $student->setProvider($provider);
        $student->setUsername("test.test");
        $student->setFirstname("Pippo Test");
        $student->setLastname("Student");

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setDistinguishedId($student->getId());
        $basicLdapUser->setUsername($student->getUsername());
        $basicLdapUser->setFirstname($student->getFirstname());
        $basicLdapUser->setLastname($student->getLastname());

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($basicLdapUser->getDistinguishedId())->willReturn($basicLdapUser);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapUserWithDBUser']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapUserWithDBUser')->willThrowException(new \Exception(''));

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncPersonWithLdapUser($student, true);
    }

    public function testResetPersonLdapUserPassword() {
        $student = new Student('XXXXX');
        $student->setUsername("test.test");
        $student->setStartingPassword("Az-12345");

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setUsername($student->getUsername());

        $ldapUser = clone $basicLdapUser;
        $ldapUser->setOperation("MODIFY");
        $ldapUser->setStartingPassword($student->getStartingPassword());

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByUsername']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByUsername')->with($basicLdapUser->getUsername())->willReturn($basicLdapUser);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapUserWithDBUser']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapUserWithDBUser')->with($ldapUser);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->resetPersonLdapUserPassword($student, true);
    }

    public function testResetPersonLdapUserPasswordEmptyFail() {
        $student = new Student('XXXXX');
        $student->setUsername("test.test");

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setUsername($student->getUsername());

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByUsername']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByUsername')->with($basicLdapUser->getUsername())->willReturn(null);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->resetPersonLdapUserPassword($student, true);
    }

    public function testResetPersonLdapUserPasswordFail() {
        $student = new Student('XXXXX');
        $student->setUsername("test.test");
        $student->setStartingPassword("Az-12345");

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setUsername($student->getUsername());

        $ldapUser = clone $basicLdapUser;
        $ldapUser->setOperation("MODIFY");
        $ldapUser->setStartingPassword($student->getStartingPassword());

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByUsername']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByUsername')->with($basicLdapUser->getUsername())->willReturn($basicLdapUser);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapUserWithDBUser']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapUserWithDBUser')->with($ldapUser)->willThrowException(new \Exception(''));

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->resetPersonLdapUserPassword($student, true);
    }

    public function testRemovePersonLdapUser() {
        $student = new Student('XXXXX');

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setDistinguishedId($student->getId());

        $ldapUser = clone $basicLdapUser;
        $ldapUser->setOperation("REMOVE");

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($basicLdapUser->getDistinguishedId())->willReturn($basicLdapUser);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapUserWithDBUser']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapUserWithDBUser')->with($ldapUser);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->removePersonLdapUser($student);
    }

    public function testRemovePersonLdapUserEmptyFail() {
        $student = new Student('XXXXX');

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setDistinguishedId($student->getId());

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($basicLdapUser->getDistinguishedId())->willReturn(null);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->removePersonLdapUser($student);
    }

    public function testRemovePersonLdapUserFail() {
        $student = new Student('XXXXX');

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setDistinguishedId($student->getId());

        $ldapUser = clone $basicLdapUser;
        $ldapUser->setOperation("REMOVE");

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($basicLdapUser->getDistinguishedId())->willReturn($basicLdapUser);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapUserWithDBUser']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapUserWithDBUser')->with($ldapUser)->willThrowException(new \Exception(''));

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->removePersonLdapUser($student);
    }


    public function testCheckLdapUsersMembership() {
        $provider = new Provider();
        $provider->setName("Test Provider");

        $student = new Student('XXXXX');
        $student->setFirstname("Test");
        $student->setLastname("Student");
        $student->setProvider($provider);

        $teacher = new Teacher('XXXYY');
        $teacher->setFirstname("Test");
        $teacher->setLastname("Teacher");
        $teacher->setProvider($provider);

        $fakeStudentRepository = $this->createPartialMock(StudentRepository::class,['findAll']);
        $fakeStudentRepository->expects($this->once())->method('findAll')->willReturn([$student]);

        $fakeTeacherRepository = $this->createPartialMock(TeacherRepository::class,['findAll']);
        $fakeTeacherRepository->expects($this->once())->method('findAll')->willReturn([$teacher]);

        $fakeProviderRepository = $this->createPartialMock(EntityRepository::class,['findBy']);
        $fakeProviderRepository->expects($this->once())->method('findBy')->with(['active' => true])->willReturn([$provider]);

        $fakeEM = $this->getMockedEM(['Student' => $fakeStudentRepository, 'Teacher' => $fakeTeacherRepository]);


        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncPersonMembershipsWithLdapGroups', 'syncProviderGroupLdapMembers']);
        $this->setVariablesOnClass($personsAndGroups, ['studentRepository' => $fakeStudentRepository,
                                                       'teacherRepository' => $fakeTeacherRepository,
                                                       'providerRepository' => $fakeProviderRepository,
                                                       'em' => $fakeEM,
                                                       'logger' => new NullLogger(),
                                                       'active_directory_generated_group_prefix' => "test_",
                                                       'active_directory_generated_teacher_group_prefix' => "Docente_",
        ]);

        $personsAndGroups->expects($this->at(0))->method('syncPersonMembershipsWithLdapGroups')->with($student);
        $personsAndGroups->expects($this->at(1))->method('syncPersonMembershipsWithLdapGroups')->with($teacher);
        $personsAndGroups->expects($this->at(2))->method('syncProviderGroupLdapMembers')->with($provider);
        $personsAndGroups->expects($this->at(3))->method('syncProviderGroupLdapMembers')->with($provider, true);

        $personsAndGroups->checkLdapUsersMembership();
    }

    public function testSyncPersonMembershipsWithLdapGroups() {
        $adPrefix = "test_";

        $provider = new Provider();
        $provider->setName("Test Provider");

        $group1 = new Group();
        $group1->setName("Group 1");

        $group2 = new Group();
        $group2->setName("Group 2");

        $student = new Student('XXXXX');
        $student->setUsername("test.test");
        $student->addMemberOf($group1);
        $student->addMemberOf($group2);

        $ldapGroup1 = new LdapGroup();
        $ldapGroup1->setName($adPrefix.$group1->getName());

        $ldapGroup2 = new LdapGroup();
        $ldapGroup2->setName($adPrefix.$group2->getName());
        $ldapGroup2->addMember('user', $student->getUsername());

        $ldapGroup3 = new LdapGroup();
        $ldapGroup3->setName($adPrefix."Group 3");
        $ldapGroup3->addMember('user', $student->getUsername());


        $basicLdapUser = new LdapUser();
        $basicLdapUser->setDistinguishedId($student->getId());
        $basicLdapUser->setUsername($student->getUsername());

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['findAll', 'find']);
        $fakeLdapGroupRepository->expects($this->once())->method('findAll')->willReturn([$ldapGroup1, $ldapGroup2, $ldapGroup3]);
        $fakeLdapGroupRepository->expects($this->at(1))->method('find')->with(strtolower($ldapGroup3->getName()))->willReturn($ldapGroup3);
        $fakeLdapGroupRepository->expects($this->at(2))->method('find')->with(strtolower($ldapGroup1->getName()))->willReturn($ldapGroup1);

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($basicLdapUser->getDistinguishedId())->willReturn($basicLdapUser);

        $fakeLdapProviderRepository = $this->createPartialMock(EntityRepository::class,['findAll']);
        $fakeLdapProviderRepository->expects($this->once())->method('findAll')->willReturn([$provider]);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupMembershipWithDB']);
        $fakeLdapProxy->expects($this->at(0))->method('syncLdapGroupMembershipWithDB')->with($ldapGroup3);
        $fakeLdapProxy->expects($this->at(1))->method('syncLdapGroupMembershipWithDB')->with($ldapGroup1);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository, 'LdapGroup' => $fakeLdapGroupRepository, 'Provider' => $fakeLdapProviderRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(['getActiveDirectoryGeneratedGroupPrefix' => $adPrefix]), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->syncPersonMembershipsWithLdapGroups($student, true);

        $this->assertArraySubset([$student->getUsername()], $ldapGroup1->getMembersList('user'));
        $this->assertArraySubset([$student->getUsername()], $ldapGroup2->getMembersList('user'));
        $this->assertEmpty($ldapGroup3->getMembersList('user'));
    }

    public function testSyncPersonMembershipsWithLdapGroupsFail() {
        $adPrefix = "docente_";

        $group1 = new Group();
        $group1->setName("Group 1");
        $this->setVariablesOnClass($group1, ['id' => "djdjdjdjd-djdajdas-dsada"]);

        $teacher = new Teacher('XXXXX');
        $teacher->setUsername("test.test");

        $tsg = new TeacherSubjectGroup();
        $tsg->setGroup($group1);
        $tsg->setTeacher($teacher);

        $teacher->getTeacherSubjectGroups()->add($tsg);

        $ldapGroup1 = new LdapGroup();
        $ldapGroup1->setName($adPrefix.$group1->getName());

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setDistinguishedId($teacher->getId());
        $basicLdapUser->setUsername($teacher->getUsername());

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['findAll', 'find']);
        $fakeLdapGroupRepository->expects($this->once())->method('findAll')->willReturn([$ldapGroup1]);
        $fakeLdapGroupRepository->expects($this->at(1))->method('find')->with(strtolower($ldapGroup1->getName()))->willReturn($ldapGroup1);

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($basicLdapUser->getDistinguishedId())->willReturn($basicLdapUser);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupMembershipWithDB']);
        $fakeLdapProxy->expects($this->once())->method('syncLdapGroupMembershipWithDB')->with($ldapGroup1)->willThrowException(new \Exception(''));

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository, 'LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(['getActiveDirectoryGeneratedTeacherGroupPrefix' => $adPrefix]), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncPersonMembershipsWithLdapGroups($teacher, true);
    }

    public function testSyncPersonMembershipsWithLdapGroupsNoLdapUserFail() {
        $student = new Student('XXXXX');

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['findAll']);
        $fakeLdapGroupRepository->expects($this->never())->method('findAll');

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($student->getId())->willReturn(null);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository, 'LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $personsAndGroups->syncPersonMembershipsWithLdapGroups($student, true);
    }


    public function testSyncGroupMembershipWithLdapGroupMembership() {
        $adPrefix = "test_";

        $student1 = new Student('XXXXX');
        $student1->setUsername("student1.test");

        $student2 = new Student('XXXX2');
        $student2->setUsername("student2.test");

        $group = new Group();
        $group->setName("Group Test");
        $group->addStudent($student1);
        $group->addStudent($student2);

        $ldapGroup = new LdapGroup();
        $ldapGroup->setName($adPrefix.$group->getName());
        $ldapGroup->addMember("user", "student2.test");
        $ldapGroup->addMember("user", "student3.test");

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->once())->method('find')->with(strtolower($ldapGroup->getName()))->willReturn($ldapGroup);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupMembershipWithDB']);
        $fakeLdapProxy->expects($this->once())->method('syncLdapGroupMembershipWithDB')->with($ldapGroup);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(['getActiveDirectoryGeneratedTeacherGroupPrefix' => $adPrefix]), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->syncGroupMembershipWithLdapGroupMembership($group);

        $this->assertEquals(["student2.test", "student1.test"], $ldapGroup->getMembersList('user'));
    }

    public function testSyncGroupMembershipWithLdapGroupMembershipTeacher() {
        $adPrefix = "test_";

        $teacher1 = new Teacher('XXXXX');
        $teacher1->setUsername("teacher1.test");
        $this->setVariablesOnClass($teacher1, ['id' => "udlfjvidlkf-djdajdas-dsada"]);

        $teacher2 = new Teacher('XXXX2');
        $teacher2->setUsername("teacher2.test");
        $this->setVariablesOnClass($teacher2, ['id' => "kfkfkfkfkfk-djdajdas-dsada"]);

        $group = new Group();
        $group->setName("Group Test");
        $this->setVariablesOnClass($group, ['id' => "djdjdjdjd-djdajdas-dsada"]);

        $tsg1 = new TeacherSubjectGroup();
        $tsg1->setGroup($group);
        $tsg1->setTeacher($teacher1);

        $tsg2 = new TeacherSubjectGroup();
        $tsg2->setGroup($group);
        $tsg2->setTeacher($teacher2);

        $group->getTeacherSubjectGroups()->add($tsg1);
        $group->getTeacherSubjectGroups()->add($tsg2);

        $ldapGroup = new LdapGroup();
        $ldapGroup->setName($adPrefix.$group->getName());
        $ldapGroup->addMember("user", "teacher2.test");
        $ldapGroup->addMember("user", "teacher3.test");

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->once())->method('find')->with(strtolower($ldapGroup->getName()))->willReturn($ldapGroup);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupMembershipWithDB']);
        $fakeLdapProxy->expects($this->once())->method('syncLdapGroupMembershipWithDB')->with($ldapGroup);

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(['getActiveDirectoryGeneratedTeacherGroupPrefix' => $adPrefix]), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->syncGroupMembershipWithLdapGroupMembership($group, true);

        $this->assertEquals(["teacher2.test", "teacher1.test"], $ldapGroup->getMembersList('user'));
    }

    public function testSyncGroupMembershipWithLdapGroupMembershipFail() {
        $adPrefix = "test_";

        $group = new Group();
        $group->setName("Group Test");

        $ldapGroup = new LdapGroup();
        $ldapGroup->setName($adPrefix.$group->getName());
        $ldapGroup->addMember("user", "student.test");

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->once())->method('find')->with(strtolower($ldapGroup->getName()))->willReturn($ldapGroup);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapGroupMembershipWithDB']);
        $fakeLdapProxy->expects($this->once())->method('syncLdapGroupMembershipWithDB')->with($ldapGroup)->willThrowException(new \Exception(''));

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(['getActiveDirectoryGeneratedTeacherGroupPrefix' => $adPrefix]), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncGroupMembershipWithLdapGroupMembership($group);
    }

    public function testSyncGroupMembershipWithLdapGroupMembershipTeacherFail() {
        $group = new Group();

        $fakeLdapGroupRepository = $this->createPartialMock(LdapGroupRepository::class,['find']);
        $fakeLdapGroupRepository->expects($this->never())->method('find');

        $fakeEM = $this->getMockedEM(['LdapGroup' => $fakeLdapGroupRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $personsAndGroups->syncGroupMembershipWithLdapGroupMembership($group, true);
    }

    public function testSyncGroupMembersVlan() {
        $provider = new Provider();

        $student = new Student('XXXXX');
        $student->setUsername("student.test");
        $student->setProvider($provider);

        $group = new Group();
        $group->setName("Group Test");
        $group->addStudent($student);
        $group->setManageManually(FALSE);
        $group->setRadiusVlan(12);
        $group->setProvider($provider);

        $attributes = [
            "description" => "Creato da igroove",
            "homedrive" => "",
            "homedirectory" => "",
            "email" => "",
            "radius-vlan" => null
        ];

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setUsername($student->getUsername());
        $basicLdapUser->setDistinguishedId($student->getId());
        $basicLdapUser->setAttributes(json_encode($attributes));

        $attributes['radius-vlan'] = 12;
        $ldapUser = clone $basicLdapUser;
        $ldapUser->setAttributes(json_encode($attributes));
        $ldapUser->setOperation("MODIFY");

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($student->getId())->willReturn($basicLdapUser);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapUserWithDBUser']);
        $fakeLdapProxy->expects($this->once())->method('syncLdapUserWithDBUser')->with($ldapUser);

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->syncGroupMembersVlan($group);
    }

    public function testSyncGroupMembersVlanFail() {
        $provider = new Provider();

        $student = new Student('XXXXX');
        $student->setUsername("student.test");
        $student->setProvider($provider);

        $group = new Group();
        $group->setName("Group Test");
        $group->addStudent($student);
        $group->setManageManually(FALSE);
        $group->setRadiusVlan(12);
        $group->setProvider($provider);

        $attributes = [
            "description" => "Creato da igroove",
            "homedrive" => "",
            "homedirectory" => "",
            "email" => "",
            "radius-vlan" => null
        ];

        $basicLdapUser = new LdapUser();
        $basicLdapUser->setUsername($student->getUsername());
        $basicLdapUser->setDistinguishedId($student->getId());
        $basicLdapUser->setAttributes(json_encode($attributes));

        $attributes['radius-vlan'] = 12;
        $ldapUser = clone $basicLdapUser;
        $ldapUser->setAttributes(json_encode($attributes));
        $ldapUser->setOperation("MODIFY");

        $fakeLdapUserRepository = $this->createPartialMock(LdapUserRepository::class,['findOneByDistinguishedId']);
        $fakeLdapUserRepository->expects($this->once())->method('findOneByDistinguishedId')->with($student->getId())->willReturn($basicLdapUser);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['syncLdapUserWithDBUser']);
        $fakeLdapProxy->expects($this->once())->method('syncLdapUserWithDBUser')->with($ldapUser)->willThrowException(new \Exception());

        $fakeEM = $this->getMockedEM(['LdapUser' => $fakeLdapUserRepository]);
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncGroupMembersVlan($group);
    }

    public function testCheckLdapUsersOUMembership() {
        $provider = new Provider();
        $provider->setName("Test Provider");

        $group1 = new Group();
        $group1->setName("Test Group 1");
        $group1->setProvider($provider);

        $group2 = new Group();
        $group2->setName("Test Group 2");
        $group2->setProvider($provider);

        $teacher = new Teacher("XXXXXX");

        $fakeGroupRepository = $this->createPartialMock(GroupRepository::class,['findBy']);
        $fakeGroupRepository->expects($this->once())->method('findBy')->with(['manageManually' => FALSE])->willReturn([$group1, $group2]);

        $fakeTeacherRepository = $this->createPartialMock(GroupRepository::class,['findAll']);
        $fakeTeacherRepository->expects($this->once())->method('findAll')->willReturn([$teacher]);

        $fakeEM = $this->getMockedEM(['Group' => $fakeGroupRepository, 'Teacher' => $fakeTeacherRepository]);

        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncGroupMembershipsWithLdapOU', 'moveTeacherToLdapOU']);
        $this->setVariablesOnClass($personsAndGroups, ['groupRepository' => $fakeGroupRepository,
                                                       'teacherRepository' => $fakeTeacherRepository,
                                                       'em' => $fakeEM,
                                                       'logger' => new NullLogger(),
                                                       'active_directory_generated_group_prefix' => "test_",
                                                       'active_directory_generated_teacher_group_prefix' => "Docente_",
        ]);

        $personsAndGroups->expects($this->at(0))->method('syncGroupMembershipsWithLdapOU')->with($group1);
        $personsAndGroups->expects($this->at(1))->method('syncGroupMembershipsWithLdapOU')->with($group2);
        $personsAndGroups->expects($this->at(2))->method('moveTeacherToLdapOU')->with($teacher);

        $personsAndGroups->checkLdapUsersOUMembership();
    }

    public function testSyncGroupMembershipsWithLdapOU() {
        $testGroupName = "Test";
        $prefix = "test_";
        $path = "Pre Ou";

        $provider = new Provider();
        $provider->setStudentLdapCreateOU(true);
        $provider->setStudentLdapOUPrefix($prefix);
        $provider->setStudentLdapOUPath($path);

        $student1 = new Student('AAAAA');
        $student1->setUsername("test1.student");
        $student1->setProvider($provider);

        $student2 = new Student('BBBBB');
        $student2->setUsername("test2.student");
        $student2->setProvider($provider);

        $group = new Group();
        $group->setName($testGroupName);
        $group->setProvider($provider);
        $group->addStudent($student1);
        $group->addStudent($student2);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['createOuIfNotExists', 'updateUsersIntoOu']);
        $fakeLdapProxy->expects($this->once())->method('createOuIfNotExists')->with($prefix.$group->getName(), $path);
        $fakeLdapProxy->expects($this->once())->method('updateUsersIntoOu')->with($prefix.$group->getName(), [$student1->getUsername(), $student2->getUsername()], $path);

        $fakeEM = $this->getMockedEM();
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->syncGroupMembershipsWithLdapOU($group);
    }

    public function testMoveTeacherToLdapOU() {
        $prefix = "Docente";
        $path = "Pre Ou";

        $provider = new Provider();
        $provider->setTeacherLdapCreateOU(true);
        $provider->setTeacherLdapOUPrefix($prefix);
        $provider->setTeacherLdapOUPath($path);

        $teacher = new Teacher('AAAAA');
        $teacher->setUsername("test.teacher");
        $teacher->setProvider($provider);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['createOuIfNotExists', 'moveUserIntoOu']);
        $fakeLdapProxy->expects($this->once())->method('createOuIfNotExists')->with($prefix, $path);
        $fakeLdapProxy->expects($this->once())->method('moveUserIntoOu')->with($prefix, $teacher->getUsername(), $path);

        $fakeEM = $this->getMockedEM();
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->moveTeacherToLdapOU($teacher);
    }

    public function testMoveStudentToLdapOU() {
        $prefix = "test_";
        $ouName = "Test Ou";
        $path = "Pre Ou";

        $provider = new Provider();
        $provider->setStudentLdapCreateOU(true);
        $provider->setStudentLdapOUPrefix($prefix);
        $provider->setStudentLdapOUPath($path);

        $student = new Student('AAAAA');
        $student->setUsername("test1.student");
        $student->setProvider($provider);

        $fakeLdapProxy = $this->createPartialMock(LdapProxy::class, ['createOuIfNotExists', 'moveUserIntoOu']);
        $fakeLdapProxy->expects($this->once())->method('createOuIfNotExists')->with($prefix.$ouName, $path);
        $fakeLdapProxy->expects($this->once())->method('moveUserIntoOu')->with($prefix.$ouName, $student->getUsername(), $path);

        $fakeEM = $this->getMockedEM();
        $personsAndGroups = new PersonsAndGroups($fakeEM, $this->getConfigurationManagerMock(), null, new NullLogger(), $fakeLdapProxy, "", null, false);

        $personsAndGroups->moveStudentToLdapOU($student, $ouName);
    }


    public function testCheckGoogleAppsGroups() {
        $provider = new Provider();
        $provider->setName("Test Provider");

        $group1 = new Group();
        $group1->setName("Test Group 1");
        $group1->setProvider($provider);

        $group2 = new Group();
        $group2->setName("Test Group 2");
        $group2->setProvider($provider);

        $fakeGroupRepository = $this->createPartialMock(GroupRepository::class,['findAll']);
        $fakeGroupRepository->expects($this->once())->method('findAll')->willReturn([$group1, $group2]);

        $fakeProviderRepository = $this->createPartialMock(EntityRepository::class,['findBy']);
        $fakeProviderRepository->expects($this->once())->method('findBy')->with(['active' => true])->willReturn([$provider]);

        $fakeEM = $this->getMockedEM(['Group' => $fakeGroupRepository, 'Provider' => $fakeProviderRepository]);

        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncGroupWithGoogleGroup', 'syncProviderGroupWithGoogleGroup']);
        $this->setVariablesOnClass($personsAndGroups, ['groupRepository' => $fakeGroupRepository,
                                                       'providerRepository' => $fakeProviderRepository,
                                                       'em' => $fakeEM,
                                                       'logger' => new NullLogger(),
                                                       'active_directory_generated_group_prefix' => "test_",
                                                       'active_directory_generated_teacher_group_prefix' => "Docente_",
        ]);

        $personsAndGroups->expects($this->at(0))->method('syncGroupWithGoogleGroup')->with($group1);
        $personsAndGroups->expects($this->at(1))->method('syncGroupWithGoogleGroup')->with($group1, null, true);
        $personsAndGroups->expects($this->at(2))->method('syncGroupWithGoogleGroup')->with($group2);
        $personsAndGroups->expects($this->at(3))->method('syncGroupWithGoogleGroup')->with($group2, null, true);
        $personsAndGroups->expects($this->at(4))->method('syncProviderGroupWithGoogleGroup')->with($provider);
        $personsAndGroups->expects($this->at(5))->method('syncProviderGroupWithGoogleGroup')->with($provider, true);

        $personsAndGroups->checkGoogleAppsGroups();
    }

    public function testSyncGroupWithGoogleGroup() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['createGroup', 'groupExists', 'addUserToGroup']);
        $fakeGoogleApps->expects($this->once())->method('createGroup')->with($gPrefix.$group->getName());
        $fakeGoogleApps->expects($this->exactly(2))->method('groupExists')->withConsecutive([$gPrefix.$group->getName(), false], [$gPrefix.$group->getName(), true])->willReturn(false);
        $fakeGoogleApps->expects($this->once())->method('addUserToGroup')->with($gPrefix.$group->getName(), $gPrefix.$provider->getName());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("createGroup", $provider, false, ['groupName' => $gPrefix . $group->getName()]));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncGroupWithGoogleGroup($group, null, FALSE, TRUE);
        $personsAndGroups->syncGroupWithGoogleGroup($group, null, FALSE);
    }

    public function testSyncGroupWithGoogleGroupTeacher() {
        $gPrefix = "gTeacher_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['createGroup', 'groupExists', 'addUserToGroup']);
        $fakeGoogleApps->expects($this->once())->method('createGroup')->with($gPrefix.$group->getName());
        $fakeGoogleApps->expects($this->exactly(2))->method('groupExists')->withConsecutive([$gPrefix.$group->getName(), false], [$gPrefix.$group->getName(), true])->willReturn(false);
        $fakeGoogleApps->expects($this->once())->method('addUserToGroup')->with($gPrefix.$group->getName(), $gPrefix.$provider->getName());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("createGroup", $provider, true, ['groupName' => $gPrefix . $group->getName()]));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["t_teach.test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncGroupWithGoogleGroup($group, null, true, TRUE);
        $personsAndGroups->syncGroupWithGoogleGroup($group, null, true);
    }

    public function testSyncGroupWithGoogleGroupFail() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['createGroup', 'groupExists']);
        $fakeGoogleApps->expects($this->once())->method('createGroup')->with($gPrefix.$group->getName())->willThrowException(new \Exception(''));
        $fakeGoogleApps->expects($this->once())->method('groupExists')->with($gPrefix.$group->getName(), false)->willReturn(false);

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncGroupWithGoogleGroup($group, null, FALSE, TRUE);
    }

    public function testSyncGroupWithGoogleGroupRename() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group 2");
        $group->setProvider($provider);

        $previousGroupName = "Test Group";

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['renameGroup', 'groupExists', 'addUserToGroup']);
        $fakeGoogleApps->expects($this->once())->method('renameGroup')->with($gPrefix.$group->getName(), $gPrefix.$previousGroupName);
        $fakeGoogleApps->expects($this->exactly(2))->method('groupExists')->withConsecutive([$gPrefix.$group->getName(), false], [$gPrefix.$group->getName(), true])->willReturn(false);
        $fakeGoogleApps->expects($this->once())->method('addUserToGroup')->with($gPrefix.$group->getName(), $gPrefix.$provider->getName());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("renameGroup", $provider, false,
            ['groupName' => $gPrefix . $group->getName(), 'previousGroupName' => $gPrefix.$previousGroupName]));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncGroupWithGoogleGroup($group, $previousGroupName, FALSE, TRUE);
        $personsAndGroups->syncGroupWithGoogleGroup($group, $previousGroupName, FALSE);
    }

    public function testSyncGroupWithGoogleGroupRenameTeacher() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group 2");
        $group->setProvider($provider);

        $previousGroupName = "Test Group";

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['renameGroup', 'groupExists', 'addUserToGroup']);
        $fakeGoogleApps->expects($this->once())->method('renameGroup')->with($gPrefix.$group->getName(), $gPrefix.$previousGroupName);
        $fakeGoogleApps->expects($this->exactly(2))->method('groupExists')->withConsecutive([$gPrefix.$group->getName(), false], [$gPrefix.$group->getName(), true])->willReturn(false);
        $fakeGoogleApps->expects($this->once())->method('addUserToGroup')->with($gPrefix.$group->getName(), $gPrefix.$provider->getName());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("renameGroup", $provider, true,
            ['groupName' => $gPrefix . $group->getName(), 'previousGroupName' => $gPrefix.$previousGroupName]));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["t_teach.test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncGroupWithGoogleGroup($group, $previousGroupName, true, TRUE);
        $personsAndGroups->syncGroupWithGoogleGroup($group, $previousGroupName, true);
    }

    public function testSyncGroupWithGoogleGroupRenameFail() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group 2");
        $group->setProvider($provider);

        $previousGroupName = "Test Group";

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['renameGroup', 'groupExists']);
        $fakeGoogleApps->expects($this->once())->method('renameGroup')->with($gPrefix.$group->getName(), $gPrefix.$previousGroupName)->willThrowException(new \Exception(''));
        $fakeGoogleApps->expects($this->once())->method('groupExists')->with($gPrefix.$group->getName(), false)->willReturn(false);

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncGroupWithGoogleGroup($group, $previousGroupName, FALSE, TRUE);
    }

    public function testSyncProviderGroupWithGoogleGroup() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $provider->addGroup($group);

        $groupsInGroup = [$gPrefix."testgroup@test.domain", "extra.provideremail@test.domain"];

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['updateGroupMembers', 'groupExists']);
        $fakeGoogleApps->expects($this->exactly(2))->method('groupExists')->withConsecutive([$gPrefix.$provider->getName(), false], [$gPrefix.$provider->getName(), true])->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('updateGroupMembers')->with($groupsInGroup, $gPrefix.$provider->getName());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("updateGroupMembers", $provider, false,
            ['studentsInGroup' => $groupsInGroup, 'groupName' => $gPrefix . $provider->getName()]));


        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($fakeGoogleApps, ['domain' => "test.domain"]);
        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncProviderGroupWithGoogleGroup($provider, FALSE, TRUE);
        $personsAndGroups->syncProviderGroupWithGoogleGroup($provider, FALSE);
    }

    public function testSyncProviderGroupWithGoogleGroupTeacher() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $provider->addGroup($group);

        $groupsInGroup = [$gPrefix."testgroup@test.domain", "extra.provideremail@test.domain"];

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['updateGroupMembers', 'groupExists']);
        $fakeGoogleApps->expects($this->exactly(2))->method('groupExists')->withConsecutive([$gPrefix.$provider->getName(), false], [$gPrefix.$provider->getName(), true])->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('updateGroupMembers')->with($groupsInGroup, $gPrefix.$provider->getName());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("updateGroupMembers", $provider, true,
            ['studentsInGroup' => $groupsInGroup, 'groupName' => $gPrefix . $provider->getName()]));


        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($fakeGoogleApps, ['domain' => "test.domain"]);
        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["t_teach.test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncProviderGroupWithGoogleGroup($provider, true, TRUE);
        $personsAndGroups->syncProviderGroupWithGoogleGroup($provider, true);
    }

    public function testSyncProviderGroupWithGoogleGroupFail() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $provider->addGroup($group);

        $groupsInGroup = [$gPrefix."testgroup@test.domain", "extra.provideremail@test.domain"];

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['updateGroupMembers', 'groupExists']);
        $fakeGoogleApps->expects($this->once())->method('groupExists')->with($gPrefix.$provider->getName(), false)->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('updateGroupMembers')->with($groupsInGroup, $gPrefix.$provider->getName())->willThrowException(new \Exception(''));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($fakeGoogleApps, ['domain' => "test.domain"]);
        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncProviderGroupWithGoogleGroup($provider, FALSE, TRUE);
    }

    public function testRemoveGroupGoogleAppsGroup() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['removeGroup']);
        $fakeGoogleApps->expects($this->once())->method('removeGroup')->with($gPrefix.$group->getName());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("removeGroup", $provider, false, ['groupName' => $gPrefix . $group->getName()]));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->removeGroupGoogleAppsGroup($group, FALSE, TRUE);
        $personsAndGroups->removeGroupGoogleAppsGroup($group, FALSE);
    }

    public function testRemoveGroupGoogleAppsGroupTeacher() {
        $gPrefix = "gTeacher_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['removeGroup']);
        $fakeGoogleApps->expects($this->once())->method('removeGroup')->with($gPrefix.$group->getName());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("removeGroup", $provider, true, ['groupName' => $gPrefix . $group->getName()]));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["t_teach.test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->removeGroupGoogleAppsGroup($group, true, TRUE);
        $personsAndGroups->removeGroupGoogleAppsGroup($group, true);
    }

    public function testRemoveGroupGoogleAppsGroupFail() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['removeGroup']);
        $fakeGoogleApps->expects($this->once())->method('removeGroup')->with($gPrefix.$group->getName())->willThrowException(new \Exception(''));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->removeGroupGoogleAppsGroup($group, FALSE, TRUE);
    }


    public function testCheckGoogleAppsOUs() {
        $provider = new Provider();
        $provider->setName("Test Provider");

        $group1 = new Group();
        $group1->setName("Test Group 1");
        $group1->setProvider($provider);

        $group2 = new Group();
        $group2->setName("Test Group 2");
        $group2->setProvider($provider);

        $fakeGroupRepository = $this->createPartialMock(GroupRepository::class,['findBy']);
        $fakeGroupRepository->expects($this->once())->method('findBy')->with(['manageManually' => FALSE])->willReturn([$group1, $group2]);

        $fakeEM = $this->getMockedEM(['Group' => $fakeGroupRepository]);

        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncGroupWithGoogleOU']);
        $this->setVariablesOnClass($personsAndGroups, ['groupRepository' => $fakeGroupRepository,
                                                       'em' => $fakeEM,
                                                       'logger' => new NullLogger(),
                                                       'active_directory_generated_group_prefix' => "test_",
                                                       'active_directory_generated_teacher_group_prefix' => "Docente_",
        ]);

        $personsAndGroups->expects($this->at(0))->method('syncGroupWithGoogleOU')->with($group1);
        $personsAndGroups->expects($this->at(1))->method('syncGroupWithGoogleOU')->with($group2);

        $personsAndGroups->checkGoogleAppsOUs();
    }

    public function testSyncGroupWithGoogleOU() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['createOU', 'ouExists']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('ouExists')->with($gPrefix.$group->getName())->willReturn(false);
        $fakeGoogleApps->expects($this->once())->method('createOU')->with($gPrefix.$group->getName());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("createOU", $provider, false, ['ouName' => $gPrefix . $group->getName()]));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncGroupWithGoogleOU($group, TRUE);
        $personsAndGroups->syncGroupWithGoogleOU($group);
    }

    public function testSyncGroupWithGoogleOUFail() {
        $gPrefix = "gprefix_";
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider,$gPrefix);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['createOU', 'ouExists']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('ouExists')->with($gPrefix.$group->getName())->willReturn(false);
        $fakeGoogleApps->expects($this->once())->method('createOU')->with($gPrefix.$group->getName())->willThrowException(new \Exception(''));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncGroupWithGoogleOU($group, TRUE);
    }

    public function testCheckGoogleAppsUsers() {
        $provider = new Provider();
        $provider->setName("Test Provider");

        $student = new Student("XXXXXX");
        $student->setUsername("test.student");

        $teacher = new Teacher("XXXXXY");
        $teacher->setUsername("test.teacher");

        $fakeStudentRepository = $this->createPartialMock(StudentRepository::class,['findAll']);
        $fakeStudentRepository->expects($this->once())->method('findAll')->willReturn([$student]);

        $fakeTeacherRepository = $this->createPartialMock(TeacherRepository::class,['findAll']);
        $fakeTeacherRepository->expects($this->once())->method('findAll')->willReturn([$teacher]);

        $fakeEM = $this->getMockedEM(['Student' => $fakeStudentRepository, 'Teacher' => $fakeTeacherRepository]);

        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncPersonWithGoogleAppsUser']);
        $this->setVariablesOnClass($personsAndGroups, [
                                                        'studentRepository' => $fakeStudentRepository,
                                                        'teacherRepository' => $fakeTeacherRepository,
                                                        'em' => $fakeEM,
                                                        'logger' => new NullLogger(),
                                                        'active_directory_generated_group_prefix' => "test_",
                                                        'active_directory_generated_teacher_group_prefix' => "Docente_",
        ]);

        $personsAndGroups->expects($this->at(0))->method('syncPersonWithGoogleAppsUser')->with($student);
        $personsAndGroups->expects($this->at(1))->method('syncPersonWithGoogleAppsUser')->with($teacher);

        $personsAndGroups->checkGoogleAppsUsers();
    }

    public function testSyncPersonWithGoogleAppsUser() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);
        $student->setFirstname("Test");
        $student->setLastname("Student");
        $student->setUsername("test.student");
        $student->setStartingPassword("Az-12345");

        $expectedEmail = $student->getUsername()."@".$provider->getStudentGoogleAppDomain();

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['emailInDomain', 'userExists', 'createUser']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($expectedEmail)->willReturn(true);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('userExists')->with($expectedEmail)->willReturn(false);
        $fakeGoogleApps->expects($this->once())->method('createUser')->with($expectedEmail, $student->getFirstname(), $student->getLastname(), $student->getStartingPassword());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("createUser", $provider, false,
            [
                'email' => $expectedEmail,
                'firstname' => $student->getFirstname(),
                'lastname' => $student->getLastname(),
                'password' => $student->getStartingPassword(),
                'userId' => $student->getId(),
                'teacher' => false
            ]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncPersonWithGoogleAppsUser($student, "", true);
        $personsAndGroups->syncPersonWithGoogleAppsUser($student);
    }

    public function testSyncPersonWithGoogleAppsUserRename() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider);
        $provider->setTeacherGoogleAppAutoEmailStyle("INIZIALENOMECOGNOME");

        $teacher = new Teacher("XXXXXX");
        $teacher->setProvider($provider);
        $teacher->setFirstname("Test");
        $teacher->setLastname("Teacher");
        $teacher->setStartingPassword("Az-12345");

        $expectedEmail = strtolower(substr($teacher->getFirstname(), 0, 1).$teacher->getLastname()."@".$provider->getTeacherGoogleAppDomain());
        $previousEmail = "teacher@".$provider->getTeacherGoogleAppDomain();

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['getDomain', 'emailInDomain', 'userExists', 'getUsersList', 'renameUser']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('getDomain')->willReturn($provider->getTeacherGoogleAppDomain());
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($expectedEmail)->willReturn(true);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('userExists')->with($expectedEmail)->willReturn(false);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('getUsersList')->willReturn([]);
        $fakeGoogleApps->expects($this->once())->method('renameUser')->with($expectedEmail, $previousEmail, $teacher->getFirstname(), $teacher->getLastname());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("renameUser", $provider, true,
            [
                'email' => $expectedEmail,
                'firstname' => $teacher->getFirstname(),
                'lastname' => $teacher->getLastname(),
                'previousEmail' => $previousEmail,
                'userId' => $teacher->getId(),
                'teacher' => true
            ]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["t_".$provider->getTeacherGoogleAppDomain() => $fakeGoogleApps]]);

        $personsAndGroups->syncPersonWithGoogleAppsUser($teacher, $previousEmail, true);
        $personsAndGroups->syncPersonWithGoogleAppsUser($teacher, $previousEmail);
    }

    public function testSyncPersonWithGoogleAppsUserEmptyFail() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $this->createMock(GoogleApps::class)]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncPersonWithGoogleAppsUser($student);
    }

    public function testSyncPersonWithGoogleAppsUserFail() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);
        $student->setFirstname("Test");
        $student->setLastname("Student");
        $student->setUsername("test.student");
        $student->setStartingPassword("Az-12345");

        $expectedEmail = $student->getUsername()."@".$provider->getStudentGoogleAppDomain();

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['emailInDomain', 'userExists', 'createUser']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($expectedEmail)->willReturn(true);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('userExists')->with($expectedEmail)->willReturn(false);
        $fakeGoogleApps->expects($this->once())->method('createUser')->with($expectedEmail, $student->getFirstname(), $student->getLastname(), $student->getStartingPassword())
            ->willThrowException(new \Exception(''));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncPersonWithGoogleAppsUser($student, "", true);
    }

    public function testResetPersonGoogleAppsUserPassword() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);
        $student->setEmail("test.student@".$provider->getStudentGoogleAppDomain());
        $student->setStartingPassword("Az-12345");

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['resetUserPassword']);
        $fakeGoogleApps->expects($this->once())->method('resetUserPassword')->with($student->getEmail(), $student->getStartingPassword());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("resetUserPassword", $provider, false,
            [
                'email' => $student->getEmail(),
                'password' => $student->getStartingPassword(),
            ]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->resetPersonGoogleAppsUserPassword($student);
        $personsAndGroups->resetPersonGoogleAppsUserPassword($student, false);
    }

    public function testResetPersonGoogleAppsUserPasswordEmptyFail() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $this->createMock(GoogleApps::class)]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->resetPersonGoogleAppsUserPassword($student);
    }

    public function testResetPersonGoogleAppsUserPasswordFail() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);
        $student->setEmail("test.student@".$provider->getStudentGoogleAppDomain());
        $student->setStartingPassword("Az-12345");

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['resetUserPassword']);
        $fakeGoogleApps->expects($this->once())->method('resetUserPassword')->with($student->getEmail(), $student->getStartingPassword())->willThrowException(new \Exception(''));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->resetPersonGoogleAppsUserPassword($student);
    }


    public function testCheckGoogleAppsUsersMembershipToGroup() {
        $provider = new Provider();
        $provider->setName("Test Provider");

        $group1 = new Group();
        $group1->setName("Test Group 1");
        $group1->setProvider($provider);

        $group2 = new Group();
        $group2->setName("Test Group 2");
        $group2->setProvider($provider);

        $fakeGroupRepository = $this->createPartialMock(GroupRepository::class,['findAll']);
        $fakeGroupRepository->expects($this->once())->method('findAll')->willReturn([$group1, $group2]);

        $fakeEM = $this->getMockedEM(['Group' => $fakeGroupRepository]);

        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncGroupMembershipsWithGoogleGroupMemberships']);
        $this->setVariablesOnClass($personsAndGroups, ['groupRepository' => $fakeGroupRepository,
                                                       'em' => $fakeEM,
                                                       'logger' => new NullLogger(),
                                                       'active_directory_generated_group_prefix' => "test_",
                                                       'active_directory_generated_teacher_group_prefix' => "Docente_",
        ]);

        $personsAndGroups->expects($this->at(0))->method('syncGroupMembershipsWithGoogleGroupMemberships')->with($group1);
        $personsAndGroups->expects($this->at(1))->method('syncGroupMembershipsWithGoogleGroupMemberships')->with($group1, true);
        $personsAndGroups->expects($this->at(2))->method('syncGroupMembershipsWithGoogleGroupMemberships')->with($group2);
        $personsAndGroups->expects($this->at(3))->method('syncGroupMembershipsWithGoogleGroupMemberships')->with($group2, true);

        $personsAndGroups->checkGoogleAppsUsersMembershipToGroup();
    }

    public function testSyncGroupMembershipsWithGoogleGroupMemberships() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student1 = new Student("XXXXXX");
        $student1->setEmail("test.student1@".$provider->getStudentGoogleAppDomain());

        $student2 = new Student("XXXXXX2");
        $student2->setEmail("test.student2@".$provider->getStudentGoogleAppDomain());

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);
        $group->addStudent($student1);
        $group->addStudent($student2);

        $personsInGroup = [$student1->getEmail(), $student2->getEmail(), $provider->getStudentGoogleAppGroupExtraEmail()];
        $groupName = $provider->getStudentGoogleAppGroupPrefix().$group->getName();

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['updateGroupMembers']);
        $fakeGoogleApps->expects($this->once())->method('updateGroupMembers')->with($personsInGroup, $groupName);

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("updateGroupMembers", $provider, false,
            [
                'studentsInGroup' => $personsInGroup,
                'groupName' => $groupName
            ]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships($group, false, true);
        $personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships($group);
    }

    public function testSyncGroupMembershipsWithGoogleGroupMembershipsTeacher() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider);

        $teacher1 = new Teacher("XXXXXX");
        $teacher1->setEmail("test.teacher1@".$provider->getTeacherGoogleAppDomain());
        $this->setVariablesOnClass($teacher1, ['id' => "udlfjvidlkf-djdajdas-dsada"]);

        $teacher2 = new Teacher("XXXXXX2");
        $teacher2->setEmail("test.teacher2@".$provider->getTeacherGoogleAppDomain());
        $this->setVariablesOnClass($teacher2, ['id' => "dkdkdkdkdkdk-djdajdas-dsada"]);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);

        $tsg1 = new TeacherSubjectGroup();
        $tsg1->setGroup($group);
        $tsg1->setTeacher($teacher1);

        $tsg2 = new TeacherSubjectGroup();
        $tsg2->setGroup($group);
        $tsg2->setTeacher($teacher2);

        $group->getTeacherSubjectGroups()->add($tsg1);
        $group->getTeacherSubjectGroups()->add($tsg2);

        $personsInGroup = [$teacher1->getEmail(), $teacher2->getEmail(), $provider->getTeacherGoogleAppGroupExtraEmail()];
        $groupName = $provider->getTeacherGoogleAppGroupPrefix().$group->getName();

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['updateGroupMembers']);
        $fakeGoogleApps->expects($this->once())->method('updateGroupMembers')->with($personsInGroup, $groupName);

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("updateGroupMembers", $provider, true,
            [
                'studentsInGroup' => $personsInGroup,
                'groupName' => $groupName
            ]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["t_".$provider->getTeacherGoogleAppDomain() => $fakeGoogleApps]]);

        $personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships($group, true, true);
        $personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships($group, true);
    }

    public function testSyncGroupMembershipsWithGoogleGroupMembershipsFail() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student1 = new Student("XXXXXX");
        $student1->setEmail("test.student1@".$provider->getStudentGoogleAppDomain());

        $student2 = new Student("XXXXXX2");
        $student2->setEmail("test.student2@".$provider->getStudentGoogleAppDomain());

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);
        $group->addStudent($student1);
        $group->addStudent($student2);

        $personsInGroup = [$student1->getEmail(), $student2->getEmail(), $provider->getStudentGoogleAppGroupExtraEmail()];
        $groupName = $provider->getStudentGoogleAppGroupPrefix().$group->getName();

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['updateGroupMembers']);
        $fakeGoogleApps->expects($this->once())->method('updateGroupMembers')->with($personsInGroup, $groupName)->willThrowException(new \Exception(''));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncGroupMembershipsWithGoogleGroupMemberships($group, false, true);
    }

    public function testCheckGoogleAppsUsersMembershipToOU() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider);

        $group1 = new Group();
        $group1->setName("Test Group 1");
        $group1->setProvider($provider);

        $group2 = new Group();
        $group2->setName("Test Group 2");
        $group2->setProvider($provider);

        $teacher = new Teacher("XXXXXX");
        $teacher->setProvider($provider);
        $teacher->setEmail("docente@test.it");

        $fakeGroupRepository = $this->createPartialMock(GroupRepository::class,['findBy']);
        $fakeGroupRepository->expects($this->once())->method('findBy')->with(['manageManually' => FALSE])->willReturn([$group1, $group2]);

        $fakeTeacherRepository = $this->createPartialMock(TeacherRepository::class,['findBy']);
        $fakeTeacherRepository->expects($this->once())->method('findBy')->with(['provider' => $provider->getId()])->willReturn([$teacher]);

        $fakeProviderRepository = $this->createPartialMock(EntityRepository::class,['findBy']);
        $fakeProviderRepository->expects($this->once())->method('findBy')->with(['active' => true])->willReturn([$provider]);

        $fakeEM = $this->getMockedEM(['Group' => $fakeGroupRepository, 'Teacher' => $fakeTeacherRepository, 'Provider' => $fakeProviderRepository]);

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("updateOUMembers", $provider, true,
            [
                'studentsInOU' => [$teacher->getEmail()],
                'ouName' => $provider->getTeacherGoogleAppOUPrefix()
            ]
        ));

        $personsAndGroups = $this->createPartialMock(PersonsAndGroups::class, ['syncGroupMembershipsWithGoogleOU']);
        $this->setVariablesOnClass($personsAndGroups, ['groupRepository' => $fakeGroupRepository,
                                                       'teacherRepository' => $fakeTeacherRepository,
                                                       'providerRepository' => $fakeProviderRepository,
                                                       'em' => $fakeEM,
                                                       'logger' => new NullLogger(),
                                                       'rabbitMQclient' => $fakeRabbitMQ,
                                                       'active_directory_generated_group_prefix' => "test_",
                                                       'active_directory_generated_teacher_group_prefix' => "Docente_",
        ]);

        $personsAndGroups->expects($this->at(0))->method('syncGroupMembershipsWithGoogleOU')->with($group1);
        $personsAndGroups->expects($this->at(1))->method('syncGroupMembershipsWithGoogleOU')->with($group2);

        $personsAndGroups->checkGoogleAppsUsersMembershipToOU();
    }

    public function testSyncGroupMembershipsWithGoogleOU() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student1 = new Student("XXXXXX");
        $student1->setEmail("test.student1@".$provider->getStudentGoogleAppDomain());
        $student1->setProvider($provider);

        $student2 = new Student("XXXXXX2");
        $student2->setEmail("test.student2@".$provider->getStudentGoogleAppDomain());
        $student2->setProvider($provider);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);
        $group->addStudent($student1);
        $group->addStudent($student2);

        $personsInGroup = [$student1->getEmail(), $student2->getEmail()];
        $ouName = $provider->getStudentGoogleAppOUPrefix().$group->getName();

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['updateOUMembers']);
        $fakeGoogleApps->expects($this->once())->method('updateOUMembers')->with($personsInGroup, $ouName);

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("updateOUMembers", $provider, false,
            [
                'studentsInOU' => $personsInGroup,
                'ouName' => $ouName
            ]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $personsAndGroups->syncGroupMembershipsWithGoogleOU($group, true);
        $personsAndGroups->syncGroupMembershipsWithGoogleOU($group);
    }

    public function testSyncGroupMembershipsWithGoogleOUFail() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student1 = new Student("XXXXXX");
        $student1->setEmail("test.student1@".$provider->getStudentGoogleAppDomain());
        $student1->setProvider($provider);

        $student2 = new Student("XXXXXX2");
        $student2->setEmail("test.student2@".$provider->getStudentGoogleAppDomain());
        $student2->setProvider($provider);

        $group = new Group();
        $group->setName("Test Group");
        $group->setProvider($provider);
        $group->addStudent($student1);
        $group->addStudent($student2);

        $personsInGroup = [$student1->getEmail(), $student2->getEmail()];
        $ouName = $provider->getStudentGoogleAppOUPrefix().$group->getName();

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['updateOUMembers']);
        $fakeGoogleApps->expects($this->once())->method('updateOUMembers')->with($personsInGroup, $ouName)->willThrowException(new \Exception(''));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["test.domain" => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->syncGroupMembershipsWithGoogleOU($group, true);
    }

    public function testMoveTeacherToGoogleAppsOU() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider);

        $teacher = new Teacher("XXXXXX");
        $teacher->setProvider($provider);
        $teacher->setEmail("test.teacher@".$provider->getTeacherGoogleAppDomain());

        $ouName = ($provider->getTeacherGoogleAppOUPrefix() ?: "");

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['emailInDomain', 'userExists', 'ouExists', 'addUserToOU']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($teacher->getEmail())->willReturn(true);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('userExists')->withConsecutive([$teacher->getEmail(), false], [$teacher->getEmail(), true])->willReturn(true);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('ouExists')->with($ouName)->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('addUserToOU')->with($teacher->getEmail(), $ouName);

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("addUserToOU", $provider, true,
            [
                'email' => $teacher->getEmail(),
                'ouName' => $ouName
            ]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["t_".$provider->getTeacherGoogleAppDomain() => $fakeGoogleApps]]);

        $personsAndGroups->moveTeacherToGoogleAppsOU($teacher, true);
        $personsAndGroups->moveTeacherToGoogleAppsOU($teacher);
    }

    public function testMoveTeacherToGoogleAppsOUFail() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider);

        $teacher = new Teacher("XXXXXX");
        $teacher->setProvider($provider);
        $teacher->setEmail("test.teacher@".$provider->getTeacherGoogleAppDomain());

        $ouName = ($provider->getTeacherGoogleAppOUPrefix() ?: "");

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['emailInDomain',  'ouExists', 'userExists', 'addUserToOU']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($teacher->getEmail())->willReturn(true);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('userExists')->withConsecutive([$teacher->getEmail(), false], [$teacher->getEmail(), true])->willReturn(true);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('ouExists')->with($ouName)->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('addUserToOU')->with($teacher->getEmail(), $ouName)->willThrowException(new \Exception(''));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["t_".$provider->getTeacherGoogleAppDomain() => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->moveTeacherToGoogleAppsOU($teacher, true);
    }

    public function testMoveStudentToGoogleAppsOU() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);
        $student->setEmail("test.student@".$provider->getStudentGoogleAppDomain());

        $ouName = "test/aaa";
        $fullOuName = ($provider->getStudentGoogleAppOUPrefix() ?: "").$ouName;

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['emailInDomain', 'ouExists', 'addUserToOU']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($student->getEmail())->willReturn(true);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('ouExists')->with($fullOuName)->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('addUserToOU')->with($student->getEmail(), $fullOuName);

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("addUserToOU", $provider, false,
            [
                'email' => $student->getEmail(),
                'ouName' => $fullOuName
            ]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => [$provider->getStudentGoogleAppDomain() => $fakeGoogleApps]]);

        $personsAndGroups->moveStudentToGoogleAppsOU($student, $ouName, true);
        $personsAndGroups->moveStudentToGoogleAppsOU($student, $ouName);
    }

    public function testMoveStudentToGoogleAppsOUFail() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);
        $student->setEmail("test.student@".$provider->getStudentGoogleAppDomain());

        $ouName = "test/aaa";
        $fullOuName = ($provider->getStudentGoogleAppOUPrefix() ?: "").$ouName;

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['emailInDomain', 'ouExists', 'addUserToOU']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($student->getEmail())->willReturn(true);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('ouExists')->with($fullOuName)->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('addUserToOU')->with($student->getEmail(), $fullOuName)->willThrowException(new \Exception(''));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => [$provider->getStudentGoogleAppDomain() => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->moveStudentToGoogleAppsOU($student, $ouName, true);
    }

    public function testRemovePersonGoogleAppsUser() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);
        $student->setEmail("test.student@".$provider->getStudentGoogleAppDomain());

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['emailInDomain', 'removeUser']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($student->getEmail())->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('removeUser')->with($student->getEmail());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("removeUser", $provider, false,
            ['email' => $student->getEmail()]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => [$provider->getStudentGoogleAppDomain() => $fakeGoogleApps]]);

        $personsAndGroups->removePersonGoogleAppsUser($student, true);
        $personsAndGroups->removePersonGoogleAppsUser($student);
    }

    public function testRemovePersonGoogleAppsUserTeacher() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsTeacherSettings($provider);

        $teacher = new Teacher("XXXXXX");
        $teacher->setProvider($provider);
        $teacher->setEmail("test.teacher@".$provider->getTeacherGoogleAppDomain());

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['emailInDomain', 'removeUser']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($teacher->getEmail())->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('removeUser')->with($teacher->getEmail());

        $fakeRabbitMQ = $this->createPartialMock(Producer::class, ['publish']);
        $fakeRabbitMQ->expects($this->once())->method('publish')->with($this->createRabbitMQAnswer("removeUser", $provider, true,
            ['email' => $teacher->getEmail()]
        ));

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), $fakeRabbitMQ, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => ["t_".$provider->getTeacherGoogleAppDomain() => $fakeGoogleApps]]);

        $personsAndGroups->removePersonGoogleAppsUser($teacher, true);
        $personsAndGroups->removePersonGoogleAppsUser($teacher);
    }

    public function testRemovePersonGoogleAppsUserFail() {
        $provider = new Provider();
        $provider->setName("Test Provider");
        $this->prepareGoogleAppsStudentSettings($provider);

        $student = new Student("XXXXXX");
        $student->setProvider($provider);
        $student->setEmail("test.student@".$provider->getStudentGoogleAppDomain());

        $fakeGoogleApps = $this->createPartialMock(GoogleApps::class, ['emailInDomain', 'removeUser']);
        $fakeGoogleApps->expects($this->atLeastOnce())->method('emailInDomain')->with($student->getEmail())->willReturn(true);
        $fakeGoogleApps->expects($this->once())->method('removeUser')->with($student->getEmail())->willThrowException(new \Exception(''));;

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['googleAppManagers' => [$provider->getStudentGoogleAppDomain() => $fakeGoogleApps]]);

        $this->expectException(\Exception::class);

        $personsAndGroups->removePersonGoogleAppsUser($student, true);
    }

    public function testCreateUsername() {
        $student = new Student("XXXXXX");
        $student->setFirstname("Pippo Pluto");
        $student->setLastname("Zio Paperino");

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['ldapAccountsUsername' => []]);

        $usernameCombinations = [
            "INIZIALENOME.COGNOME" => "p.ziopaperino",
            "INIZIALENOMECOGNOME" => "pziopaperino",
            "NOME.COGNOME" => "pippopluto.ziopaperi",
            "COGNOME.NOME" => "ziopaperino.pippoplu",
            "PRIMONOME.COGNOME" => "pippo.ziopaperino",
            "COGNOME.PRIMONOME" => "ziopaperino.pippo",
        ];

        foreach ($usernameCombinations as $usernameStyle => $username) {
            $this->assertEquals($username, $personsAndGroups->createUsername($student, $usernameStyle));
        }
    }

    public function testCreateUsernameDuplicates() {
        $student = new Student("XXXXXX");
        $student->setFirstname("Pippo Pluto");
        $student->setLastname("Zio Paperino");

        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['ldapAccountsUsername' => ["p.ziopaperino", "pziopaperino", "pippopluto.ziopaperi", "ziopaperino.pippoplu", "pippo.ziopaperino", "ziopaperino.pippo"]]);

        $usernameCombinations = [
            "INIZIALENOME.COGNOME" => "pi.ziopaperino",
            "INIZIALENOMECOGNOME" => "piziopaperino",
            "NOME.COGNOME" => "pippopluto2.ziopaper",
            "COGNOME.NOME" => "ziopaperino2.pippopl",
            "PRIMONOME.COGNOME" => "pippo2.ziopaperino",
            "COGNOME.PRIMONOME" => "ziopaperino2.pippo",
        ];

        foreach ($usernameCombinations as $usernameStyle => $username) {
            $this->assertEquals($username, $personsAndGroups->createUsername($student, $usernameStyle));
        }
    }

    public function testCreateUsernameFail() {
        $student = new Student("XXXXXX");
        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $this->setVariablesOnClass($personsAndGroups, ['ldapAccountsUsername' => []]);

        $this->expectException(\Exception::class);

        $personsAndGroups->createUsername($student, "COGNOME.NOME");
    }

    public function testGetRandomPassword() {
        $personsAndGroups = new PersonsAndGroups($this->getMockedEM(), $this->getConfigurationManagerMock(), null, new NullLogger(), null, "", null, false);

        $complexPassword = $personsAndGroups->getRandomPassword();
        $this->assertEquals(8, strlen($complexPassword));
        $this->assertStringStartsWith("Az-", $complexPassword);
        $this->assertTrue((int)substr($complexPassword,3) >= 10000);

        $normalPassword = $personsAndGroups->getRandomPassword(false, 10);
        $this->assertEquals(10, strlen($normalPassword));
        $this->assertTrue((int)$normalPassword >= 1000000000);
    }
}
